import { SystemOptions } from "./system-options";

export class System {
    constructor(public id_system:number, public name:String, public options : SystemOptions) {

    }
}
