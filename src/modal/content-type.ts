import { ContentTypeOptions } from "./content-type-options";

export class ContentType {
    constructor(public id_content_type:number, public name:String, public options : ContentTypeOptions) {}

    getActionsByEvent(event : String) {
        let actions : Array<String> = [];
        switch (this.id_content_type) {
            case 1:
                actions = Object.assign([],this.options.actions);
                if(event == "Touch" || event == "DoubleTouch") {
                    actions = Object.assign([],this.options.actions);
                } else {
                    actions.splice(2,1);
                }
                break;
            case 12:
                actions = Object.assign([],this.options.actions);
                if(event == "Touch" || event == "DoubleTouch") {
                    actions = Object.assign([],this.options.actions);
                } else {
                    actions.splice(2,1);
                }
                break;
            
            default:
                actions = Object.assign([],this.options.actions);
                break;
        }
        return actions;
    }
}
