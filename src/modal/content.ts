import { ContentOptions } from "./content-options";
import { Element } from "./element";
import { ElementOptions } from "./element-options";

export class Content {
    file : FormData = new FormData();
    childs : Array<Content> =[];
    selected : Boolean;
    sceneXR : Element;
    sceneVR : Element;

    constructor(
        public id_content:number,
        public resource:String,
        public id_content_type:number,
        public id_language : number,
        public id_element : number,
        public id_system : number,
        public options : ContentOptions,
        public id_version : number,
        public status : number,
        public id_content_parent : number,
        public elementsInMarker : Array<Content>) {

    }

    toFormData() : FormData {
        if(this.file instanceof FormData) {
            if(this.id_content != null) {
                this.file.append('id_content',""+this.id_content);
            }

            if(this.id_content_parent != null) {
                this.file.append('id_content_parent',""+this.id_content_parent);
            }
            this.file.append('resource',""+this.resource);
            this.file.append('id_content_type',""+this.id_content_type);
            this.file.append('id_language',""+this.id_language);
            this.file.append('id_element',""+this.id_element);
            this.file.append('id_system',""+this.id_system);
            this.file.append('options', JSON.stringify(this.options));
            this.file.append('id_version', ""+this.id_version);
            this.file.append('status', ""+this.status);
            return this.file;
        } else {
            return undefined;
        }
    }

    instanceOfElementsInMarker() : Content {
        this.elementsInMarker = this.elementsInMarker.map(item => new Content(
            item.id_content,
            item.resource,
            item.id_content_type,
            item.id_language,
            item.id_element,
            item.id_system,
            new ContentOptions(
                item.options.vuforia_target_id,
                item.options.rating,
                item.options.event,
                item.options.action,
                item.options.canvas,
                item.options.loop,
                item.options.transparency,
                item.options.subtitle,
                item.options.frame,
                item.options.frameStart,
                item.options.frameEnd,
                item.options.position
                ),
            item.id_version,
            item.status,
            item.id_content_parent,
            item.elementsInMarker).instanceOfXRScene(item.sceneXR).instanceOfVRScene(item.sceneVR));
        return this;
    }

    instanceOfXRScene(data : Element) : Content {
        if(data != null && data != undefined) {
            this.sceneXR =  new Element(
                data.id_element,
                data.id_element_type,
                data.content,
                new ElementOptions(data.options.travel_mode,data.options.ar_custom),
                data.id_version,
                data.status,
                data.id_element_parent,
                data.id_content_parent
            );
            this.sceneXR.gpar = data.gpar;
            this.sceneXR.instanceOfContent();
            return this;
        }
        return this;
    }

    instanceOfVRScene(data : Element) : Content {
        if(data != null && data != undefined) {
            this.sceneVR =  new Element(
                data.id_element,
                data.id_element_type,
                data.content,
                new ElementOptions(data.options.travel_mode,data.options.ar_custom),
                data.id_version,
                data.status,
                data.id_element_parent,
                data.id_content_parent
            );
           
            this.sceneVR.instanceOfContent();
            return this;
        }
        return this;
    }
}
