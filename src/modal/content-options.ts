export class ContentOptions {
    constructor(
        public vuforia_target_id:number, 
        public rating : number, 
        public event : String, 
        public action : String, 
        public canvas : any = null,
        public loop : Boolean = false,
        public transparency : Boolean = false,
        public subtitle : Boolean = false,
        public frame : number = 0,
        public frameStart : number = 0,
        public frameEnd : number = 0,
        public position : number = 0) {
    }
}
