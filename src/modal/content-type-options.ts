export class ContentTypeOptions {
    constructor(public type:String, public code:String, public language : Array<number>, public system : Array<number>, public events : Array<String>, public actions : Array<String>, public accept : Array<String>) {}
    
    findAccept(accept) {
        if(this.accept.length > 0) {
            return this.accept.find(item => item == accept);
        } else {
            return '';
        }
    }
}
