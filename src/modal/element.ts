import { Content } from "./content";
import { ElementOptions } from "./element-options";
import { ContentOptions } from "./content-options";

export class Element {
    removed : Boolean = false;
    gpar : Element;

    constructor(
        public id_element:number,
        public id_element_type:number,
        public content : Array<Content>,
        public options : ElementOptions,
        public id_version : number,
        public status : number,
        public id_element_parent : number,
        public id_content_parent : number = null) {}


    getResourceByContentTypeId(id_content_type : number) {
        if(this.content.length > 0) {
            let content : Content  = this.content.find(item => (item.id_content_type == id_content_type));
            if(content instanceof Content) {
                return content.resource;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    getResourceByContentTypeIdAndLanguageId(id_content_type : number, id_language : number) {
        if(this.content.length > 0) {
            let content : Content  = this.content.find(item => (item.id_content_type == id_content_type && item.id_language == id_language));
            if(content instanceof Content) {
                return content.resource;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    getContentIdByContentTypeId(id_content_type : number) {
        if(this.content.length > 0) {
            let content : Content  = this.content.find(item => (item.id_content_type == id_content_type));
            if(content instanceof Content) {
                return content.id_content;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    instanceOfContent() : Element {
        this.content = this.content.map(item => new Content(
            item.id_content,
            item.resource,
            item.id_content_type,
            item.id_language,
            item.id_element,
            item.id_system,
            new ContentOptions(
                item.options.vuforia_target_id,
                item.options.rating,
                item.options.event,
                item.options.action,
                item.options.canvas,
                item.options.loop,
                item.options.transparency,
                item.options.subtitle,
                item.options.frame,
                item.options.frameStart,
                item.options.frameEnd,
                item.options.position
                ),
            item.id_version,
            item.status,
            item.id_content_parent,
            (item.elementsInMarker == undefined) ? [] : item.elementsInMarker).instanceOfElementsInMarker());

        return this;
    }

    statusName() {
        this.status = parseInt(this.status.toString());

        if(this.status == 0) {
            return "Inactivo";
        } else if(this.status == 1) {
            return "Activo";
        } else {
            return "Pendiente";
        }
    }
}
