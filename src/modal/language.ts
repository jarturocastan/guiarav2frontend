import { LanguageOptions } from "./language-options";

export class Language {
    constructor(public id_language:number, public name:String, public options : LanguageOptions) {}

    isEqualId(id_language : number) {
        if(this.id_language == id_language) {
            return true;
        } else {
            return false;
        }
    }
}
