import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ContentTypeService } from 'src/services/content-type.service';
import { HelperService } from 'src/services/helper.service';
import { ElementService } from 'src/services/element.service';
import { Content } from 'src/modal/content';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentOptions } from 'src/modal/content-options';
import { MarkerService } from 'src/services/marker.service';
import { ContentService } from 'src/services/content.service';
import { ContentType } from 'src/modal/content-type';
import { ProgressService } from 'src/services/progress.service';
import { faFileArchive , faSpinner, faCheck } from '@fortawesome/free-solid-svg-icons';
import { ErrorService } from 'src/services/error.service';

@Component({
  selector: 'app-create-element-canvas-form',
  templateUrl: './create-element-canvas-form.component.html',
  styleUrls: ['./create-element-canvas-form.component.css']
})
export class CreateElementCanvasFormComponent implements OnInit {
  faFileArchive = faFileArchive;
  faSpinner = faSpinner;
  faCheck = faCheck;
  @Output()  saveElementInMarker = new EventEmitter();
  @Output()  cancelElementInMarker = new EventEmitter();
  showElementInMarkerForm : Boolean = true;
  showSceneXRForm : Boolean = false;
  showSceneVRForm : Boolean = false;
  showSceneGPAR : Boolean = false;
  showEditSceneXRForm : Boolean = false;
  showEditSceneGPAR : Boolean = false;
  reg : any;
  elementInMarker : Content;

  constructor(
    public helperService : HelperService,
    public contentTypeService : ContentTypeService,
    public elementService : ElementService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public markerService : MarkerService,
    public contentService : ContentService,
    public progressService : ProgressService,
    public errorService : ErrorService
  ) { }

  ngOnInit() {
    this.progressService.type = 2;
    this.progressService.progressActiveByName('element-canvas-form');
    this.errorService.clean();
    let httpOptions  = this.helperService.getHeaders();
    this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
      this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
        this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.contentTypeService.determinateContentTypeCombobox(3);
          this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
          this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
          this.reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]';
          this.elementInMarker = new Content(null,
            "",
            this.contentTypeService.contentType.id_content_type,
            this.languageService.language.id_language,
            this.elementService.getLocal('instruction-gpar').id_element,
            this.systemService.system.id_system,
            new ContentOptions(
              0,0,this.contentTypeService.contentType.options.events[0],
              this.contentTypeService.contentType.getActionsByEvent(this.contentTypeService.contentType.options.events[0])[0])
              ,1,1,this.markerService.getCurrentMarker().id_content,[]);
            this.contentService.save(this.elementInMarker.toFormData(),this.helperService.determinateHost(),httpOptions)
            .then(() => {
              this.contentService.content.childs = [];
              this.markerService.elementInMarker = this.contentService.content;
              this.changeContentType();
              this.progressService.progressInactiveByName('element-canvas-form');
            })

        })
      })
    });
  }

  changeContentType() {
    let contentType : ContentType = this.contentTypeService.contentTypeCombobox.find(item => (item.name == this.contentTypeService.contentType.name));
    if(contentType instanceof ContentType) {
      this.markerService.elementInMarker.id_content_type = this.contentTypeService.contentType.id_content_type;
      this.markerService.elementInMarker.options.event = this.contentTypeService.contentType.options.events[0];
      this.markerService.elementInMarker.options.action = this.contentTypeService.contentType.getActionsByEvent(this.markerService.elementInMarker.options.event)[0];
    }
  }

  save() {
    this.markerService.elementInMarker.options['position'] = this.markerService.currentMarker.elementsInMarker.length;
    if(this.markerService.elementInMarker.options.action != "Escena XR" && this.markerService.elementInMarker.options.action != "Escena VR") {
      if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
        this.markerService.currentMarker.elementsInMarker.push(this.markerService.elementInMarker);
        this.markerService.addCurrentMarker(this.markerService.currentMarker);
        this.saveElementInMarker.emit(this.markerService.currentMarker);
      }
    } else if(this.markerService.elementInMarker.options.action == "Escena VR") {
      this.showElementInMarkerForm = false;
      this.showSceneVRForm = true;
    } else {
      this.showElementInMarkerForm = false;
      this.showSceneXRForm = true;
    }
  }

  saveSceneVRInMarker(event) {
    if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
      this.markerService.currentMarker.elementsInMarker.push(this.markerService.elementInMarker);
      this.markerService.addCurrentMarker(this.markerService.currentMarker);
      this.showElementInMarkerForm = true;
      this.showSceneVRForm = false;
      this.saveElementInMarker.emit(this.markerService.currentMarker);
    }
  }

  saveSceneXRInMarker(event) {
    let content : Content = this.markerService.elementInMarker.sceneXR.content.find(item => (item.id_content_type == 16 && item.id_system == 3));
    if(content instanceof Content) {
      this.showElementInMarkerForm = false;
      this.showSceneXRForm = false;
      this.showSceneGPAR = true;
    } else {
      if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
        this.markerService.currentMarker.elementsInMarker.push(this.markerService.elementInMarker);
        this.markerService.addCurrentMarker(this.markerService.currentMarker);
        this.showElementInMarkerForm = true;
        this.showSceneXRForm = false;
        this.saveElementInMarker.emit(this.markerService.currentMarker);
      }
    }
  }

  saveSceneGPARInMarker(event) {
    if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
      this.markerService.currentMarker.elementsInMarker.push(this.markerService.elementInMarker);
      this.markerService.addCurrentMarker(this.markerService.currentMarker);
      this.showElementInMarkerForm = false;
      this.showSceneGPAR = false;
      this.saveElementInMarker.emit(this.markerService.currentMarker);
    }
  }

  cancel() {
    this.cancelElementInMarker.emit({});
    this.markerService.elementInMarker = null;
  }
}
