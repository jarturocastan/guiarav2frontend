import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateElementCanvasFormComponent } from './create-element-canvas-form.component';

describe('CreateElementCanvasFormComponent', () => {
  let component: CreateElementCanvasFormComponent;
  let fixture: ComponentFixture<CreateElementCanvasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateElementCanvasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateElementCanvasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
