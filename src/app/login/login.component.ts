import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/services/user.service';
import { HelperService } from 'src/services/helper.service';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
	username : String;
	password : String;
	error : AbstractControl;
	errors : Array<String> = [];

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private userService : UserService,
		public helperService : HelperService
	) { }

	ngOnInit() {}

	signIn(username,password) {
		let data : any = {
			username : username['viewModel'],
			password : password['viewModel']
		}
		data.client_id = 2;
		data.client_secret = "uNnzQLx3CG90BHI7f6cEf3HL20DzDgzRAxHK9A9p";
    data.grant_type = "password";
		this.userService.logIn(this.helperService.determinateHost(false),data).then(
			(success) =>{
        this.userService.getUser(this.helperService.determinateHost(false)+'/api', this.helperService.getHeaders()).then(
          (success) => {
            let dataToken = JSON.parse(localStorage.getItem('APICredentials'));
            dataToken.user = success;
            localStorage.setItem('APICredentials', JSON.stringify(dataToken));
            this.router.navigate(["/elementos", 0]);
          },
          (error) => {
            console.log(error);
          }
        )
			},
			(error)=>{
				if(error.status == 401) {
					this.errors.push("Usuario o contraseña invalida");
				}
			}
		)
	}
}
