import { Component, OnInit, Input, NgZone, AfterViewInit } from '@angular/core';
import { Element } from 'src/modal/element';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { HelperService } from 'src/services/helper.service';
import { faTrash, faCircle, faEdit, faClock } from '@fortawesome/free-solid-svg-icons';
import { ProgressService } from 'src/services/progress.service';
import { ElementService } from 'src/services/element.service';
import { Router, ActivatedRoute } from '@angular/router';
import  'fabric';
import { Content } from 'src/modal/content';
declare const fabric: any;
declare var $:any;


@Component({
  selector: 'app-card-collection',
  templateUrl: './card-collection.component.html',
  styleUrls: ['./card-collection.component.css']
})
export class CardCollectionComponent implements OnInit, AfterViewInit {
  @Input() element : Element;
  @Input() hide_description = false;
  @Input() hide_name = false;
  @Input() hide_titles = false;
  @Input() hide_controls = true;
  canvas : any;
  faEdit = faEdit;
  faTrash = faTrash;
  languageName : String;
  canvasWrapper: any;
  aspectRatio : Number;

  constructor(
    public languageService : LanguageService,
    public systemService : SystemService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public elementService : ElementService,
    private router: Router,
    private route: ActivatedRoute,
    public ngZone : NgZone,
  ) { }

  ngOnInit() {
    this.languageName = this.languageService.findLanguageById(2).name;

  }

  ngAfterViewInit() {
    window['angularComponentRef-'+this.element.id_element] = { component: this, ngZone: this.ngZone};
    let id = '#canvasWrapper-'+this.element.id_element;
    this.canvasWrapper = $(id)[0];
    let id_element = this.element.id_element;
    this.canvas = new fabric.StaticCanvas('canvasID-'+this.element.id_element, {
      preserveObjectStacking: true,
      height: 280,
      width: 200
    });
    let image : Content = this.element.content.find(item => (item.id_content_type == 4));
    if(image != undefined) {
      this.createCanvas(this.element.content.find(item => (item.id_content_type == 4)));
    }
    let componentRef = 'angularComponentRef-'+this.element.id_element;
    $(window).resize(function (){
      window[componentRef].component.responsiveCanvas( window[componentRef].component.canvas);
    });

  }

  responsiveCanvas(){
    let factorWidth = this.canvasWrapper.clientWidth;

    if (this.canvas.width != factorWidth) {
        let scaleFactor = factorWidth / this.canvas.width;
        let objects = this.canvas.getObjects();
        for (let i in objects) {
            objects[i].scaleX = objects[i].scaleX * scaleFactor;
            objects[i].scaleY = objects[i].scaleY * scaleFactor;
            objects[i].left = objects[i].left * scaleFactor;
            objects[i].top = objects[i].top * scaleFactor;
            objects[i].setCoords();
        }
        this.canvas.setWidth(this.canvas.width * scaleFactor);
        this.canvas.setHeight(this.canvas.height * scaleFactor);
        this.canvas.renderAll();
        this.canvas.calcOffset();
    }
  }

  delete() {
    this.progressService.msg = "Eliminando";
    this.progressService.progressActiveByName('default');
    this.elementService.delete(this.element.id_element,this.helperService.determinateHost(),this.helperService.getHeaders())
      .then(
        (data) => {
          let index : number = this.elementService.elements.indexOf(this.element);
          this.elementService.elements.splice(index,1);
          this.progressService.progressInactiveByName('default');
        },
        (error) => {
          this.progressService.progressInactiveByName('default');
          this.router.navigate(["/error",error[1],error[0].replace(new RegExp(' ', 'g'),'_%')]);
        }
      )
    let data = {
      elements : []
    }

    for (let index = 0; index < this.elementService.elements.length; index++) {
      this.elementService.elements[index].options["position"] = index;
      data.elements.push({id_element : this.elementService.elements[index].id_element, position : index});
    }
    let httpOptions  = this.helperService.getHeaders();
    this.elementService.savePositionOfElements(data,this.helperService.determinateHost(),httpOptions)
      .then(
        (data) => {},
        (error) =>{
				  this.router.navigate(["/error",error[1],error[0].replace(new RegExp(' ', 'g'),'_%')]);
        }
      )
  }

  edit() {
    this.router.navigate(['/editar/coleccion/',this.element.id_element,(this.elementService.currentElement == undefined) ? 0 : 0 ]);
  }

  goTo() {
    if(this.hide_description) {
      this.router.navigate(["/elementos",this.element.id_element]);
    }
  }

  createCanvas(image : Content) {
    this.aspectRatio = 0;
    if(this.canvas != undefined) {
        this.canvas.clear();
    }
    let id_element = this.element.id_element;
    fabric.Image.fromURL(this.helperService.determinateHost(false)+"/storage/"+image.resource,function(image){
      window['angularComponentRef-'+id_element].ngZone.run(()=>{
       let scale = ( window['angularComponentRef-'+id_element].component.canvasWrapper.clientWidth -  window['angularComponentRef-'+id_element].component.canvasWrapper.clientWidth * .01) /  image.width;
       image.set({
         selectable : false,
         scaleX: scale,
         scaleY: scale
       });
        let sizePercentages  = window['angularComponentRef-'+id_element].component.getPercentages(image.width * scale, image.height * scale,  window['angularComponentRef-'+id_element].component.canvas);
        image.percentage_width  = sizePercentages.left;
        image.percentage_height = sizePercentages.top;
        window['angularComponentRef-'+id_element].component.canvas.add(image);
        image.center();
      })
  });
    this.canvas.renderAll();
  }


  getPercentages(left, top, container){

    let percentageWidth  = (100 * left) / container.width;
    let percentageHeight = (100 * top) /container.height;

    return {left: percentageWidth, top: percentageHeight};
  }
}
