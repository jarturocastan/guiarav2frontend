import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { MarkerService } from 'src/services/marker.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Element } from 'src/modal/element';
import { ElementOptions } from 'src/modal/element-options';
import { faSpinner, faImage } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-create-element-instruction-scene-gpar-canvas-form',
  templateUrl: './create-element-instruction-scene-gpar-canvas-form.component.html',
  styleUrls: ['./create-element-instruction-scene-gpar-canvas-form.component.css']
})
export class CreateElementInstructionSceneGparCanvasFormComponent implements OnInit {
  @Output()  cancelElementInMarker = new EventEmitter();
  @Output()  saveSceneGPARInMarker = new EventEmitter();
  faImage = faImage;
  faSpinner = faSpinner;

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public markerService : MarkerService,
    public errorService : ErrorService
  ) { }

  ngOnInit() {
    this.errorService.clean();
    let element_instruction : Element = this.elementService.getLocal('instruction-gpar');
    let element = new Element(null,5,[],new ElementOptions(false,false),1,1, element_instruction.id_element, this.markerService.elementInMarker.id_content);
    let httpOptions  = this.helperService.getHeaders();
    this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
      this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
        this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(() => {
              this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
              this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
              this.contentTypeService.determinateContentTypeCombobox(this.elementService.currentElement.id_element_type);
            });
          });
        });
      });
    });
  }

  cancel() {
    this.cancelElementInMarker.emit({});
  }

  save(element : Element) {
    if(this.errorService.validateElement(element)) {
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
            this.elementService.currentElement.content = element.content;
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
            this.markerService.elementInMarker.sceneXR.gpar = this.elementService.currentElement;
            this.saveSceneGPARInMarker.emit({});
          })
        },
        (error) => {}
      )
    }
  }
}
