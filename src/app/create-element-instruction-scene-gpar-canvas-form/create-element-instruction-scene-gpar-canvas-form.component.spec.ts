import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateElementInstructionSceneGparCanvasFormComponent } from './create-element-instruction-scene-gpar-canvas-form.component';

describe('CreateElementInstructionSceneGparCanvasFormComponent', () => {
  let component: CreateElementInstructionSceneGparCanvasFormComponent;
  let fixture: ComponentFixture<CreateElementInstructionSceneGparCanvasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateElementInstructionSceneGparCanvasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateElementInstructionSceneGparCanvasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
