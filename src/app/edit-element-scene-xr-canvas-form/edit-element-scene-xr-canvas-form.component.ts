import { Component, OnInit, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { SystemService } from 'src/services/system.service';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { MarkerService } from 'src/services/marker.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Element } from 'src/modal/element';
import { ElementOptions } from 'src/modal/element-options';
import { Content } from 'src/modal/content';
import { faSpinner, faFileArchive, faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-edit-element-scene-xr-canvas-form',
  templateUrl: './edit-element-scene-xr-canvas-form.component.html',
  styleUrls: ['./edit-element-scene-xr-canvas-form.component.css']
})
export class EditElementSceneXrCanvasFormComponent implements OnInit {
  faFileArchive = faFileArchive;
  faSpinner = faSpinner;
  faCheck = faCheck;
  @Output()  cancelEditElementXR = new EventEmitter();
  @Output()  saveEditSceneXRInMarker = new EventEmitter();
  @Output()  editEditGPARInMarker = new EventEmitter();

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public markerService : MarkerService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    let httpOptions  = this.helperService.getHeaders();
    this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
      this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
        this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.elementTypeService.determinateMenu(-1);
            console.log('sceneXR ::',this.markerService.elementInMarker.sceneXR);
            if(this.markerService.elementInMarker.sceneXR != null) {
              let content : Content;
              if(this.markerService.elementInMarker.sceneXR.content.length > 1) {
                content = this.markerService.elementInMarker.sceneXR.content.find(item => (item.id_system == 3 && (item.id_content_type == 15 || item.id_content_type == 16)));
              } else {
                content = this.markerService.elementInMarker.sceneXR.content[0];
              }

              this.languageService.determinateLanguagesCombobox(this.markerService.elementInMarker.sceneXR.id_element_type);
              this.systemService.determinateSystemsCombobox(this.markerService.elementInMarker.sceneXR.id_element_type);
              this.contentTypeService.determinateContentTypeCombobox(this.markerService.elementInMarker.sceneXR.id_element_type);
              if(content != undefined) {
                this.contentTypeService.contentType = this.contentTypeService.findContentTypeById(content.id_content_type);
              }
              this.elementService.currentElement = this.elementService.getInstanceOfPlaneObject(this.markerService.elementInMarker.sceneXR).instanceOfContent();
              console.log('current element ::', this.elementService.currentElement);
            }
          });
        });
      });
    });
  }

  cancel() {
    this.cancelEditElementXR.emit({});
  }

  editGPAR() {
    this.elementService.currentElement = new Element(
      this.markerService.elementInMarker.sceneXR.gpar.id_element,
      this.markerService.elementInMarker.sceneXR.gpar.id_element_type,
      this.markerService.elementInMarker.sceneXR.gpar.content,
      new ElementOptions(
        this.markerService.elementInMarker.sceneXR.gpar.options.travel_mode,
        this.markerService.elementInMarker.sceneXR.gpar.options.ar_custom,
      ),
      this.markerService.elementInMarker.sceneXR.gpar.id_version,
      this.markerService.elementInMarker.sceneXR.gpar.status,
      this.markerService.elementInMarker.sceneXR.gpar.id_element_parent,
      this.markerService.elementInMarker.sceneXR.gpar.id_content_parent
    );
    this.elementService.currentElement.instanceOfContent();
    this.editEditGPARInMarker.emit({});
  }


  save(element : Element) {
    if(this.contentTypeService.contentType.id_content_type != 16) {
      element.content.forEach(item => {
        if(item.id_content_type == 16) {
          let index = element.content.indexOf(item);
          element.content.splice(index,1);
        }
      })
      element.gpar = undefined;
    }


    if(this.errorService.validateElement(element)) {
      let gpar = null;
      if(element.gpar != undefined && element.gpar != null) {
        gpar = element.gpar;
      }
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
          this.elementService.currentElement.content = element.content;
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
            this.markerService.elementInMarker.sceneXR = this.elementService.currentElement;
            if(gpar != null) {
              this.markerService.elementInMarker.sceneXR.gpar = this.elementService.getInstanceOfPlaneObject(gpar);
            }
            this.saveEditSceneXRInMarker.emit({});
          })
        },
        (error) => {}
      )
    }
  }

}
