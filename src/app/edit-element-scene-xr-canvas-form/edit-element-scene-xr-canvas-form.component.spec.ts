import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditElementSceneXrCanvasFormComponent } from './edit-element-scene-xr-canvas-form.component';

describe('EditElementSceneXrCanvasFormComponent', () => {
  let component: EditElementSceneXrCanvasFormComponent;
  let fixture: ComponentFixture<EditElementSceneXrCanvasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditElementSceneXrCanvasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditElementSceneXrCanvasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
