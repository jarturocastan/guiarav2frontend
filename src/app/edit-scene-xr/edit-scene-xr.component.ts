import { Component, OnInit } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MarkerService } from 'src/services/marker.service';
import { Content } from 'src/modal/content';
import { faVideo, faSpinner, faImage, faTrash, faEdit, faFileArchive } from '@fortawesome/free-solid-svg-icons';
import { Element } from 'src/modal/element';
import { ContentOptions } from 'src/modal/content-options';
import { ContentService } from 'src/services/content.service';
import { ElementOptions } from 'src/modal/element-options';
declare var videojs: any;

@Component({
  selector: 'app-edit-scene-xr',
  templateUrl: './edit-scene-xr.component.html',
  styleUrls: ['./edit-scene-xr.component.css']
})
export class EditSceneXRComponent implements OnInit {
  faVideo = faVideo;
  faSpinner = faSpinner;
  faImage = faImage;
  faTrash = faTrash;
  faEdit = faEdit;
  faFileArchive = faFileArchive;
  id_element_parent : number;

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
    public markerService : MarkerService,
    public contentService : ContentService
  ) { }

  ngOnInit() {
    this.progressService.progressActiveByName('default');
    this.errorService.clean();
    this.route.params.subscribe(params => {
      let id_element : number = parseInt(params["id_element"]);
      this.id_element_parent = parseInt(params["id_element_parent"]);
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.getById(id_element,this.helperService.determinateHost(),httpOptions).then(() => {
        this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
              this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
                this.elementTypeService.determinateMenu(-1);
                this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
                this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
                this.contentTypeService.determinateContentTypeCombobox(this.elementService.currentElement.id_element_type);
                this.contentTypeService.contentType = this.contentTypeService.findContentTypeById(this.elementService.currentElement.content[0].id_content_type);
                if(this.contentTypeService.contentType.id_content_type == 16) {
                  this.elementService.getElementsByElementParentId(this.elementService.currentElement.id_element, this.helperService.determinateHost(),httpOptions)
                    .then(() => {
                        if(this.elementService.elements.length > 0) {
                          this.elementService.currentElement.gpar = this.elementService.elements[0];
                        }
                    })
                }

                if( JSON.parse(localStorage.getItem('scene-gpar')) != null) {
                  let data : Element =  JSON.parse(localStorage.getItem('scene-gpar'));
                    data.gpar = this.elementService.currentElement.gpar;
                    this.elementService.currentElement = new Element(
                      data.id_element,
                      data.id_element_type,
                      data.content,
                      new ElementOptions(data.options.travel_mode,data.options.ar_custom),
                      data.id_version,
                      data.status,
                      data.id_element_parent,
                      data.id_content_parent
                    );
                  this.elementService.currentElement.instanceOfContent();
                  this.contentTypeService.contentType = this.contentTypeService.findContentTypeById(this.elementService.currentElement.content[0].id_content_type);
                }

                this.progressService.progressInactiveByName('default');
              });
            });
          });
        });
      })
    })
  }

  clenContent() {
    let  keys : Array<String> = Object.keys(Object.assign([], videojs.getPlayers()));
    keys.forEach(key => {
      videojs.getPlayers()[""+key].pause();
      delete videojs.getPlayers()[""+key];
    })

    this.elementService.currentElement.content = [];

  }

  editGPAR() {
    this.elementService.currentElement.id_element_parent = this.id_element_parent;
    localStorage.setItem('scene-gpar', JSON.stringify(this.elementService.currentElement));
    this.router.navigate(['/editar/escena-gpar/',this.elementService.currentElement.gpar.id_element, this.elementService.currentElement.id_element]);
  }

  cancel() {
    localStorage.setItem('scene-gpar', JSON.stringify(null));
    this.router.navigate(['/elementos',(this.id_element_parent != undefined)? this.id_element_parent : 0]);
  }

  save(element : Element) {
    if(this.errorService.validateElement(element)) {
      element.id_version = 2;
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
          this.elementService.currentElement.content = element.content;
          let ids : Array<number> = [this.elementService.currentElement.content[0].id_content];
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
              this.contentService.deleteNotIn(ids,this.elementService.currentElement.id_element,this.helperService.determinateHost()).then(() => {
                if(this.contentTypeService.contentType.id_content_type == 16) {
                  localStorage.setItem('scene-gpar', JSON.stringify(null));
                  this.router.navigate(['/crear/escena-gpar',this.elementService.currentElement.id_element]);
                } else {
                  localStorage.setItem('scene-gpar', JSON.stringify(null));
                  this.router.navigate(['/elementos',0]);
                }
              })
            })
        },
        (error) => {}
      )
    }
  }
}
