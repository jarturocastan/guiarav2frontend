import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSceneXRComponent } from './edit-scene-xr.component';

describe('EditSceneXRComponent', () => {
  let component: EditSceneXRComponent;
  let fixture: ComponentFixture<EditSceneXRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSceneXRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSceneXRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
