import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSceneGPARComponent } from './create-scene-gpar.component';

describe('CreateSceneGPARComponent', () => {
  let component: CreateSceneGPARComponent;
  let fixture: ComponentFixture<CreateSceneGPARComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSceneGPARComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSceneGPARComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
