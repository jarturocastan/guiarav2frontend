import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditElementInstructionSceneGparCanvasFormComponent } from './edit-element-instruction-scene-gpar-canvas-form.component';

describe('EditElementInstructionSceneGparCanvasFormComponent', () => {
  let component: EditElementInstructionSceneGparCanvasFormComponent;
  let fixture: ComponentFixture<EditElementInstructionSceneGparCanvasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditElementInstructionSceneGparCanvasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditElementInstructionSceneGparCanvasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
