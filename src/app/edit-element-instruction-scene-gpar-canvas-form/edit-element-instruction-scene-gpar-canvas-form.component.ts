import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { MarkerService } from 'src/services/marker.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Element } from 'src/modal/element';
import { ElementOptions } from 'src/modal/element-options';
import { faImage, faSpinner } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-edit-element-instruction-scene-gpar-canvas-form',
  templateUrl: './edit-element-instruction-scene-gpar-canvas-form.component.html',
  styleUrls: ['./edit-element-instruction-scene-gpar-canvas-form.component.css']
})
export class EditElementInstructionSceneGparCanvasFormComponent implements OnInit {
  @Output()  cancelEditElemenGPARtInMarker = new EventEmitter();
  @Output()  saveEditSceneGPARInMarker = new EventEmitter();
  faImage = faImage;
  faSpinner = faSpinner;

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public markerService : MarkerService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.errorService.clean();
    let httpOptions  = this.helperService.getHeaders();
      this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
        this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
              this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
              this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
              this.contentTypeService.determinateContentTypeCombobox(this.elementService.currentElement.id_element_type);
            });
          });
        });
      });

  }

  cancel() {
    this.cancelEditElemenGPARtInMarker.emit({});
  }

  save(element : Element) {

    if(this.errorService.validateElement(element)) {
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
            this.elementService.currentElement.content = element.content;

            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
            this.markerService.elementInMarker.sceneXR.gpar = this.elementService.currentElement;
            this.saveEditSceneGPARInMarker.emit({});
          })
        },
        (error) => {}
      )
    }
  }
}
