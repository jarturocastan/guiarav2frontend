import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSceneVrCanvasFormComponent } from './create-scene-vr-canvas-form.component';

describe('CreateSceneVrCanvasFormComponent', () => {
  let component: CreateSceneVrCanvasFormComponent;
  let fixture: ComponentFixture<CreateSceneVrCanvasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSceneVrCanvasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSceneVrCanvasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
