import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSceneXRComponent } from './card-scene-xr.component';

describe('CardSceneXRComponent', () => {
  let component: CardSceneXRComponent;
  let fixture: ComponentFixture<CardSceneXRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSceneXRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSceneXRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
