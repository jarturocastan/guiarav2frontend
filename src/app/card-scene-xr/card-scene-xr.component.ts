import { Component, OnInit, Input, AfterViewInit, AfterViewChecked, NgZone } from '@angular/core';
import { Element } from 'src/modal/element';
import { HelperService } from 'src/services/helper.service';
import { faTrash, faCircle, faEdit, faClock } from '@fortawesome/free-solid-svg-icons';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ProgressService } from 'src/services/progress.service';
import { ElementService } from 'src/services/element.service';
import { ActivatedRoute, Router } from '@angular/router';
declare var videojs: any;
import  'fabric';
import { Content } from 'src/modal/content';
declare const fabric: any;
declare var $:any;
declare var pannellum: any;

@Component({
  selector: 'app-card-scene-xr',
  templateUrl: './card-scene-xr.component.html',
  styleUrls: ['./card-scene-xr.component.css']
})
export class CardSceneXRComponent implements OnInit, AfterViewInit, AfterViewChecked {
  @Input() element : Element;
  @Input() hide_description = false;
  @Input() hide_name = false;
  @Input() hide_titles = false;
  @Input() hide_controls = true;
  video : any;
  canvas : any;
  faEdit = faEdit;
  faTrash = faTrash;
  languageName : String;
  canvasWrapper: any;
  aspectRatio : Number;
  pannelluminstance : any;

  constructor(
    public languageService : LanguageService,
    public systemService : SystemService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public elementService : ElementService,
    private router: Router,
    private route: ActivatedRoute,
    public ngZone : NgZone,
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
      if(this.element.content[0] != undefined) {
        if(videojs.getPlayers()['panorama-'+this.element.content[0].id_content]) {
          delete videojs.getPlayers()['panorama-'+this.element.content[0].id_content];
        }
      }


    if(this.element != undefined) {
      window['angularComponentRef-'+this.element.id_element] = { component: this, ngZone: this.ngZone};
      let id = '#canvasWrapper-'+this.element.id_element;
      this.canvasWrapper = $(id)[0];
      let id_element = this.element.id_element;
      this.canvas = new fabric.StaticCanvas('canvasID-'+this.element.id_element, {
        preserveObjectStacking: true,
        height: 350,
        width: window['angularComponentRef-'+id_element].component.canvasWrapper.clientWidth
      });
      this.determinateContentPreview(this.element);
    }
  }

  ngAfterViewChecked() {
    if(this.element != undefined) {
      if(this.element.content[0] != undefined) {
        if(videojs.getPlayers()['panorama-'+this.element.content[0].id_content]) {
          delete videojs.getPlayers()['panorama-'+this.element.content[0].id_content];
        }
      }
      window['angularComponentRef-'+this.element.id_element] = { component: this, ngZone: this.ngZone};
      this.determinateContentPreview(this.element);
    }
  }

  delete() {
    this.progressService.msg = "Eliminando";
    this.progressService.progressActiveByName('default');
    this.elementService.delete(this.element.id_element,this.helperService.determinateHost(),this.helperService.getHeaders())
      .then(
        (data) => {
          let index : number = this.elementService.elements.indexOf(this.element);
          this.elementService.elements.splice(index,1);
          this.progressService.progressInactiveByName('default');
        },
        (error) => {}
      )
  }

  edit() {
	window['angularComponentRef-'+this.element.id_element] = null;
	this.router.navigate(["/editar/escena-xr/", this.element.id_element,(this.elementService.currentElement == undefined) ? 0 : this.elementService.currentElement.id_element]);
  }

  goTo() {
    if(!this.hide_controls) {
		window['angularComponentRef-'+this.element.id_element] = null;
      if(this.element.content[0] != undefined) {
        if(this.element.content[0].id_content_type == 16) {
          this.router.navigate(["/elementos/", this.element.id_element]);
        } else {
          this.router.navigate(["/editar/escena-xr/", this.element.id_element,(this.elementService.currentElement == undefined) ? 0 : this.elementService.currentElement.id_element]);
        }
      }
    }
  }

  createCanvas(image : Content) {
    this.aspectRatio = 0;
    if(this.canvas != undefined) {
        this.canvas.clear();
    }
    let id_element = this.element.id_element;
    let url = this.helperService.determinateHost()+"/storage/"+image.resource;
    fabric.Image.fromURL(url,function(image){
      window['angularComponentRef-'+id_element].ngZone.run(()=>{
        let scale = 200 / image.width;
        image.set({
          scaleX: scale,
          scaleY: scale,
          selectable : false,
        });
        window['angularComponentRef-'+id_element].component.canvas.add(image);
        image.center();
      })
    });
    this.canvas.renderAll();
  }

  determinateContentPreview(element : Element) {
    if(element.content[0] != undefined) {
      if(element.content[0].id_content_type == 19) {
        if(this.video != undefined) {
          let src = this.helperService.determinateHost()+'/content/getFile/'+element.getContentIdByContentTypeId(19);
          if(src != this.video.currentSrc()) {
            this.video = videojs('panorama-'+element.content[0].id_content, {
                plugins: {
                    pannellum: {}
                }
            });
          }
        } else {
          this.video = videojs('panorama-'+element.content[0].id_content, {
            plugins: {
                pannellum: {}
            }
        });
        }
      } else if(element.content[0].id_content_type == 15 || element.content[0].id_content_type == 16) {
        if(this.canvas == undefined) {
          window['angularComponentRef-'+this.element.id_element] = { component: this, ngZone: this.ngZone};
          let id = '#canvasWrapper-'+this.element.id_element;
          this.canvasWrapper = $(id)[0];
          let id_element = this.element.id_element;
          this.canvas = new fabric.StaticCanvas('canvasID-'+this.element.id_element, {
            preserveObjectStacking: true,
            height: 350,
            width: window['angularComponentRef-'+id_element].component.canvasWrapper.clientWidth
          });
          this.createCanvas(new Content(0,"image/file.png",null,null,null,null,null,null,null,null,null));
        } else {
          if(this.canvas.getObjects().length == 0) {
            this.createCanvas(new Content(0,"image/file.png",null,null,null,null,null,null,null,null,null));
          }
        }
      } else if(element.content[0].id_content_type == 17) {
        let url = this.helperService.determinateHost()+"/content/getFile/"+element.content[0].id_content;
        if(this.pannelluminstance == undefined) {
          this.pannelluminstance = pannellum.viewer('panorama', {
            "type": "equirectangular",
            "panorama": url,
            "autoLoad": true,
          })
        } else {

        }
      }
    } else {
      if(this.canvas != undefined) { this.canvas.clear() }
    }
  }
}
