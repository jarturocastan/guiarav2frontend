import { Component, OnInit, Input, AfterViewInit, NgZone } from '@angular/core';
import { Element } from 'src/modal/element';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { Language } from 'src/modal/language';
import { HelperService } from 'src/services/helper.service';
import { faTrash, faCircle, faEdit, faClock } from '@fortawesome/free-solid-svg-icons';
import { ProgressService } from 'src/services/progress.service';
import { ElementService } from 'src/services/element.service';
import { Router, ActivatedRoute } from '@angular/router';
import  'fabric';
import { Content } from 'src/modal/content';
declare const fabric: any;
declare var $:any;

@Component({
  selector: 'app-card-scene-gpar',
  templateUrl: './card-scene-gpar.component.html',
  styleUrls: ['./card-scene-gpar.component.css']
})
export class CardSceneGPARComponent implements OnInit,AfterViewInit {
  @Input() element : Element;
  @Input() hide_description = false;
  @Input() hide_name = false;
  @Input() hide_titles = false;
  @Input() hide_controls = true;
  canvas : any;
  faEdit = faEdit;
  faTrash = faTrash;
  languageName : String;
  canvasWrapper: any;
  aspectRatio : Number;

  constructor(
    public languageService : LanguageService,
    public systemService : SystemService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public elementService : ElementService,
    private router: Router,
    private route: ActivatedRoute,
    public ngZone : NgZone,
  ) { }

  ngOnInit() {
    this.languageName = this.languageService.findLanguageById(2).name;
  }

  ngAfterViewInit() {
    window['angularComponentRef-'+this.element.id_element] = { component: this, ngZone: this.ngZone};
    let id = '#canvasWrapper-'+this.element.id_element;
    this.canvasWrapper = $(id)[0];
    let id_element = this.element.id_element;
    this.canvas = new fabric.StaticCanvas('canvasID-'+this.element.id_element, {
      preserveObjectStacking: true,
      height: 280,
      width: 200
    });
    let marker : Content = this.element.content.find(item => (item.id_content_type == 2));
    if(marker != undefined) {
      this.createCanvas(marker);
    }
  }

  delete() {
    this.progressService.msg = "Eliminando";
    this.progressService.progressActiveByName('default');
    this.elementService.delete(this.element.id_element,this.helperService.determinateHost(),this.helperService.getHeaders())
      .then(
        (data) => {
          let index : number = this.elementService.elements.indexOf(this.element);
          this.elementService.elements.splice(index,1);
          this.progressService.progressInactiveByName('default');
        },
        (error) => {}
      )
  }

  edit() {
    this.router.navigate(['/editar/escena-gpar/', this.element.id_element,(this.elementService.currentElement == undefined) ? 0 : this.elementService.currentElement.id_element]);
  }

  goTo() {
    if(!this.hide_controls) {
      this.router.navigate(["/editar/escena-gpar/",  this.element.id_element,(this.elementService.currentElement == undefined) ? 0 : this.elementService.currentElement.id_element]);
    }
  }

  createCanvas(image : Content) {
    this.aspectRatio = 0;
    if(this.canvas != undefined) {
        this.canvas.clear();
    }
    let id_element = this.element.id_element;
    fabric.Image.fromURL(this.helperService.determinateHost()+"/storage/"+image.resource,function(image){
      window['angularComponentRef-'+id_element].ngZone.run(()=>{
        let scale = 200 / image.width;
        image.set({
          scaleX: scale,
          scaleY: scale,
          selectable : false,
        }); 
        window['angularComponentRef-'+id_element].component.canvas.add(image);  
        image.center();
      })
    });
    this.canvas.renderAll();
  }
}
