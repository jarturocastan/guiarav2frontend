import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSceneGPARComponent } from './card-scene-gpar.component';

describe('CardSceneGPARComponent', () => {
  let component: CardSceneGPARComponent;
  let fixture: ComponentFixture<CardSceneGPARComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSceneGPARComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSceneGPARComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
