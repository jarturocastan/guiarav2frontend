import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewMarkerComponent } from './preview-marker.component';

describe('PreviewMarkerComponent', () => {
  let component: PreviewMarkerComponent;
  let fixture: ComponentFixture<PreviewMarkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewMarkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewMarkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
