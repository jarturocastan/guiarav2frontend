import { Component, OnInit, Input, AfterViewInit, NgZone } from '@angular/core';
import { Element } from 'src/modal/element';
import { faTrash, faCircle, faEdit, faClock } from '@fortawesome/free-solid-svg-icons';
import  'fabric';
import { Content } from 'src/modal/content';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { ElementService } from 'src/services/element.service';
import { Router, ActivatedRoute } from '@angular/router';
declare const fabric: any;
declare var $:any;


@Component({
  selector: 'app-preview-marker',
  templateUrl: './preview-marker.component.html',
  styleUrls: ['./preview-marker.component.css']
})
export class PreviewMarkerComponent implements OnInit, AfterViewInit {
  @Input() element : Element;
  canvas : any;
  faEdit = faEdit;
  faTrash = faTrash;
  languageName : String;
  canvasWrapper: any;
  aspectRatio : Number;

  constructor(
    public languageService : LanguageService,
    public systemService : SystemService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public elementService : ElementService,
    private router: Router,
    private route: ActivatedRoute,
    public ngZone : NgZone,
  ) { }

  ngOnInit() {
  }

  
  ngAfterViewInit() {
    window['angularComponentRef-'+this.element.id_element] = { component: this, ngZone: this.ngZone};
    let id = '#canvasWrapper-'+this.element.id_element;
    this.canvasWrapper = $(id)[0];
    let id_element = this.element.id_element;
    this.canvas = new fabric.StaticCanvas('canvasID-'+this.element.id_element, {
      preserveObjectStacking: true,
      height: 180,
      width: 100
    });
    let marker : Content = this.element.content.find(item => (item.id_content_type == 2));
    if(marker != undefined) {
      this.createCanvas(marker);
    }
  }

  createCanvas(marker : Content) {
    this.aspectRatio = 0;
    if(this.canvas != undefined) {
        this.canvas.clear();
    }
    let id_element = this.element.id_element;
    fabric.Image.fromURL(this.helperService.determinateHost()+"/storage/"+marker.resource,function(image){
      window['angularComponentRef-'+id_element].ngZone.run(()=>{
        let scale = 100 / image.width;
        image.set({
          scaleX: scale,
          scaleY: scale,
          selectable : false,
        }); 
        window['angularComponentRef-'+id_element].component.canvas.add(image);  
        image.center();
      })
    });
    this.canvas.renderAll();
  }
}
