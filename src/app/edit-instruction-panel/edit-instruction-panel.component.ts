import { Component, OnInit } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MarkerService } from 'src/services/marker.service';
import { Content } from 'src/modal/content';
import { faVideo, faSpinner, faImage, faTrash, faEdit} from '@fortawesome/free-solid-svg-icons';
import { Element } from 'src/modal/element';
import { ContentOptions } from 'src/modal/content-options';
import { ContentService } from 'src/services/content.service';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-edit-instruction-panel',
  templateUrl: './edit-instruction-panel.component.html',
  styleUrls: ['./edit-instruction-panel.component.css']
})
export class EditInstructionPanelComponent implements OnInit {
  id_element_parent : number;
  faVideo = faVideo;
  faSpinner = faSpinner;
  faImage = faImage;
  faTrash = faTrash;
  faEdit = faEdit;
  options : any;

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
    public markerService : MarkerService,
    public contentService : ContentService
  ) { }

  ngOnInit() {
    this.progressService.progressActiveByName('default');
    this.errorService.clean();
    this.route.params.subscribe(params => {
      let id_element : number = parseInt(params["id_element"]);
      this.id_element_parent = parseInt(params["id_element_parent"]);
      this.progressService.progressInactiveByName('default');
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.getById(id_element,this.helperService.determinateHost(),httpOptions).then(() => {
        this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
              this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
                this.elementTypeService.determinateMenu(-1);
                this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
                this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
                this.elementTypeService.determinateNextElementTypes(this.elementService.currentElement.id_element_type);
                this.elementService.addIdParent(this.id_element_parent);
                if(this.elementService.getLocal('instruction-gpar') == null || this.elementService.getLocal('instruction-gpar') == undefined || this.markerService.markers.length <= 0) {
                  if(this.markerService.markers.length <= 0 && (this.elementService.getLocal('instruction-gpar') == null || this.elementService.getLocal('instruction-gpar') == undefined)) {
                    for (let index = 0; index < this.elementService.currentElement.content.length; index++) {
                      if(this.elementService.currentElement.content[index].id_content_type == 2) {
                        console.log('content index ::',this.elementService.currentElement.content[index]);
                        let content : Content = this.elementService.currentElement.content[index].instanceOfElementsInMarker();
                        console.log('content ::',content);
                        this.markerService.markers.push(content);
                      }
                    }
                  } else if (this.elementService.getLocal('instruction-gpar') != null && this.elementService.getLocal('instruction-gpar') != undefined){
                    let element : Element = this.elementService.getInstanceOfPlaneObject(this.elementService.getLocal('instruction-gpar'));
                    for (let index = 0; index < element.content.length; index++) {
                      if(element.content[index].id_content_type == 2) {
                        console.log('content index ::',this.elementService.currentElement.content[index]);

                        let content : Content = element.content[index].instanceOfElementsInMarker();
                        console.log('content ::',content);

                        this.markerService.markers.push(content);
                      }
                    }
                  }
                }

                this.elementService.currentElement.content.forEach(content => {
                  if(content.id_content_type == 11 || content.id_content_type == 3 || content.id_content_type == 23 || content.id_content_type == 29 || content.id_content_type == 1) {
                    if(content.id_content_parent != undefined && content.id_content_parent != null) {
                      for (let index = 0; index < this.markerService.markers.length; index++) {
                        for (let index_2 = 0; index_2 <  this.markerService.markers[index].elementsInMarker.length; index_2++) {
                          if(this.markerService.markers[index].elementsInMarker[index_2].id_content == content.id_content_parent ) {
                            this.markerService.markers[index].elementsInMarker[index_2].childs.push(this.contentService.getInstanceOfPlaneObject(content));
                          }
                        }
                      }
                    }
                  }
                });
                this.markerService.markers = this.markerService.markers.sort((a,b) => {
									return a.options.position-b.options.position;
								});
                this.progressService.progressInactiveByName('default');
              })
            })
          })
        })
      })
    })

    this.options = {
			onUpdate: (event: any) => {
				let data = {
					contents : []
				}
				for (let index = 0; index < this.markerService.markers.length; index++) {
      		data.contents.push({id_content : this.markerService.markers[index].id_content, position : index});
          let content = this.elementService.currentElement.content.find(item => (item.id_content ==  this.markerService.markers[index].id_content));
          if(content != null) {
            let indexItem = this.elementService.currentElement.content.indexOf(content);
            this.elementService.currentElement.content[indexItem].options.position = index;
            this.markerService.markers[index].options.position = index;
          }
        }
        let httpOptions  = this.helperService.getHeaders();
				this.markerService.savePositionOfMarker(data,this.helperService.determinateHost(),httpOptions).then((data) => {
        }, (error) => {
        })

			}
		};
  }

  cancel() {
    this.elementService.currentElement = null;
    this.markerService.markers = [];
    this.elementService.saveLocal('instruction-gpar')
    this.router.navigate(['/elementos', (this.id_element_parent != undefined)? this.id_element_parent : 0]);
  }

  save(element : Element) {
    this.progressService.progressActiveByName('save');
    this.progressService.msg = "Guardando..";
    if(this.errorService.validateElement(element)) {
      if(this.markerService.markers.length > 0) {
        this.elementService.currentElement.id_version = 2;
        let httpOptions  = this.helperService.getHeaders();
        this.elementService.save(this.elementService.currentElement,this.helperService.determinateHost(),httpOptions).then(
          (data) => {
            this.elementService.currentElement.content = element.content;
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
              this.saveAllMarker(this.markerService.markers).then((data) => {
                this.contentService.createZip(this.id_element_parent,this.helperService.determinateHost(false),'ios');
                this.contentService.createZip(this.id_element_parent,this.helperService.determinateHost(false),'android');
                this.elementService.currentElement = null;
                this.markerService.markers = [];
                this.markerService.currentMarker = null;
                this.elementService.saveLocal('instruction-gpar')
                this.progressService.progressInactiveByName('save');
               this.router.navigate(['/elementos',0]);
              }, (error) => {})
            })
          },
          (error) => {}
        )
      } else {
        this.errorService.errors.push("Tienes que tener al menos un marcador");
      }
    } else {
      this.progressService.progressInactiveByName('save');
    }
  }

  saveAllMarker(markers : Array<Content>, index = 0) {
    let promise = new Promise((resolve, reject) => {
       if(index < markers.length) {
         markers[index].id_version = 2;
         this.saveMarker(markers[index]).then(() => {
            console.log('index plus ::', index);
            index++;
            this.saveAllMarker(markers, index).then(() => {
              resolve();
            }, () =>{})
         }, (error) => {})
       } else {
         resolve();
       }
    })

    return promise;
  }

  saveMarker(currentMarker : Content) {
    let promise = new Promise((resolve, reject) => {
      let marker : Content = this.contentService.getInstanceOfPlaneObject(currentMarker);
      marker.id_version = 2;
      let httpOptions  = this.helperService.getHeaders();
        this.contentService.save(marker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
            (data) => {
                if(currentMarker.elementsInMarker.length > 0) {
                  this.markerService.currentMarker = currentMarker;
                  this.saveElementsInMarker(currentMarker).then(() => { console.log(':: Markers saved ::');resolve()},(e) => {reject(e)})
                } else {
                  resolve();
                }
            },
            (error) => {
                reject(error);
            }
        )
    })
    return promise;
  }

  saveElementsInMarker(marker : Content, index = 0) {
    let promise = new Promise((resolve, reject) => {
      if(index < marker.elementsInMarker.length) {
        let newElementInMarker : Content = this.contentService.getInstanceOfPlaneObject( marker.elementsInMarker[index]);
        newElementInMarker.id_version = 2;
        newElementInMarker.childs = marker.elementsInMarker[index].childs;
        newElementInMarker.sceneXR = marker.elementsInMarker[index].sceneXR;
        newElementInMarker.sceneVR = marker.elementsInMarker[index].sceneVR;
        this.storageElementInMarker(newElementInMarker).then(() => {
          index++;
          this.saveElementsInMarker(marker,index).then(() => resolve(), (e) => {reject(e)})
        }, (e) => { reject(e)})
      } else {
        resolve()
      }
    })
    return promise;
  }

  storageElementInMarker(elementInMarker : Content) {
    let promise = new Promise((resolve, reject) => {
      let httpOptions  = this.helperService.getHeaders();
      this.contentService.save(elementInMarker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
        () => {
          this.elementService.currentElement.content =  this.generateChilds(elementInMarker.childs);
          this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
            if(elementInMarker.sceneXR != undefined) {
              elementInMarker.sceneXR = this.elementService.getInstanceOfPlaneObject(elementInMarker.sceneXR);
              elementInMarker.sceneXR.id_version = 2;
              this.satorageXR(elementInMarker.sceneXR).then(() => {  console.log('Element in marker saved ::', elementInMarker); resolve() }, (error) => { reject(error)})
            } else if(elementInMarker.sceneVR != undefined) {
              elementInMarker.sceneVR = this.elementService.getInstanceOfPlaneObject(elementInMarker.sceneVR);
              elementInMarker.sceneVR.id_version = 2;
              this.satorageVR(elementInMarker.sceneVR).then(() => { resolve() }, (error) => { reject(error)})
            } else {
              resolve();
            }
          })
        }
      )
    })
    return promise;
  }

  generateChilds(childs : Array<Content>) : Array<Content>{
    let childsIntance : Array<Content> = [];
    childs.forEach(child => {
      child = this.contentService.getInstanceOfPlaneObject(child);
      child.id_version = 2;
      childsIntance.push(child);
    })
    return childsIntance;
  }

  satorageXR(sceneXR : Element)  {
    let promise = new Promise((resolve, reject) => {
      if(sceneXR instanceof Element) {
        let httpOptions  = this.helperService.getHeaders();
          this.elementService.save(sceneXR, this.helperService.determinateHost(),httpOptions).then(
              (data) => {
                  this.elementService.currentElement.content =  sceneXR.content;
                  this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
                    if(sceneXR.gpar != undefined) {
                      sceneXR.gpar = this.elementService.getInstanceOfPlaneObject(sceneXR.gpar);
                      sceneXR.gpar.id_version = 2;
                      this.saveGPARScene(sceneXR.gpar).then((data) => { resolve(data)},(error) => {reject(error)})
                    } else {
                      resolve();
                    }
                  })
              },
              (error) => {
                reject(error);
              }
          )
      } else {
        resolve();
      }
    })
    return promise;
  }

  satorageVR(sceneVR : Element)  {
    let promise = new Promise((resolve, reject) => {
      console.log('VR Scene ::',sceneVR);
      if(sceneVR instanceof Element) {
        let httpOptions  = this.helperService.getHeaders();
          this.elementService.save(sceneVR, this.helperService.determinateHost(),httpOptions).then(
              (data) => {
                console.log('data save VR Scene ::', data);
                  this.elementService.currentElement.content =  sceneVR.content;
                  console.log('data save VR Scene contents::', this.elementService.currentElement.content );
                  this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then((data) => {
                      resolve(data);
                  })
              },
              (error) => {
                reject(error);
              }
          )
      } else {
        resolve();
      }
    })
    return promise;
  }

  saveGPARScene(gpar : Element) {
    let promise = new Promise((resolve, reject) => {
      if(gpar instanceof Element) {
        gpar.id_version = 2;
        let httpOptions  = this.helperService.getHeaders();
        this.elementService.save(gpar, this.helperService.determinateHost(),httpOptions).then((data) =>{
          this.elementService.currentElement.content = [];
          gpar.content.forEach(item => {
            if(item.id_content != this.markerService.currentMarker.id_content) {
              this.elementService.currentElement.content.push(item);
            }
          });
          this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then((data) => {
            console.log('data save GPAR Scene ::', data);
            resolve()
          }, (error) => { reject(error)})
        })
      }
    })
    return promise;
  }
}
