import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInstructionPanelComponent } from './edit-instruction-panel.component';

describe('EditInstructionPanelComponent', () => {
  let component: EditInstructionPanelComponent;
  let fixture: ComponentFixture<EditInstructionPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInstructionPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInstructionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
