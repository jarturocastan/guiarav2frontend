import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { faSadTear} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  faSadTear = faSadTear;
  code : number;
  msg : String;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.code = parseInt(params['code']);
      this.msg = params['msg'].replace(new RegExp('_%', 'g'),' ');
    })
  }

  back() {
    const dataToken = JSON.parse(localStorage.getItem('APICredentials'));
    switch (this.code) {
      case 404:
        if(dataToken == null || dataToken == undefined) {
          this.router.navigate(['/login']);
        } else {
          this.router.navigate(['/elementos',0]);
        }
        break;

      default:
        break;
    }
  }
}
