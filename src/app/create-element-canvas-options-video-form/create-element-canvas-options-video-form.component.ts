import { Component, OnInit } from '@angular/core';
import { MarkerService } from 'src/services/marker.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { Language } from 'src/modal/language';
import { System } from 'src/modal/system';
import { ContentType } from 'src/modal/content-type';
import { ElementService } from 'src/services/element.service';
import { Content } from 'src/modal/content';
import { ContentOptions } from 'src/modal/content-options';
import { ContentTypeService } from 'src/services/content-type.service';
import { ContentService } from 'src/services/content.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-create-element-canvas-options-video-form',
  templateUrl: './create-element-canvas-options-video-form.component.html',
  styleUrls: ['./create-element-canvas-options-video-form.component.css']
})
export class CreateElementCanvasOptionsVideoFormComponent implements OnInit {
  faTrash = faTrash;

  constructor(
    public markerService : MarkerService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public elementService : ElementService,
    public contentTypeService : ContentTypeService,
    public contentService : ContentService,
    public helperService : HelperService,
    public progressService : ProgressService
  ) { }

  ngOnInit() {

  }

  addSubtitle(subtitle : String,language : Language,system : System,contentType : ContentType) {
    let httpOptions  = this.helperService.getHeaders();
    let code = contentType.options.code;
    this.progressService.progressActiveByName(code);
    let content : Content = new Content(
      null,
      subtitle,
      contentType.id_content_type,
      language.id_language,
      this.elementService.currentElement.id_element,
      system.id_system,
      new ContentOptions(0,0,null,null),1,1, this.markerService.elementInMarker.id_content,[]);
    this.contentService.save(content.toFormData(),this.helperService.determinateHost(),httpOptions)
      .then(
        (data) => {
          content.id_content = this.contentService.content.id_content;
          content.resource = this.contentService.content.resource;
          this.markerService.elementInMarker.childs.push(content);
          this.progressService.progressInactiveByName(code);
        },
        (error) => {}
      )
  }

  delete(id_content : number) {
    let code = this.contentTypeService.findContentTypeById(11).options.code;
    this.progressService.progressActiveByName(code);
    let httpOptions  = this.helperService.getHeaders();
    let content = this.markerService.elementInMarker.childs.find(item => (item.id_content == id_content));
    this.contentService.delete(id_content,this.helperService.determinateHost(),httpOptions)
      .then(
        (data) => {
          let index = this.markerService.elementInMarker.childs.indexOf(content);
          this.markerService.elementInMarker.childs.splice(index,1);
          this.progressService.progressInactiveByName(code);
        },
        (error) =>{}
      )
  }

}
