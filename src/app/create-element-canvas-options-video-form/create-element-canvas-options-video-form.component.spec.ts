import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateElementCanvasOptionsVideoFormComponent } from './create-element-canvas-options-video-form.component';

describe('CreateElementCanvasOptionsVideoFormComponent', () => {
  let component: CreateElementCanvasOptionsVideoFormComponent;
  let fixture: ComponentFixture<CreateElementCanvasOptionsVideoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateElementCanvasOptionsVideoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateElementCanvasOptionsVideoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
