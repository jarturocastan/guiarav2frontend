import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorService } from 'src/services/error.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { SystemService } from 'src/services/system.service';
import { LanguageService } from 'src/services/language.service';
import { ProgressService } from 'src/services/progress.service';
import { HelperService } from 'src/services/helper.service';
import { ElementOptions } from 'src/modal/element-options';
import { Element } from 'src/modal/element';
import { MarkerService } from 'src/services/marker.service';
import { faFileArchive, faSpinner, faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-create-element-scene-xr-canvas-form',
  templateUrl: './create-element-scene-xr-canvas-form.component.html',
  styleUrls: ['./create-element-scene-xr-canvas-form.component.css']
})
export class CreateElementSceneXrCanvasFormComponent implements OnInit {
  faFileArchive = faFileArchive;
  faSpinner = faSpinner;
  faCheck = faCheck;
  @Output()  cancelElementInMarker = new EventEmitter();
  @Output()  saveSceneXRInMarker = new EventEmitter();

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public markerService : MarkerService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
  ){ }

  ngOnInit() {
    this.errorService.clean();
    let element = new Element(null,4,[],new ElementOptions(false,false),1,1,this.elementService.getLocal('instruction-gpar').id_element, this.markerService.elementInMarker.id_content);
    let httpOptions  = this.helperService.getHeaders();
    this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(() => {
      this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
        this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
              this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
              this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
              this.contentTypeService.determinateContentTypeCombobox(this.elementService.currentElement.id_element_type);
            });
          });
        });
      });
    });
  }

  clenContent() {
    this.elementService.currentElement.content = [];
  }

  cancel() {
    this.cancelElementInMarker.emit({});
  }

  save(element : Element) {
    if(this.errorService.validateElement(element)) {
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
          this.elementService.currentElement.content = element.content;
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
            this.markerService.elementInMarker.sceneXR = this.elementService.currentElement;
            this.saveSceneXRInMarker.emit({});
          })
        },
        (error) => {}
      )
    }
  }

}
