import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateElementSceneXrCanvasFormComponent } from './create-element-scene-xr-canvas-form.component';

describe('CreateElementSceneXrCanvasFormComponent', () => {
  let component: CreateElementSceneXrCanvasFormComponent;
  let fixture: ComponentFixture<CreateElementSceneXrCanvasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateElementSceneXrCanvasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateElementSceneXrCanvasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
