import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementCanvasCardMiniComponent } from './element-canvas-card-mini.component';

describe('ElementCanvasCardMiniComponent', () => {
  let component: ElementCanvasCardMiniComponent;
  let fixture: ComponentFixture<ElementCanvasCardMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementCanvasCardMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementCanvasCardMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
