import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, NgZone } from '@angular/core';
import { faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Content } from 'src/modal/content';
import { ContentTypeService } from 'src/services/content-type.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { ContentType } from 'src/modal/content-type';
import { ContentService } from 'src/services/content.service';
import { MarkerService } from 'src/services/marker.service';
import  'fabric';
declare const fabric: any;
declare var $:any;

@Component({
  selector: 'app-element-canvas-card-mini',
  templateUrl: './element-canvas-card-mini.component.html',
  styleUrls: ['./element-canvas-card-mini.component.css']
})
export class ElementCanvasCardMiniComponent implements OnInit, AfterViewInit {
  faTrash = faTrash;
  faEdit = faEdit;
  @Input() elementInMarker : Content;
  @Output() updateCanvas = new EventEmitter();
  @Output() updateElementInMarker = new EventEmitter();
  @Output() deleteElementInMarker = new EventEmitter();
  @Input() hide_controls : Boolean = false;
  canvas : any;
  languageName : String;
  canvasWrapper: any;
  aspectRatio : Number;

  constructor(
    public contentTypeService : ContentTypeService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public contentService : ContentService,
    public markerService : MarkerService,
    public ngZone : NgZone,
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {

    if(this.elementInMarker.id_content_type != 12) {
      window['angularComponentRef-'+this.elementInMarker.id_content] = { component: this, ngZone: this.ngZone};
      let id = '#canvasWrapper-'+this.elementInMarker.id_content;
      this.canvasWrapper = $(id)[0];
      let id_content = this.elementInMarker.id_content;
      this.canvas = new fabric.StaticCanvas('canvasID-'+this.elementInMarker.id_content, {
        preserveObjectStacking: true,
        height: 100,
        width: 100
      });
      this.createCanvas(this.elementInMarker);
    }
  }

  findContentTypeNameById(id_content_type : number) : String {
    let contentType = this.contentTypeService.findContentTypeById(id_content_type);
    if(contentType instanceof ContentType) {
      return contentType.name;
    }
  }

  delete(id_content : number) {

    let r = confirm("¿Seguro que deseas eliminar este elemento?");
    if (r == true) {
      let content : Content = this.markerService.currentMarker.elementsInMarker.find(item => (item.id_content == id_content));
      if(content != undefined) {
        this.progressService.msg = "Eliminado elemento";
        this.progressService.progressActiveByName('default');
        let httpOptions  = this.helperService.getHeaders();
        this.contentService.delete(content.id_content, this.helperService.determinateHost(),httpOptions)
          .then(
            (data) => {
              let index : number = this.markerService.currentMarker.elementsInMarker.indexOf(content);
              this.markerService.currentMarker.elementsInMarker.splice(index,1);
              this.markerService.addCurrentMarker(this.markerService.currentMarker);
              this.progressService.progressInactiveByName('default');
              this.deleteElementInMarker.emit({});
            },
            (error) => {}
          )

      }
    }
  }

  createCanvas(image : Content) {
    this.aspectRatio = 0;
    if(this.canvas != undefined) {
        this.canvas.clear();
    }
    let name = 'angularComponentRef-'+this.elementInMarker.id_content;
    if(this.elementInMarker.id_content_type != 12) {
      let url;
      if(image.id_content_type == 1) {
        url = this.helperService.determinateHost(false)+"/storage/image/file.png";
      } else   if(image.id_content_type == 30) {
        if(image.options.action == "Escena VR") {
          url = this.helperService.determinateHost(false)+"/storage/image/file.png";
        } else if(image.options.action == "Play/PauseVideo") {
          url = this.helperService.determinateHost(false)+"/storage/"+image.childs[0].resource;
        }else {
          url = this.helperService.determinateHost(false)+"/storage/image/ninguno.png";
        }
      }  else {
        url = this.helperService.determinateHost(false)+"/storage/"+image.resource;
      }


      fabric.Image.fromURL(url,function(image){
        window[name].ngZone.run(()=>{
          let percentageWidth  = (200 * image.width)  / 100;
          let percentageHeight = (200 * image.height) /100;
          image.scaleToWidth(80);
          image.set({
            selectable : false,
            percentage_width:percentageWidth,
            percentage_height:percentageHeight
          });
          window[name].component.canvas.add(image);
          image.center();
        })
      });
    }
    window[name].ngZone.runOutsideAngular(() => {
      fabric.util.requestAnimFrame(function render() {
          window[name].component.canvas.renderAll();
          fabric.util.requestAnimFrame(render);
      });
    });
  }

  addFabricVideo(content : Content) {
    let url : string = this.helperService.determinateHost(false)+"/storage/"+content.resource;
    let videoElement = null;
    let name = 'angularComponentRef-'+this.elementInMarker.id_content;
    this.getVideoElement(url).then((video) => {
        videoElement = video;
        fabric.Image.fromURL(this.helperService.determinateHost(false)+"/storage/image/file.png", function(image){
                let scale =  180 /  videoElement.videoWidth;
                let video = new fabric.Image(videoElement.el,{
                    gui_img :url,
                    width: videoElement.videoWidth,
                    height: videoElement.videoHeight,
                    scaleX: scale,
                    scaleY: scale,
                    hasRotatingPoint : false
                });
                image.set({
                    gui_img :url,
                    visible : false,
                    scaleX: scale,
                    scaleY: scale,
                    width: videoElement.videoWidth,
                    height: videoElement.videoHeight,
                    hasRotatingPoint : false
                });

                let group = new fabric.Group([image,video], {
                    selectable: false,
                    objectCaching: false,
                    scaleX: scale,
                    scaleY: scale,
                    hasRotatingPoint : false,
                    width: videoElement.videoWidth,
                    height: videoElement.videoHeight,
                });

                window[name].component.canvas.add(group);
                group.center();
        });
    });
}

  getVideoElement(url) {
      let promise = new Promise((resolve, reject) => {

          var videoE = document.createElement('video');
          videoE.width = 450;
          videoE.height = 285;
          videoE.muted = true;
          var source = document.createElement('source');
          source.src = url;
          source.type = 'video/mp4';
          videoE.appendChild(source);
          let video = {
              videoHeight : 0,
              videoWidth : 0,
              el : videoE
          }
          videoE.addEventListener( "loadedmetadata", function (e) {
              video.videoHeight = this.videoHeight;
              video.videoWidth = this.videoWidth;
              resolve(video);
          }, false );

      })

      return promise;
  }
  update(id_content : number) {
    this.updateElementInMarker.emit({ 'id_content' : id_content });
  }



  determinateUrl(content : Content) {
    if(content.id_content_type == 30) {
      return content.childs[0].resource
    } else {
      return content.resource
    }
  }
}
