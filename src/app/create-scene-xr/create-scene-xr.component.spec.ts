import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSceneXRComponent } from './create-scene-xr.component';

describe('CreateSceneXRComponent', () => {
  let component: CreateSceneXRComponent;
  let fixture: ComponentFixture<CreateSceneXRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSceneXRComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSceneXRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
