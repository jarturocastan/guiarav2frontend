import { Component, OnInit } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorService } from 'src/services/error.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { SystemService } from 'src/services/system.service';
import { LanguageService } from 'src/services/language.service';
import { ProgressService } from 'src/services/progress.service';
import { HelperService } from 'src/services/helper.service';
import { ElementOptions } from 'src/modal/element-options';
import { Element } from 'src/modal/element';
import { faImage, faSpinner, faFileArchive } from '@fortawesome/free-solid-svg-icons';

declare var videojs: any;

@Component({
  selector: 'app-create-scene-xr',
  templateUrl: './create-scene-xr.component.html',
  styleUrls: ['./create-scene-xr.component.css']
})
export class CreateSceneXRComponent implements OnInit {
  languageName : String;
  faSpinner = faSpinner;
  faImage = faImage;
  faFileArchive = faFileArchive;
  id_element_parent : number;

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.progressService.progressActiveByName('default');
    this.errorService.clean();
    this.route.params.subscribe(params => {
      let element = new Element(null,4,[],new ElementOptions(false,false),1,1,parseInt(params['id_element_parent']));
      this.id_element_parent = parseInt(params['id_element_parent']);
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(() => {
        this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
              this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
                this.elementTypeService.determinateMenu(-1);
                this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
                this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
                this.contentTypeService.determinateContentTypeCombobox(this.elementService.currentElement.id_element_type);
                this.progressService.progressInactiveByName('default');
              });
            });
          });
        });
      });
    })
  }

  clenContent() {
    let  keys : Array<String> = Object.keys(Object.assign([], videojs.getPlayers()));
    keys.forEach(key => {
      videojs.getPlayers()[""+key].pause();
      delete videojs.getPlayers()[""+key];
    })

    this.elementService.currentElement.content = [];

  }

  cancel() {
    this.router.navigate(['/elementos',this.id_element_parent]);
  }

  save(element : Element) {
    if(this.errorService.validateElement(element)) {
      element.id_version = 2;
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
            this.elementService.currentElement.content = element.content;
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
              if(this.contentTypeService.contentType.id_content_type == 16) {
                this.router.navigate(['/crear/escena-gpar',this.elementService.currentElement.id_element]);
              } else {
                this.router.navigate(['/elementos',0]);
              }
            })
        },
        (error) => {}
      )
    }
  }
}
