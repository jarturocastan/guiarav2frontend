import { Component, OnInit } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Element } from 'src/modal/element';
import { ElementOptions } from 'src/modal/element-options';
import { faVideo, faSpinner, faImage, faTrash, faEdit} from '@fortawesome/free-solid-svg-icons';
import { MarkerService } from 'src/services/marker.service';
import { ContentService } from 'src/services/content.service';
import { Content } from 'src/modal/content';
import { reject } from 'q';

@Component({
  selector: 'app-create-instruction-panel',
  templateUrl: './create-instruction-panel.component.html',
  styleUrls: ['./create-instruction-panel.component.css']
})
export class CreateInstructionPanelComponent implements OnInit {
  faVideo = faVideo;
  faSpinner = faSpinner;
  faImage = faImage;
  faTrash = faTrash;
  faEdit = faEdit;
  id_element_parent : number;
  options : any;

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public contentService : ContentService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
    public markerService : MarkerService
  ) { }

  ngOnInit() {
    this.progressService.type = 0;
    this.progressService.progressActiveByName('default');
    this.errorService.clean();
    this.route.params.subscribe(params => {
      let element =  null;
      if(this.elementService.getLocal('instruction-gpar') == null || this.elementService.getLocal('instruction-gpar') == undefined) {
        element = new Element(null,3,[],new ElementOptions(false,false),1,1,parseInt(params['id_element_parent']));
      } else {
        element = this.elementService.getInstanceOfPlaneObject(this.elementService.getLocal('instruction-gpar'));
        element.id_element_parent = parseInt(params['id_element_parent']);
      }
      console.log('Element instrucction::', element);

      this.id_element_parent = parseInt(params['id_element_parent']);
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(() => {
        this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
              this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
                this.elementService.currentElement.content = element.content;
                this.elementService.currentElement.id_element_parent =  parseInt(params['id_element_parent']);;
                this.elementService.currentElement.id_content_parent = null;
                this.elementService.currentElement.instanceOfContent();
                this.elementService.addIdParent(this.id_element_parent);
                this.elementTypeService.determinateMenu(-1);
                this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
                this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
                this.elementTypeService.determinateNextElementTypes(this.elementService.currentElement.id_element_type);

                if(this.elementService.getLocal('instruction-gpar') == null || this.elementService.getLocal('instruction-gpar') == undefined || this.markerService.markers.length <= 0) {
                  if(this.markerService.markers.length <= 0 && (this.elementService.getLocal('instruction-gpar') == null || this.elementService.getLocal('instruction-gpar') == undefined)) {
                    for (let index = 0; index < this.elementService.currentElement.content.length; index++) {
                      if(this.elementService.currentElement.content[index].id_content_type == 2) {
                        let content : Content = this.elementService.currentElement.content[index].instanceOfElementsInMarker();
                        this.markerService.markers.push(content);
                      }
                    }
                  } else if (this.elementService.getLocal('instruction-gpar') != null && this.elementService.getLocal('instruction-gpar') != undefined){
                    let element : Element = this.elementService.getInstanceOfPlaneObject(this.elementService.getLocal('instruction-gpar'));
                    for (let index = 0; index < element.content.length; index++) {
                      if(element.content[index].id_content_type == 2) {
                        let content : Content = element.content[index].instanceOfElementsInMarker();
                        this.markerService.markers.push(content);
                      }
                    }
                  }
                }


                this.elementService.currentElement.content.forEach(content => {
                  if(content.id_content_type == 11 || content.id_content_type == 3 || content.id_content_type == 23) {
                    if(content.id_content_parent != undefined && content.id_content_parent != null) {
                      for (let index = 0; index < this.markerService.markers.length; index++) {
                        for (let index_2 = 0; index_2 <  this.markerService.markers[index].elementsInMarker.length; index_2++) {
                          if(this.markerService.markers[index].elementsInMarker[index_2].id_content == content.id_content_parent ) {
                            this.markerService.markers[index].elementsInMarker[index_2].childs.push(content);
                          }
                        }
                      }
                    }
                  }
                });
                this.markerService.markers = this.markerService.markers.sort((a,b) => {
									return a.options.position-b.options.position;
                });


                this.progressService.progressInactiveByName('default');
              })
            })
          })
        })

      })

    })


		this.options = {
			onUpdate: (event: any) => {
				let data = {
					contents : []
				}
				for (let index = 0; index < this.markerService.markers.length; index++) {
      		data.contents.push({id_content : this.markerService.markers[index].id_content, position : index});
          let content = this.elementService.currentElement.content.find(item => (item.id_content ==  this.markerService.markers[index].id_content));
          if(content != null) {
            let indexItem = this.elementService.currentElement.content.indexOf(content);
            this.elementService.currentElement.content[indexItem].options.position = index;
          }
        }
        let httpOptions  = this.helperService.getHeaders();
				this.markerService.savePositionOfMarker(data,this.helperService.determinateHost(),httpOptions).then(() => {})

			}
		};
  }

  cancel() {
    this.elementService.currentElement = null;
    if(this.markerService.markers.length > 0) {
      this.markerService.markers.forEach(marker => {
        let httpOptions  = this.helperService.getHeaders();
        this.contentService.delete(marker.id_content,this.helperService.determinateHost(),httpOptions).then(() =>{})
      })
    }
    this.elementService.saveLocal('instruction-gpar')
    this.router.navigate(['/elementos',this.id_element_parent]);
  }

  cancelMarkers() {

  }

  save(element : Element) {
    this.progressService.progressActiveByName('save');
    this.progressService.msg = "Guardando..";
    if(this.errorService.validateElement(element)) {
      if(this.markerService.markers.length > 0) {
        this.elementService.currentElement.id_version = 2;
        let httpOptions  = this.helperService.getHeaders();
        this.elementService.save(this.elementService.currentElement,this.helperService.determinateHost(),httpOptions).then(
          (data) => {
            this.elementService.currentElement.content = element.content;
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
              this.saveAllMarker(this.markerService.markers).then((data) => {
                this.contentService.createZip(this.id_element_parent,this.helperService.determinateHost(false),'ios');
                this.contentService.createZip(this.id_element_parent,this.helperService.determinateHost(false),'android');
                this.elementService.currentElement = null;
                this.markerService.markers = [];
                this.markerService.currentMarker = null;
                this.elementService.saveLocal('instruction-gpar')
                this.progressService.progressInactiveByName('save');
                this.router.navigate(['/elementos',0]);
              }, (error) => {})
            })
          },
          (error) => {}
        )
      } else {
        this.errorService.errors.push("Tienes que tener al menos un marcador");
      }
    } else {
      this.progressService.progressInactiveByName('save');
    }
  }

  saveAllMarker(markers : Array<Content>) {
    let promise = new Promise((resolve, reject) => {
      let index = 0;
      console.log('markers to save ::', markers);
      markers.forEach(marker => {
        marker.id_version = 2;
        console.log('marker to save ::', marker);
        this.saveMarker(marker).then(()=> {
          if(index == (markers.length-1)) {
            resolve();
          }
          index++;
        }, (error) => {})
      })
    })

    return promise;
  }

  saveMarker(currentMarker : Content) {
    let promise = new Promise((resolve, reject) => {
      let marker : Content = this.contentService.getInstanceOfPlaneObject(currentMarker);
        let httpOptions  = this.helperService.getHeaders();
        this.contentService.save(marker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
            (data) => {
                if(currentMarker.elementsInMarker.length > 0) {
                  this.markerService.currentMarker = currentMarker;
                  this.saveElementsInMarker(currentMarker).then(() => { resolve()},(e) => {reject(e)})
                } else {
                  resolve();
                }
            },
            (error) => {
                reject(error);
            }
        )
    })
    return promise;
  }

  saveElementsInMarker(marker : Content) {
    let promise = new Promise((resolve, reject) => {
      let i = 0;
      marker.elementsInMarker.forEach(elementInMarker => {
        let newElementInMarker : Content = this.contentService.getInstanceOfPlaneObject(elementInMarker);
        newElementInMarker.id_version = 2;
        newElementInMarker.childs = elementInMarker.childs;
        newElementInMarker.sceneXR = elementInMarker.sceneXR;
        this.storageElementInMarker(newElementInMarker).then(() => {
          if(i == (marker.elementsInMarker.length -1)) {
            resolve();
          }
          i++;
        }, (e) => { reject(e)})

      })
    })
    return promise;
  }

  storageElementInMarker(elementInMarker : Content) {
    let promise = new Promise((resolve, reject) => {
      let httpOptions  = this.helperService.getHeaders();
      this.contentService.save(elementInMarker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
        () => {
          this.elementService.currentElement.content =  this.generateChilds(elementInMarker.childs);
          this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
            console.log('Element in marker ::', elementInMarker);
            if(elementInMarker.sceneXR != undefined) {
              elementInMarker.sceneXR = this.elementService.getInstanceOfPlaneObject(elementInMarker.sceneXR);
              elementInMarker.sceneXR.id_version = 2;
              this.satorageXR(elementInMarker.sceneXR).then(() => { resolve() }, (error) => { reject(error)})
            } else {
              resolve();
            }
          })
        }
      )
    })
    return promise;
  }

  generateChilds(childs : Array<Content>) : Array<Content>{
    let childsIntance : Array<Content> = [];
    childs.forEach(child => {
      child = this.contentService.getInstanceOfPlaneObject(child);
      child.id_version = 2;
      childsIntance.push(child);
    })
    return childsIntance;
  }

  satorageXR(sceneXR : Element)  {
    let promise = new Promise((resolve, reject) => {
      console.log('XR Scene ::',sceneXR);
      if(sceneXR instanceof Element) {
          let httpOptions  = this.helperService.getHeaders();
          this.elementService.save(sceneXR, this.helperService.determinateHost(),httpOptions).then(
              (data) => {
                  this.elementService.currentElement.content =  sceneXR.content;
                  this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
                    if(sceneXR.gpar != undefined) {
                      sceneXR.gpar = this.elementService.getInstanceOfPlaneObject(sceneXR.gpar);
                      sceneXR.gpar.id_version = 2;
                      this.saveGPARScene(sceneXR.gpar).then((data) => { resolve(data)},(error) => {reject(error)})
                    } else {
                      resolve();
                    }
                  })
              },
              (error) => {
                reject(error);
              }
          )
      } else {
        resolve();
      }
    })
    return promise;
  }

  saveGPARScene(gpar : Element) {
    let promise = new Promise((resolve, reject) => {
      console.log('GPAR Scene ::',gpar);
      if(gpar instanceof Element) {
        let httpOptions  = this.helperService.getHeaders();
        this.elementService.save(gpar, this.helperService.determinateHost(),httpOptions).then((data) =>{
          console.log('data save GPAR Scene ::', data);
          gpar.content.forEach(item => {
            if(item.id_content != this.markerService.currentMarker.id_content) {
              this.elementService.currentElement.content.push(item);
            }
          });
          this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then((data) => {
            resolve()
          }, (error) => { reject(error)})
        })
      }
    })
    return promise;
  }
}
