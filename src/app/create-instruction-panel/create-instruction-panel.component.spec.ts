import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInstructionPanelComponent } from './create-instruction-panel.component';

describe('CreateInstructionPanelComponent', () => {
  let component: CreateInstructionPanelComponent;
  let fixture: ComponentFixture<CreateInstructionPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateInstructionPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInstructionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
