import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { faSpinner, faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { ProgressService } from 'src/services/progress.service';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {
  faSpinner = faSpinner;
  faCheckCircle = faCheckCircle;
  
  constructor(
      public progressService : ProgressService
  ) { }

  ngOnInit() {
  }
}
