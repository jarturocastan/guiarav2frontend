import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MarkerService } from 'src/services/marker.service';
import { ProgressService } from 'src/services/progress.service';
import { HelperService } from 'src/services/helper.service';
import { ContentService } from 'src/services/content.service';


@Component({
  selector: 'app-panel-elements-canvas',
  templateUrl: './panel-elements-canvas.component.html',
  styleUrls: ['./panel-elements-canvas.component.css']
})
export class PanelElementsCanvasComponent implements OnInit {
  @Output() addElementInMarker = new EventEmitter();
  @Output() updateCanvas = new EventEmitter();
  @Output() updateElementInMarker = new EventEmitter();
  @Output() reloadElementInMarker = new EventEmitter();
  @Output() deleteElementInMarker = new EventEmitter();
  options : any;

  constructor(
    public markerService : MarkerService,
    public progressService : ProgressService,
    public helperService : HelperService,
    public contentService : ContentService
  ) {

  }

  ngOnInit() {
    this.options = {
			onUpdate: (event: any) => {
				let data = {
					contents : []
				}
				for (let index = 0; index < this.markerService.currentMarker.elementsInMarker.length; index++) {
					this.markerService.currentMarker.elementsInMarker[index].options["position"] = index;
					data.contents.push({id_content : this.markerService.currentMarker.elementsInMarker[index].id_content, position : index});
				}
        let httpOptions  = this.helperService.getHeaders();
        this.contentService.savePositionOfContents(data,this.helperService.determinateHost(),httpOptions).then(() => {})
        this.markerService.addCurrentMarker(this.markerService.currentMarker);
        this.reloadElementInMarker.emit({});
			}
		};
  }

  addElement() {
    this.addElementInMarker.emit({ show : true});
  }

  updateCanvasEvent(event) {
    this.updateCanvas.emit({});
  }

  deleteCanvas(event) {
    this.deleteElementInMarker.emit({});
  }

  updateElementInMarkerEvent(event) {
    this.updateElementInMarker.emit(event);
  }

}
