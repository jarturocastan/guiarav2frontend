import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelElementsCanvasComponent } from './panel-elements-canvas.component';

describe('PanelElementsCanvasComponent', () => {
  let component: PanelElementsCanvasComponent;
  let fixture: ComponentFixture<PanelElementsCanvasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelElementsCanvasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelElementsCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
