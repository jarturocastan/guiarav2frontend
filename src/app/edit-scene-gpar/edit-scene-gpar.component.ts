import { Component, OnInit } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { MarkerService } from 'src/services/marker.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Element } from 'src/modal/element';
import { ElementOptions } from 'src/modal/element-options';
import { faVideo, faSpinner, faImage, faTrash, faEdit, faFileArchive } from '@fortawesome/free-solid-svg-icons';
import { ContentService } from 'src/services/content.service';

@Component({
  selector: 'app-edit-scene-gpar',
  templateUrl: './edit-scene-gpar.component.html',
  styleUrls: ['./edit-scene-gpar.component.css']
})
export class EditSceneGPARComponent implements OnInit {
  faVideo = faVideo;
  faSpinner = faSpinner;
  faImage = faImage;
  faTrash = faTrash;
  faEdit = faEdit;
  faFileArchive = faFileArchive;
  id_element_parent : number;


  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public contentService : ContentService,
    public markerService : MarkerService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.progressService.progressActiveByName('default');
    this.errorService.clean();
    this.route.params.subscribe(params => {
      let id_element : number = parseInt(params["id_element"]);
      this.id_element_parent = parseInt(params["id_element_parent"]);
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.getById(id_element,this.helperService.determinateHost(),httpOptions).then(() => {
        this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
              this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
                this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
                this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
                this.contentTypeService.determinateContentTypeCombobox(this.elementService.currentElement.id_element_type);
                this.elementService.addIdParent(this.id_element_parent);

                this.progressService.progressInactiveByName('default');
              });
            });
          });
        });
      })

    });
  }

  cancel() {
    let scenexr : Element = JSON.parse(localStorage.getItem('scene-gpar'));
    if(scenexr != undefined && scenexr != null) {
      this.router.navigate(["/editar/escena-xr/",  scenexr.id_element, scenexr.id_element_parent]);
    } else {
      this.router.navigate(['/elementos',(this.id_element_parent != undefined)? this.id_element_parent : 0]);
    }
  }

  save(element : Element) {
    if(this.errorService.validateElement(element)) {
      element.id_version = 2;
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
            this.elementService.currentElement.content = element.content;
            let ids : Array<number> = [];
            this.elementService.currentElement.content.forEach(content => {
              ids.push(content.id_content);
            });
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
              this.contentService.deleteNotIn(ids,this.elementService.currentElement.id_element,this.helperService.determinateHost())
                .then(() => {
                  let scenexr : Element = JSON.parse(localStorage.getItem('scene-gpar'));
                  if(scenexr != undefined && scenexr != null) {
                    this.router.navigate(["/editar/escena-xr/", scenexr.id_element, scenexr.id_element_parent]);
                  } else {
                    this.router.navigate(['/elementos',(this.id_element_parent != undefined)? this.id_element_parent : 0]);
                  }
                });
          })
        },
        (error) => {}
      )
    }
  }
}
