import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSceneGPARComponent } from './edit-scene-gpar.component';

describe('EditSceneGPARComponent', () => {
  let component: EditSceneGPARComponent;
  let fixture: ComponentFixture<EditSceneGPARComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSceneGPARComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSceneGPARComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
