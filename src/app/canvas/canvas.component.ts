import { Component, OnInit, NgZone, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { ProgressService } from 'src/services/progress.service';
import { HelperService } from 'src/services/helper.service';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute, ResolveEnd } from '@angular/router';
import { MarkerService } from 'src/services/marker.service';
import  'fabric';
import { Content } from 'src/modal/content';
import { ContentService } from 'src/services/content.service';
import { Element } from 'src/modal/element';
declare const fabric: any;
declare var $:any;

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit, AfterViewInit {
    canvas : any;
    canvasWrapper: any;
    aspectRatio : Number;
    showForm : Boolean = false;
    showFormEdit : Boolean = false;
    loadCanvas : Boolean;
    status : number;
    constructor(
        public elementTypeService : ElementTypeService,
        public elementService : ElementService,
        public helperService : HelperService,
        public progressService : ProgressService,
        public languageService : LanguageService,
        public systemService : SystemService,
        public contentTypeService : ContentTypeService,
        public errorService : ErrorService,
        private router: Router,
        private route: ActivatedRoute,
        public markerService : MarkerService,
        public contentService : ContentService,
        public ngZone : NgZone,
    ) { }

    ngOnInit() {
        this.loadCanvas = true;
        this.progressService.clean();
        this.progressService.progressRun = false;
    }

    ngAfterViewInit() {
        this.route.params.subscribe(params => {
            this.status =  params['status'];
            window['angularComponentRef'] = { component: this, ngZone: this.ngZone};
            fabric.Object.prototype.transparentCorners = false;
            this.elementService.currentElement = this.elementService.getLocal('instruction-gpar');
            let httpOptions  = this.helperService.getHeaders();
            this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
                let objects = [];
                this.markerService.currentMarker = this.markerService.getCurrentMarker();
                if(this.markerService.currentMarker != undefined) {
                    this.canvas = new fabric.Canvas('canvasID', {
                        preserveObjectStacking: true,
                        height: 600,
                        width: 600,
                        skipTargetFind:  false,
                        renderOnAddRemove:false
                    });
                    this.createCanvas(this.markerService.getCurrentMarker());
                    this.canvas.on({
                    'object:selected': function(evt){

                    },
                    'object:modified': function(evt){

                        window['angularComponentRef'].component.updateInfoObject(evt);
                    },
                    'after:render': function(evt){
                    },
                        'object:moving': function (e) {
                            var obj = e.target;
                            if(obj.currentHeight > obj.canvas.height || obj.currentWidth > obj.canvas.width){
                                return;
                            }
                            obj.setCoords();
                            if(obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0){
                                obj.top = Math.max(obj.top, obj.top-obj.getBoundingRect().top);
                                obj.left = Math.max(obj.left, obj.left-obj.getBoundingRect().left);
                            }
                            if(obj.getBoundingRect().top+obj.getBoundingRect().height  > obj.canvas.height || obj.getBoundingRect().left+obj.getBoundingRect().width  > obj.canvas.width){
                                obj.top = Math.min(obj.top, obj.canvas.height-obj.getBoundingRect().height+obj.top-obj.getBoundingRect().top);
                                obj.left = Math.min(obj.left, obj.canvas.width-obj.getBoundingRect().width+obj.left-obj.getBoundingRect().left);
                            }
                        },
                        'object:added': function(object) {

                            let marker = window['angularComponentRef'].component.markerService.currentMarker;
                            objects.push(object.target);

                            if(objects.length == marker.elementsInMarker.length) {
                                objects = objects.sort(function(a,b){ return b["XZIndex"] - a["XZIndex"];})


                                objects.forEach(el => {
                                    window['angularComponentRef'].component.canvas.sendToBack(el)
                                });
                                objects = [];

                                window['angularComponentRef'].component.loadCanvas = false;
                            }
                        },
                        'object:scaling': function(object) {
                            var maxScaleX = 2;
                            var maxScaleY = 2;

                            object = object.target;

                            let actualWidth  = object.scaleX * object.width
                            let  actualHeight = object.scaleY * object.height;
                            if(actualWidth  >=  window['angularComponentRef'].component.canvas.width) {
                                object.set({ scaleX : window['angularComponentRef'].component.canvas.width/object.width })
                                object.centerH();
                            }

                            if(actualHeight  >=  window['angularComponentRef'].component.canvas.height) {
                                object.set({ scaleY : window['angularComponentRef'].component.canvas.height/object.height })
                                object.centerV();
                            }

                            var obj = object;
                            if(obj.currentHeight > obj.canvas.height || obj.currentWidth > obj.canvas.width){
                                return;
                            }
                            obj.setCoords();
                            if(obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0){
                                obj.top = Math.max(obj.top, obj.top-obj.getBoundingRect().top);
                                obj.left = Math.max(obj.left, obj.left-obj.getBoundingRect().left);
                            }
                            if(obj.getBoundingRect().top+obj.getBoundingRect().height  > obj.canvas.height || obj.getBoundingRect().left+obj.getBoundingRect().width  > obj.canvas.width){
                                obj.top = Math.min(obj.top, obj.canvas.height-obj.getBoundingRect().height+obj.top-obj.getBoundingRect().top);
                                obj.left = Math.min(obj.left, obj.canvas.width-obj.getBoundingRect().width+obj.left-obj.getBoundingRect().left);
                            }
                        }
                    });
                }
            })

            $(window).resize(function (){
                window['angularComponentRef'].component.responsiveCanvas( window['angularComponentRef'].component.canvas);
            });
        });
    }


    responsiveCanvas(){
        let factorWidth = this.canvasWrapper.clientWidth;

        if (this.canvas.width != factorWidth) {
            let scaleFactor = factorWidth / this.canvas.width;
            let objects = this.canvas.getObjects();
            for (let i in objects) {
                objects[i].scaleX = objects[i].scaleX * scaleFactor;
                objects[i].scaleY = objects[i].scaleY * scaleFactor;
                objects[i].left = objects[i].left * scaleFactor;
                objects[i].top = objects[i].top * scaleFactor;
                objects[i].setCoords();
            }
            this.canvas.setWidth(this.canvas.width * scaleFactor);
            this.canvas.setHeight(this.canvas.height * scaleFactor);
            this.canvas.renderAll();
            this.canvas.calcOffset();
        }
    }

    createCanvas(marker : Content) {
        this.canvasWrapper = $('#canvasWrapper')[0];
        this.aspectRatio = 0;
        console.log('canvas ::', this.canvasWrapper);
        this.canvas.clear();
        window['angularComponentRef'] = { component: this, ngZone: this.ngZone};
        fabric.Image.fromURL(this.helperService.determinateHost(false)+"/storage/"+marker.resource,function(img){
            window['angularComponentRef'].ngZone.run(()=>{
                let imgWidth = img.width;
                let  imgHeight = img.height;
                let aspectRatio = imgHeight/imgWidth;
                let canvasWidth = 600;
                let canvasHeight = 0;
                let scaleFactor = 0;
                if(img.width >= img.height) {
                  aspectRatio = imgWidth/imgHeight;
                  canvasHeight = canvasWidth * aspectRatio;
                  scaleFactor = canvasWidth/ imgWidth;
                } else {
                  canvasHeight = canvasWidth * aspectRatio;
                  scaleFactor = canvasWidth/ imgHeight;
                }
                img.set({
                    width: imgWidth,
                    height: imgHeight,
                    originX: 'left',
                    originY: 'top',
                    scaleX: scaleFactor,
                    scaleY:scaleFactor
                });
                console.log(img);
                window['angularComponentRef'].component.canvas.setWidth(imgWidth * scaleFactor);;
                window['angularComponentRef'].component.canvas.setHeight(imgHeight * scaleFactor);
                let bg = window['angularComponentRef'].component.canvas.setBackgroundImage(img, window['angularComponentRef'].component.canvas.renderAll.bind(window['angularComponentRef'].component.canvas));
               // $('#canvasWrapper').css("height", bg.backgroundImage.canvas.height);
               console.log(bg.backgroundImage.canvas.height)
            })

        });



        if(marker.elementsInMarker.length > 0) {
            this.addAllElementsInMarker(marker.elementsInMarker);
        } else {
            this.loadCanvas = false;
        }
        window['angularComponentRef'].ngZone.runOutsideAngular(() => {
            fabric.util.requestAnimFrame(function render() {
                window['angularComponentRef'].component.canvas.renderAll();
                fabric.util.requestAnimFrame(render);
            });
        });
    }


    centerImg(imgInstance,canvasWrapper){
        let scale = (canvasWrapper.clientWidth - canvasWrapper.clientWidth * .12) /  imgInstance.width;
        imgInstance.left            = canvasWrapper.clientWidth * .06;
        imgInstance.top             = canvasWrapper.clientHeight * .06;
        imgInstance.scaleY          = scale;
        imgInstance.scaleX          = scale;
        return imgInstance;
    }

    updatePercentages(selectedObj, container, index){
        let percentagesPosition = this.getPercentages(selectedObj.left , selectedObj.top, container);
        let percentagesSize     = this.getPercentages(selectedObj.width * selectedObj.scaleX, selectedObj.height * selectedObj.scaleY, container);
        selectedObj.percentage_left   = percentagesPosition.left;
        selectedObj.percentage_top    = percentagesPosition.top;
        selectedObj.percentage_width  = percentagesSize.left;
        selectedObj.percentage_height = percentagesSize.top;
        return selectedObj;
    }

    getPercentages(left, top, container){
        let percentageWidth  = (100 * left) / container.width;
        let percentageHeight = (100 * top) /container.height;

        return {left: percentageWidth, top: percentageHeight};
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        let m = Math.floor(Math.random() * (max - min)) + min;
        return m; //The maximum is exclusive and the minimum is inclusive
    }

    addAllElementsInMarker(elementsInMarker : Array<Content>) {
        elementsInMarker.sort((a,b) => {
            return a.options['position'] -b.options['position'];
        });

        for (let index = 0; index < elementsInMarker.length; index++) {
            this.addLastElementInMarker(elementsInMarker[index], index);
        }
    }

    addLastElementInMarker(elementInMarker : Content, index : number) {
        if(elementInMarker.id_content_type == 1) {
            this.addFabricAssetBundle(elementInMarker, index);
        } else if(elementInMarker.id_content_type == 12) {
            this.addFabricVideo(elementInMarker, index);
        } else if(elementInMarker.id_content_type == 10) {
            this.addFabricImage(elementInMarker, index);
          } else if(elementInMarker.id_content_type == 30) {
            this.addFabricNinguno(elementInMarker, index);
        }
    }

    addFabricAssetBundle(content : Content, index : number) {
        let url : string = this.helperService.determinateHost(false)+"/storage/image/file.png";
        fabric.Image.fromURL(url, function(image){
            image.scaleToWidth(100);
                let percentages      = window['angularComponentRef'].component.getPercentages(window['angularComponentRef'].component.canvas.width / 2, window['angularComponentRef'].component.canvas.height / 2, window['angularComponentRef'].component.canvas);
                let sizePercentages  = window['angularComponentRef'].component.getPercentages(100, image.height, window['angularComponentRef'].component.canvas);

                image.set({
                    gui_img :url,
                    width: image.width,
                    height: image.height,
                    XZIndex : index,
                    hasRotatingPoint : false
                });

                let group = new fabric.Group([image], {
                    left: window['angularComponentRef'].component.canvas.width / 2 + window['angularComponentRef'].component.getRandomInt(10,100),
                    top: window['angularComponentRef'].component.canvas.height / 2 + window['angularComponentRef'].component.getRandomInt(10,100),
                    percentage_left: percentages.left,
                    percentage_top: percentages.top,
                    percentage_width : sizePercentages.left,
                    percentage_height : sizePercentages.top,
                    id_content : (content.id_content == undefined )? 0: content.id_content,
                    id_element : (content.id_content == undefined )? content.id_element: 0,
                    XZIndex : index,
                    selectable: true,
                    evented: true,
                    hasRotatingPoint : false
                });

                if(content.options.canvas != undefined && content.options.canvas != null) {
                    group.scaleX = content.options.canvas.scaleX;
                    group.scaleY  =content.options.canvas.scaleY;
                    group.left = content.options.canvas.left;
                    group.top = content.options.canvas.top;
                }

                window['angularComponentRef'].component.canvas.add(group);
                window['angularComponentRef'].component.canvas.setActiveObject(group);


        })
    }

    addFabricNinguno(content : Content, index : number) {
      if(content.options.action == "Escena VR") {
        this.addFabricAssetBundle(content,index);
      } else if(content.options.action == "Play/PauseVideo") {
        this.addFabricPlayPauseVideo(content,index);
      }else {
        let url : string = this.helperService.determinateHost(false)+"/storage/image/ninguno.png";
        fabric.Image.fromURL(url, function(image){
            image.scaleToWidth(100);
                let percentages      = window['angularComponentRef'].component.getPercentages(window['angularComponentRef'].component.canvas.width / 2, window['angularComponentRef'].component.canvas.height / 2, window['angularComponentRef'].component.canvas);
                let sizePercentages  = window['angularComponentRef'].component.getPercentages(100, image.height, window['angularComponentRef'].component.canvas);

                image.set({
                    gui_img :url,
                    width: image.width,
                    height: image.height,
                    XZIndex : index,
                    hasRotatingPoint : false
                });

                let group = new fabric.Group([image], {
                    left: 0,
                    top: 0,
                    percentage_left: 0,
                    percentage_top: 0,
                    percentage_width : sizePercentages.left,
                    percentage_height : sizePercentages.top,
                    id_content : (content.id_content == undefined )? 0: content.id_content,
                    id_element : (content.id_content == undefined )? content.id_element: 0,
                    XZIndex : index,
                    selectable: false,
                    evented: true,
                    hasRotatingPoint : false
                });

                if(content.options.canvas != undefined && content.options.canvas != null) {
                    group.scaleX = content.options.canvas.scaleX;
                    group.scaleY  =content.options.canvas.scaleY;
                    group.left = 0;
                    group.top =0;
                }

                window['angularComponentRef'].component.canvas.add(group);
                window['angularComponentRef'].component.canvas.setActiveObject(group);


        })
      }

    }

    addFabricPlayPauseVideo(content : Content, index : number) {
      let url : string = this.helperService.determinateHost(false)+"/storage/"+content.childs[0].resource;
      let videoElement = null;
      this.getVideoElement(url).then((video) => {
          videoElement = video;
          fabric.Image.fromURL(this.helperService.determinateHost(false)+"/storage/image/file.png", function(image){


                  let video = new fabric.Image(videoElement.el,{
                      id_content : (content.id_content == undefined )? 0: content.id_content,
                      XZIndex : index,
                      selectable: true,
                      objectCaching: false,
                      hasRotatingPoint : false,
                      width: videoElement.videoWidth,
                      height: videoElement.videoHeight
                  });
                  video.scaleToWidth(100);

                  let percentages      = window['angularComponentRef'].component.getPercentages(window['angularComponentRef'].component.canvas.width / 2, window['angularComponentRef'].component.canvas.height / 2, window['angularComponentRef'].component.canvas);
                  let sizePercentages  = window['angularComponentRef'].component.getPercentages(100,  video.height, window['angularComponentRef'].component.canvas);


                  video.percentage_lef= percentages.left;
                  video.percentage_top= percentages.top;
                  video.percentage_width = sizePercentages.left;
                  video.percentage_height = sizePercentages.top;



                  if(content.options.canvas != undefined && content.options.canvas != null) {
                      video.scaleX = content.options.canvas.scaleX;
                      video.scaleY  =content.options.canvas.scaleY;
                      video.left = content.options.canvas.left;
                      video.top = content.options.canvas.top;
                  }

                  window['angularComponentRef'].component.canvas.add(video);
                  window['angularComponentRef'].component.canvas.setActiveObject(video);
          });
      });
  }


    addFabricVideo(content : Content, index : number) {
        let url : string = this.helperService.determinateHost(false)+"/storage/"+content.resource;
        let videoElement = null;
        this.getVideoElement(url).then((video) => {
            videoElement = video;
            fabric.Image.fromURL(this.helperService.determinateHost(false)+"/storage/image/file.png", function(image){


                    let video = new fabric.Image(videoElement.el,{
                        id_content : (content.id_content == undefined )? 0: content.id_content,
                        XZIndex : index,
                        selectable: true,
                        objectCaching: false,
                        hasRotatingPoint : false,
                        width: videoElement.videoWidth,
                        height: videoElement.videoHeight
                    });
                    video.scaleToWidth(100);

                    let percentages      = window['angularComponentRef'].component.getPercentages(window['angularComponentRef'].component.canvas.width / 2, window['angularComponentRef'].component.canvas.height / 2, window['angularComponentRef'].component.canvas);
                    let sizePercentages  = window['angularComponentRef'].component.getPercentages(100,  video.height, window['angularComponentRef'].component.canvas);


                    video.percentage_lef= percentages.left;
                    video.percentage_top= percentages.top;
                    video.percentage_width = sizePercentages.left;
                    video.percentage_height = sizePercentages.top;



                    if(content.options.canvas != undefined && content.options.canvas != null) {
                        video.scaleX = content.options.canvas.scaleX;
                        video.scaleY  =content.options.canvas.scaleY;
                        video.left = content.options.canvas.left;
                        video.top = content.options.canvas.top;
                    }

                    window['angularComponentRef'].component.canvas.add(video);
                    window['angularComponentRef'].component.canvas.setActiveObject(video);
            });
        });
    }

    getVideoElement(url) {
        let promise = new Promise((resolve, reject) => {

            var videoE = document.createElement('video');
            videoE.width = 450;
            videoE.height = 285;
            videoE.muted = true;
            var source = document.createElement('source');
            source.src = url;
            source.type = 'video/mp4';
            videoE.appendChild(source);
            let video = {
                videoHeight : 0,
                videoWidth : 0,
                el : videoE
            }
            videoE.addEventListener( "loadedmetadata", function (e) {
                video.videoHeight = this.videoHeight;
                video.videoWidth = this.videoWidth;
                video.el.height = this.videoHeight;
                video.el.width = this.videoWidth;
                resolve(video);
            }, false );

        })

        return promise;
    }

    addFabricImage(content : Content, index : number) {
        let url : string = this.helperService.determinateHost(false)+"/storage/"+content.resource;
        fabric.Image.fromURL(url, function(image){
            image.scaleToWidth(100);
                let percentages      = window['angularComponentRef'].component.getPercentages(window['angularComponentRef'].component.canvas.width / 2, window['angularComponentRef'].component.canvas.height / 2, window['angularComponentRef'].component.canvas);
                let sizePercentages  = window['angularComponentRef'].component.getPercentages(100, image.height, window['angularComponentRef'].component.canvas);

                image.set({
                    gui_img :url,
                    width: image.width,
                    height: image.height,
                    XZIndex : index,
                    originX: 'center',
                    originY: 'center',
                    hasRotatingPoint : false
                });

                let group = new fabric.Group([image], {
                    left: window['angularComponentRef'].component.canvas.width / 2 + window['angularComponentRef'].component.getRandomInt(10,100),
                    top: window['angularComponentRef'].component.canvas.height / 2 + window['angularComponentRef'].component.getRandomInt(10,100),
                    percentage_left: percentages.left,
                    percentage_top: percentages.top,
                    percentage_width : sizePercentages.left,
                    percentage_height : sizePercentages.top,
                    id_content : (content.id_content == undefined )? 0: content.id_content,
                    id_element : (content.id_content == undefined )? content.id_element: 0,
                    XZIndex : index,
                    selectable: true,
                    hasRotatingPoint : false
                });

                if(content.options.canvas != undefined && content.options.canvas != null) {
                    group.scaleX = content.options.canvas.scaleX;
                    group.scaleY  =content.options.canvas.scaleY;
                    group.left = content.options.canvas.left;
                    group.top = content.options.canvas.top;
                }

                window['angularComponentRef'].component.canvas.add(group);
                window['angularComponentRef'].component.canvas.setActiveObject(group);


        })
    }

    updateInfoObject(evt){
        let target = evt.target;
        for (let index = 0; index < this.markerService.getCurrentMarker().elementsInMarker.length; index++) {
            if(target.id_content == this.markerService.getCurrentMarker().elementsInMarker[index].id_content) {
                let percentage = this.updatePercentages(target, this.canvas,index);
                this.markerService.currentMarker.elementsInMarker[index].options.canvas = {};
                this.markerService.currentMarker.elementsInMarker[index].options.canvas["percentage_height"] = percentage.percentage_height;
                this.markerService.currentMarker.elementsInMarker[index].options.canvas["percentage_left"] = percentage.percentage_left;
                this.markerService.currentMarker.elementsInMarker[index].options.canvas["percentage_top"] = percentage.percentage_top;
                this.markerService.currentMarker.elementsInMarker[index].options.canvas["percentage_width"] = percentage.percentage_width;
                this.markerService.currentMarker.elementsInMarker[index].options.canvas["scaleX"] = target.scaleX;
                this.markerService.currentMarker.elementsInMarker[index].options.canvas["scaleY"] = target.scaleY;
                this.markerService.currentMarker.elementsInMarker[index].options.canvas["left"] = target.left;
                this.markerService.currentMarker.elementsInMarker[index].options.canvas["top"] = target.top;
                  this.markerService.addCurrentMarker(this.markerService.currentMarker);

            }
        }
    }

    updateCanvasEvents(event) {
        let marker : Content = this.markerService.getCurrentMarker();
        let index : number = (marker.elementsInMarker.length - 1);
        this.addLastElementInMarker(marker.elementsInMarker[index],index);
    }

    reloadElementInMarker(event) {
        this.createCanvas(this.markerService.getCurrentMarker());
    }

    updateElementInMarkerEvents(event) {
        let id_content : number = event["id_content"];
        this.markerService.elementInMarker = this.markerService.currentMarker.elementsInMarker.find(item => (item.id_content == id_content));
        this.showFormEdit = true;
    }

    deleteElementInMarker(event) {
        this.createCanvas(this.markerService.getCurrentMarker());
    }

    saveElementInMarker(event) {
        this.createCanvas(this.markerService.getCurrentMarker());
        this.showForm = false;
        this.showFormEdit = false;
    }

    addElementInMarker(event) {
        this.showForm = true;
    }

    cancelElementInMarker(event) {
        this.showForm = false;
        this.showFormEdit = false;
    }

    cancel() {
        let marker = this.markerService.markers.find(item => (item.id_content == this.markerService.currentMarker.id_content));
        if(this.status > 0) {
            this.markerService.addCurrentMarker(null);
            if(this.elementService.getLocal('instruction-gpar').id_element_parent != null) {
                this.router.navigate(['/crear/panel-instrucciones',this.elementService.getLocal('instruction-gpar').id_element_parent]);
            } else {
                this.router.navigate(["/editar/panel-instrucciones/", this.elementService.getLocal('instruction-gpar').id_element, this.elementService.getIdParent()]);
            }
        } else {
            this.progressService.clean();
            this.progressService.msg = "Cancelando marcador";
            this.progressService.progressActiveByName('cancel');
            let marker = this.markerService.getCurrentMarker();
            let httpOptions  = this.helperService.getHeaders();
            this.contentService.delete(marker.id_content,this.helperService.determinateHost(),httpOptions).then(() =>{
                this.markerService.addCurrentMarker(null);
                if(this.elementService.getLocal('instruction-gpar').id_element_parent != null) {
                    this.router.navigate(['/crear/panel-instrucciones',this.elementService.getLocal('instruction-gpar').id_element_parent]);
                } else {
                    this.router.navigate(["/editar/panel-instrucciones/", this.elementService.getLocal('instruction-gpar').id_element, this.elementService.getIdParent()]);
                }
                this.progressService.clean();
                this.progressService.progressInactiveByName('cancel');
            })
        }

    }

    save() {
        let marker = this.markerService.markers.find(item => (item.id_content == this.markerService.currentMarker.id_content));
        if(marker instanceof Content) {
            this.markerService.currentMarker.options.position = marker.options.position;
            let index : number = this.markerService.markers.indexOf(marker);
            this.markerService.markers.splice(index,1);
        } else {
          this.markerService.currentMarker.options.position = this.markerService.markers.length;
        }

        let element = this.elementService.getLocal('instruction-gpar');
        this.markerService.markers.push(this.markerService.currentMarker);
        this.saveMarker(this.markerService.currentMarker).then(() => {
            element.content.forEach(item => {
              if(item.id_content_type == 2) {
                let index = element.content.indexOf(item);
                element.content.splice(index,1);
              }
            })
            this.markerService.markers.forEach(marker => {
              element.content.push(marker);
            })
            this.elementService.currentElement = element;
            this.elementService.saveLocal('instruction-gpar');
            this.markerService.addCurrentMarker(null);
            if(this.elementService.getLocal('instruction-gpar').id_element_parent != null) {
              this.router.navigate(['/crear/panel-instrucciones',this.elementService.getLocal('instruction-gpar').id_element_parent]);
            } else {
              this.router.navigate(["/editar/panel-instrucciones/", this.elementService.getLocal('instruction-gpar').id_element, this.elementService.getIdParent()]);
            }
            console.log('saved ::');
        });
    }

    saveMarker(currentMarker : Content) {
        let promise = new Promise((resolve, reject) => {
          let marker : Content = this.contentService.getInstanceOfPlaneObject(currentMarker);
          marker.id_version = 2;
          let httpOptions  = this.helperService.getHeaders();
            this.contentService.save(marker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
                (data) => {
                    if(currentMarker.elementsInMarker.length > 0) {
                      this.saveElementsInMarker(currentMarker).then(() => { resolve()},(e) => {reject(e)})
                    } else {
                      resolve();
                    }
                },
                (error) => {
                    reject(error);
                }
            )
        })
        return promise;
    }

      saveElementsInMarker(marker : Content, index = 0) {
        let promise = new Promise((resolve, reject) => {
          if(index < marker.elementsInMarker.length) {
            let newElementInMarker : Content = this.contentService.getInstanceOfPlaneObject( marker.elementsInMarker[index]);
            newElementInMarker.id_version = 2;
            newElementInMarker.childs = marker.elementsInMarker[index].childs;
            newElementInMarker.sceneXR = marker.elementsInMarker[index].sceneXR;
            newElementInMarker.sceneVR = marker.elementsInMarker[index].sceneVR;
            this.storageElementInMarker(newElementInMarker).then(() => {
              index++;
              this.saveElementsInMarker(marker,index).then(() => resolve(), (e) => {reject(e)})
            }, (e) => { reject(e)})
          } else {
            resolve()
          }
        })
        return promise;
      }

      storageElementInMarker(elementInMarker : Content) {
        let promise = new Promise((resolve, reject) => {
          let httpOptions  = this.helperService.getHeaders();
          this.contentService.save(elementInMarker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
            () => {
              this.elementService.currentElement.content =  this.generateChilds(elementInMarker.childs);
              this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
                if(elementInMarker.sceneXR != undefined) {
                  elementInMarker.sceneXR = this.elementService.getInstanceOfPlaneObject(elementInMarker.sceneXR);
                  elementInMarker.sceneXR.id_version = 2;
                  this.satorageXR(elementInMarker.sceneXR).then(() => { resolve() }, (error) => { reject(error)})
                } else if(elementInMarker.sceneVR != undefined) {
                    elementInMarker.sceneVR = this.elementService.getInstanceOfPlaneObject(elementInMarker.sceneVR);
                    elementInMarker.sceneVR.id_version = 2;
                    this.satorageVR(elementInMarker.sceneVR).then(() => { resolve() }, (error) => { reject(error)})
                } else {
                  resolve();
                }
              })
            }
          )
        })
        return promise;
      }

      generateChilds(childs : Array<Content>) : Array<Content>{
        let childsIntance : Array<Content> = [];
        childs.forEach(child => {
          child = this.contentService.getInstanceOfPlaneObject(child);
          child.id_version = 2;
          childsIntance.push(child);
        })
        return childsIntance;
      }

      satorageXR(sceneXR : Element)  {
        let promise = new Promise((resolve, reject) => {
          console.log('XR Scene ::',sceneXR);
          if(sceneXR instanceof Element) {
              let httpOptions  = this.helperService.getHeaders();
              this.elementService.save(sceneXR, this.helperService.determinateHost(),httpOptions).then(
                  (data) => {
                    console.log('data save XR Scene ::', data);
                      this.elementService.currentElement.content =  sceneXR.content;
                      console.log('data save XR Scene contents::', this.elementService.currentElement.content );
                      this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then((data) => {
                        if(sceneXR.gpar != undefined) {
                          sceneXR.gpar = this.elementService.getInstanceOfPlaneObject(sceneXR.gpar);
                          sceneXR.gpar.id_version = 2;
                          this.saveGPARScene(sceneXR.gpar).then((data) => { resolve(data)},(error) => {reject(error)})
                        } else {
                          resolve();
                        }
                      })
                  },
                  (error) => {
                    reject(error);
                  }
              )
          } else {
            resolve();
          }
        })
        return promise;
      }

    satorageVR(sceneVR : Element)  {
        let promise = new Promise((resolve, reject) => {
          console.log('VR Scene ::',sceneVR);
          if(sceneVR instanceof Element) {
              let httpOptions  = this.helperService.getHeaders();
              this.elementService.save(sceneVR, this.helperService.determinateHost(),httpOptions).then(
                  (data) => {
                    console.log('data save VR Scene ::', data);
                      this.elementService.currentElement.content =  sceneVR.content;
                      console.log('data save VR Scene contents::', this.elementService.currentElement.content );
                      this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then((data) => {
                          resolve(data);
                      })
                  },
                  (error) => {
                    reject(error);
                  }
              )
          } else {
            resolve();
          }
        })
        return promise;
    }

    saveGPARScene(gpar : Element) {
      let promise = new Promise((resolve, reject) => {
        console.log('GPAR Scene ::',gpar);
        if(gpar instanceof Element) {
          gpar.id_version = 2;
          let httpOptions  = this.helperService.getHeaders();
          this.elementService.save(gpar, this.helperService.determinateHost(),httpOptions).then((data) =>{
            console.log('data save GPAR Scene ::', data);
            gpar.content.forEach(item => {
              if(item.id_content != this.markerService.currentMarker.id_content) {
                this.elementService.currentElement.content.push(item);
              }
            });
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then((data) => {
            console.log('data save GPAR Scene contents::', data);
              resolve()
            }, (error) => { reject(error)})
          })
        }
      })
      return promise;
    }
}
