import { Component, OnInit } from '@angular/core';
import { ContentTypeService } from 'src/services/content-type.service';
import { HelperService } from 'src/services/helper.service';
import { ElementType } from 'src/modal/element-type';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.css']
})
export class BreadcrumbsComponent implements OnInit {

  constructor(
    public helperService : HelperService,
    public contentTypeService : ContentTypeService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  goTo(elementType : ElementType) {
    if(elementType.id_element_type == 0) {
      this.router.navigate(["/elementos", 0]);
    } else {
      
      this.router.navigate(["/elementos", elementType.id_element]);
    }
  }
}
