import { BrowserModule } from '@angular/platform-browser';
import { SortablejsModule } from 'angular-sortablejs';
import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ElementsComponent } from './elements/elements.component';
import { MenuTopComponent } from './menu-top/menu-top.component';
import { ProgressComponent } from './progress/progress.component';
import { CreateCollectionComponent } from './create-collection/create-collection.component';
import { CardCollectionComponent } from './card-collection/card-collection.component';
import { CreateInstructionPanelComponent } from './create-instruction-panel/create-instruction-panel.component';
import { CanvasComponent } from './canvas/canvas.component';
import { CreateElementCanvasFormComponent } from './create-element-canvas-form/create-element-canvas-form.component';
import { PanelElementsCanvasComponent } from './panel-elements-canvas/panel-elements-canvas.component';
import { CreateElementCanvasOptionsVideoFormComponent } from './create-element-canvas-options-video-form/create-element-canvas-options-video-form.component';
import { ElementCanvasCardMiniComponent } from './element-canvas-card-mini/element-canvas-card-mini.component';
import { EditElementCanvasFormComponent } from './edit-element-canvas-form/edit-element-canvas-form.component';
import { CreateElementSceneXrCanvasFormComponent } from './create-element-scene-xr-canvas-form/create-element-scene-xr-canvas-form.component';
import { EditElementSceneXrCanvasFormComponent } from './edit-element-scene-xr-canvas-form/edit-element-scene-xr-canvas-form.component';
import { CreateElementInstructionSceneGparCanvasFormComponent } from './create-element-instruction-scene-gpar-canvas-form/create-element-instruction-scene-gpar-canvas-form.component';
import { EditElementInstructionSceneGparCanvasFormComponent } from './edit-element-instruction-scene-gpar-canvas-form/edit-element-instruction-scene-gpar-canvas-form.component';
import { EditCollectionComponent } from './edit-collection/edit-collection.component';
import { CardPanelInstructionsComponent } from './card-panel-instructions/card-panel-instructions.component';
import { EditInstructionPanelComponent } from './edit-instruction-panel/edit-instruction-panel.component';
import { CreateTravelComponent } from './create-travel/create-travel.component';
import { CardTravelComponent } from './card-travel/card-travel.component';
import { EditTravelComponent } from './edit-travel/edit-travel.component';
import { CreateSceneXRComponent } from './create-scene-xr/create-scene-xr.component';
import { CardSceneXRComponent } from './card-scene-xr/card-scene-xr.component';
import { EditSceneXRComponent } from './edit-scene-xr/edit-scene-xr.component';
import { CreateSceneGPARComponent } from './create-scene-gpar/create-scene-gpar.component';
import { CardSceneGPARComponent } from './card-scene-gpar/card-scene-gpar.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { PreviewMarkerComponent } from './preview-marker/preview-marker.component';
import { EditSceneGPARComponent } from './edit-scene-gpar/edit-scene-gpar.component';
import { ShowErrorsComponent } from './show-errors/show-errors.component';
import { UsersComponent } from './users/users.component';
import { ErrorComponent } from './error/error.component';
import { CreateSceneVrCanvasFormComponent } from './create-scene-vr-canvas-form/create-scene-vr-canvas-form.component';
import { EditElementSceneVrCanvasFormComponent } from './edit-element-scene-vr-canvas-form/edit-element-scene-vr-canvas-form.component';


const routes: Routes = [
  {
    path: "elementos/:id_element",
    component : ElementsComponent
  },
  {
    path: "crear/coleccion",
    component : CreateCollectionComponent
  },
  {
    path: "crear/recorrido/:id_element_parent",
    component : CreateTravelComponent
  },
  {
    path: "crear/escena-gpar/:id_element_parent",
    component : CreateSceneGPARComponent
  },
  {
    path: "editar/escena-gpar/:id_element/:id_element_parent",
    component : EditSceneGPARComponent
  },
  {
    path: "crear/escena-xr/:id_element_parent",
    component : CreateSceneXRComponent
  },
  {
    path: "editar/escena-xr/:id_element/:id_element_parent",
    component : EditSceneXRComponent
  },
  {
    path: "editar/coleccion/:id_element/:id_element_parent",
    component : EditCollectionComponent
  },
  {
    path: "editar/recorrido/:id_element/:id_element_parent",
    component : EditTravelComponent
  },
  {
    path: "editar/panel-instrucciones/:id_element/:id_element_parent",
    component : EditInstructionPanelComponent
  },
  {
    path: "crear/panel-instrucciones/:id_element_parent",
    component : CreateInstructionPanelComponent
  },
  {
    path: "canvas/:status",
    component : CanvasComponent
  },
  {
    path: "login",
    component : LoginComponent,

  },
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo:'error/404/Pagina no encontrada' },
  {
    path: "error/:code/:msg",
    component : ErrorComponent
  },

];

@NgModule({
  declarations: [
    AppComponent,
    ElementsComponent,
    LoginComponent,
    ElementsComponent,
    MenuTopComponent,
    ProgressComponent,
    CreateCollectionComponent,
    CardCollectionComponent,
    CreateInstructionPanelComponent,
    CanvasComponent,
    CreateElementCanvasFormComponent,
    PanelElementsCanvasComponent,
    CreateElementCanvasOptionsVideoFormComponent,
    ElementCanvasCardMiniComponent,
    EditElementCanvasFormComponent,
    CreateElementSceneXrCanvasFormComponent,
    EditElementSceneXrCanvasFormComponent,
    CreateElementInstructionSceneGparCanvasFormComponent,
    EditElementInstructionSceneGparCanvasFormComponent,
    EditCollectionComponent,
    CardPanelInstructionsComponent,
    EditInstructionPanelComponent,
    CreateTravelComponent,
    CardTravelComponent,
    EditTravelComponent,
    CreateSceneXRComponent,
    CardSceneXRComponent,
    EditSceneXRComponent,
    CreateSceneGPARComponent,
    CardSceneGPARComponent,
    BreadcrumbsComponent,
    PreviewMarkerComponent,
    EditSceneGPARComponent,
    ShowErrorsComponent,
    UsersComponent,
    ErrorComponent,
    CreateSceneVrCanvasFormComponent,
    EditElementSceneVrCanvasFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}),
    SortablejsModule.forRoot({ animation: 150 }),
    FormsModule,
    FontAwesomeModule,
  ],
  providers: [
    {
      provide : LocationStrategy, useClass : HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
