import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPanelInstructionsComponent } from './card-panel-instructions.component';

describe('CardPanelInstructionsComponent', () => {
  let component: CardPanelInstructionsComponent;
  let fixture: ComponentFixture<CardPanelInstructionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardPanelInstructionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPanelInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
