import { Component, OnInit, Input } from '@angular/core';
import { Element } from 'src/modal/element';
import { HelperService } from 'src/services/helper.service';
import { faTrash, faCircle, faEdit, faClock } from '@fortawesome/free-solid-svg-icons';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ProgressService } from 'src/services/progress.service';
import { ElementService } from 'src/services/element.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-card-panel-instructions',
  templateUrl: './card-panel-instructions.component.html',
  styleUrls: ['./card-panel-instructions.component.css']
})
export class CardPanelInstructionsComponent implements OnInit {
  @Input() element : Element;
  @Input() hide_description = false;
  @Input() hide_name = false;
  @Input() hide_titles = false;
  @Input() hide_controls = true;

  faEdit = faEdit;
  faTrash = faTrash;
  languageName : String;

  constructor(
    public languageService : LanguageService,
    public systemService : SystemService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public elementService : ElementService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
  }

  delete() {
    this.progressService.msg = "Eliminando";
    this.progressService.progressActiveByName('default');
    this.elementService.delete(this.element.id_element,this.helperService.determinateHost(),this.helperService.getHeaders())
      .then(
        (data) => {
          let index : number = this.elementService.elements.indexOf(this.element);
          this.elementService.elements.splice(index,1);
          this.progressService.progressInactiveByName('default');
        },
        (error) => {}
      )
  }

  edit() {
    this.router.navigate(["/editar/panel-instrucciones/",this.element.id_element,(this.elementService.currentElement == undefined) ? 0 : this.elementService.currentElement.id_element]);
  }

  goTo() {
    this.router.navigate(["/editar/panel-instrucciones/",this.element.id_element,(this.elementService.currentElement == undefined) ? 0 : this.elementService.currentElement.id_element]);
  }


}
