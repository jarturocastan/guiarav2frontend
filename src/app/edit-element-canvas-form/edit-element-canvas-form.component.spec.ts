import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditElementCanvasFormComponent } from './edit-element-canvas-form.component';

describe('EditElementCanvasFormComponent', () => {
  let component: EditElementCanvasFormComponent;
  let fixture: ComponentFixture<EditElementCanvasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditElementCanvasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditElementCanvasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
