import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { HelperService } from 'src/services/helper.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { ElementService } from 'src/services/element.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { MarkerService } from 'src/services/marker.service';
import { ContentService } from 'src/services/content.service';
import { ProgressService } from 'src/services/progress.service';
import { ContentType } from 'src/modal/content-type';
import { Content } from 'src/modal/content';
import { ContentOptions } from 'src/modal/content-options';
import { faFileArchive , faSpinner, faCheck } from '@fortawesome/free-solid-svg-icons';
import { Element } from 'src/modal/element';
import { ErrorService } from 'src/services/error.service';
import { ElementOptions } from 'src/modal/element-options';

@Component({
  selector: 'app-edit-element-canvas-form',
  templateUrl: './edit-element-canvas-form.component.html',
  styleUrls: ['./edit-element-canvas-form.component.css']
})
export class EditElementCanvasFormComponent implements OnInit {
  faFileArchive = faFileArchive;
  faSpinner = faSpinner;
  faCheck = faCheck;
  @Output()  saveElementInMarker = new EventEmitter();
  @Output()  cancelElementInMarker = new EventEmitter();
  showElementInMarkerForm : Boolean = true;
  showSceneXRForm : Boolean = false;
  showSceneVRForm : Boolean = false;
  showSceneGPAR : Boolean = false;
  showEditSceneXRForm : Boolean = false;
  showEditSceneVRForm : Boolean = false;
  showEditSceneGPAR : Boolean = false;
  reg : any;

  constructor(
    public helperService : HelperService,
    public contentTypeService : ContentTypeService,
    public elementService : ElementService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public markerService : MarkerService,
    public contentService : ContentService,
    public progressService : ProgressService,
    public errorService : ErrorService
  ) { }

  ngOnInit() {
    this.progressService.type = 2;
    this.progressService.progressActiveByName('element-canvas-form');
    let httpOptions  = this.helperService.getHeaders();
    this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
      this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
        this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          let childs : Array<Content> = this.markerService.elementInMarker.childs;
          let sceneXR : Element = this.markerService.elementInMarker.sceneXR;
          let sceneVR : Element = this.markerService.elementInMarker.sceneVR;
          let position = this.markerService.elementInMarker.options['position'];
          this.reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]';
          this.markerService.elementInMarker = new Content(
              this.markerService.elementInMarker.id_content,
              this.markerService.elementInMarker.resource,
              this.markerService.elementInMarker.id_content_type,
              this.markerService.elementInMarker.id_language,
              this.markerService.elementInMarker.id_element,
              this.markerService.elementInMarker.id_system, new ContentOptions(
                0,0,
                this.markerService.elementInMarker.options.event,
                this.markerService.elementInMarker.options.action,
                this.markerService.elementInMarker.options.canvas,
                this.markerService.elementInMarker.options.loop,
                this.markerService.elementInMarker.options.transparency,
                this.markerService.elementInMarker.options.subtitle,
                this.markerService.elementInMarker.options.frame,
                this.markerService.elementInMarker.options.frameStart,
                this.markerService.elementInMarker.options.frameEnd,
                ),
                this.markerService.elementInMarker.id_version,
                this.markerService.elementInMarker.status,
                this.markerService.elementInMarker.id_content_parent,[]);

          this.markerService.elementInMarker.options['position'] = position;
          let childs_2 = [];
          childs.forEach(element => {
            let isValid = true;
            childs_2.forEach(element2=> {
              if(element.id_content  == element2.id_content) {
                isValid = false;
              }
            })

            if(isValid) {
              childs_2.push(this.contentService.getInstanceOfPlaneObject(element));
            }
          });
          this.markerService.elementInMarker.childs = childs_2;
          this.markerService.elementInMarker.sceneXR = sceneXR;
          this.markerService.elementInMarker.sceneVR = sceneVR;
          console.log('element in marker ::', this.markerService.elementInMarker);
          this.contentTypeService.determinateContentTypeCombobox(3);
          this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
          this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
          this.contentTypeService.contentType = this.contentTypeService.findContentTypeById(this.markerService.elementInMarker.id_content_type);
          this.changeContentType(this.contentTypeService.findContentTypeById(this.markerService.elementInMarker.id_content_type).name);
          this.progressService.progressInactiveByName('element-canvas-form');
        });
      });
    });
  }

  changeContentType(contentTypeName : String) {
    let contentType : ContentType = this.contentTypeService.contentTypeCombobox.find(item => (item.name == this.contentTypeService.contentType.name));
    if(contentType instanceof ContentType) {
      if(contentType.id_content_type == this.markerService.elementInMarker.id_content_type) {
        this.markerService.elementInMarker.options.event = (this.markerService.elementInMarker.options.event !=null) ? this.markerService.elementInMarker.options.event : this.contentTypeService.contentType.options.events[0];
        this.markerService.elementInMarker.options.action = (this.markerService.elementInMarker.options.action !=null) ? this.markerService.elementInMarker.options.action :this.contentTypeService.contentType.getActionsByEvent(this.markerService.elementInMarker.options.event)[0];
      } else {
        this.markerService.elementInMarker.id_content_type = this.contentTypeService.contentType.id_content_type;
        this.markerService.elementInMarker.options.event = contentType.options.events[0];
        this.markerService.elementInMarker.options.action = contentType.getActionsByEvent(this.markerService.elementInMarker.options.event)[0];
      }
    }
  }

  save() {
    let httpOptions  = this.helperService.getHeaders();
    if(this.markerService.elementInMarker.options.action != "Escena XR" && this.markerService.elementInMarker.options.action != "Escena VR") {
      if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
        this.contentService.save(this.markerService.elementInMarker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
          (data) => {
            if(this.markerService.elementInMarker.sceneXR != undefined) {
              this.elementService.delete(this.markerService.elementInMarker.sceneXR.id_element, this.helperService.determinateHost(),httpOptions).then(
                (data) => {
                  let elementInMarker = this.markerService.currentMarker.elementsInMarker.find(item => item.id_content == this.markerService.elementInMarker.id_content);
                  let index = this.markerService.currentMarker.elementsInMarker.indexOf(elementInMarker);
                  this.markerService.currentMarker.elementsInMarker.splice(index,1);
                  this.contentService.content.options = this.markerService.elementInMarker.options;
                  this.contentService.content.childs =  this.markerService.elementInMarker.childs;
                  this.markerService.currentMarker.elementsInMarker.push(this.contentService.content);
                  this.markerService.addCurrentMarker(this.markerService.currentMarker);
                  this.saveElementInMarker.emit(this.markerService.currentMarker);
                },
                (error) => {}
              )
            } else {
              let elementInMarker = this.markerService.currentMarker.elementsInMarker.find(item => item.id_content == this.markerService.elementInMarker.id_content);
              let index = this.markerService.currentMarker.elementsInMarker.indexOf(elementInMarker);
              this.markerService.currentMarker.elementsInMarker.splice(index,1);
              this.contentService.content.options = this.markerService.elementInMarker.options;
              this.contentService.content.childs =  this.markerService.elementInMarker.childs;
              this.markerService.currentMarker.elementsInMarker.push(this.contentService.content);
              this.markerService.addCurrentMarker(this.markerService.currentMarker);
              this.saveElementInMarker.emit(this.markerService.currentMarker);
            }
          },
          (error) => {}
        )
      } else {

      }
    } else if(this.markerService.elementInMarker.options.action == "Escena VR") {
      if(this.markerService.elementInMarker.sceneVR != undefined) {
        this.showElementInMarkerForm = false;
        this.showEditSceneXRForm = false;
        this.showEditSceneVRForm = true;
      } else {
        this.showElementInMarkerForm = false;
        this.showSceneXRForm = false;
        this.showSceneVRForm = true;
      }
    }else {
      if(this.markerService.elementInMarker.sceneXR != undefined) {
        this.showElementInMarkerForm = false;
        this.showEditSceneXRForm = true;
      } else {
        this.showElementInMarkerForm = false;
        this.showSceneXRForm = true;
      }
    }
  }

  editXR() {
    this.showElementInMarkerForm = false;
    this.showEditSceneXRForm = true;
  }

  editVR() {
    this.showElementInMarkerForm = false;
    this.showEditSceneVRForm = true;
  }

  cancelEditXR() {
    this.showElementInMarkerForm = true;
    this.showEditSceneXRForm = false;
    this.showEditSceneGPAR = false;
    this.ngOnInit();
  }

  cancelEditVR() {
    this.showElementInMarkerForm = true;
    this.showEditSceneXRForm = false;
    this.showEditSceneVRForm = false;
    this.showEditSceneGPAR = false;
    this.ngOnInit();
  }


  cancelEditElemenGPARtInMarker() {
    this.showElementInMarkerForm = false;
    this.showEditSceneXRForm = true;
    this.showEditSceneGPAR = false;
    this.ngOnInit();

  }

  saveSceneVRInMarker(event) {
    if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
      this.markerService.currentMarker.elementsInMarker.push(this.markerService.elementInMarker);
      this.markerService.addCurrentMarker(this.markerService.currentMarker);
      this.showElementInMarkerForm = true;
      this.showSceneVRForm = false;
      this.saveElementInMarker.emit(this.markerService.currentMarker);
    }
  }

  saveSceneXRInMarker(event) {
    let content : Content = this.markerService.elementInMarker.sceneXR.content.find(item => (item.id_content_type == 16));
    if(content instanceof Content) {
      this.showElementInMarkerForm = false;
      this.showSceneXRForm = false;
      this.showSceneGPAR = true;
    } else {
      if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
        let httpOptions  = this.helperService.getHeaders();
        this.contentService.save(this.markerService.elementInMarker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
          (data) => {
            let elementInMarker = this.markerService.currentMarker.elementsInMarker.find(item => item.id_content == this.markerService.elementInMarker.id_content);
            let index = this.markerService.currentMarker.elementsInMarker.indexOf(elementInMarker);
            this.markerService.currentMarker.elementsInMarker.splice(index,1);
            this.contentService.content.childs =  this.markerService.elementInMarker.childs;
            this.contentService.content.sceneXR = this.markerService.elementInMarker.sceneXR;
            this.contentService.content.sceneVR = this.markerService.elementInMarker.sceneVR;
            this.markerService.currentMarker.elementsInMarker.push(this.contentService.content);
            this.markerService.addCurrentMarker(this.markerService.currentMarker);
            this.saveElementInMarker.emit(this.markerService.currentMarker);
          },
          (error) => {}
        )
      }
    }
  }

  saveSceneGPARInMarker(event) {
    if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
      let elementInMarker = this.markerService.currentMarker.elementsInMarker.find(item => item.id_content == this.markerService.elementInMarker.id_content);
      let index = this.markerService.currentMarker.elementsInMarker.indexOf(elementInMarker);
      this.markerService.currentMarker.elementsInMarker.splice(index,1);
      this.contentService.content = this.markerService.elementInMarker;
      this.contentService.content.childs =  this.markerService.elementInMarker.childs;
      this.contentService.content.sceneXR = this.markerService.elementInMarker.sceneXR;
      this.contentService.content.sceneXR.gpar = this.markerService.elementInMarker.sceneXR.gpar;
      this.markerService.currentMarker.elementsInMarker.push(this.contentService.content);
      this.markerService.addCurrentMarker(this.markerService.currentMarker);
      this.showElementInMarkerForm = false;
      this.showSceneGPAR = false;
      this.saveElementInMarker.emit(this.markerService.currentMarker);
    }
  }

  saveEditSceneXRInMarker(event) {
    let content : Content = this.markerService.elementInMarker.sceneXR.content.find(item => (item.id_content_type == 16));
    if(content instanceof Content) {
      if(this.markerService.elementInMarker.sceneXR.gpar instanceof Element) {
        this.elementService.currentElement = new Element(
          this.markerService.elementInMarker.sceneXR.gpar.id_element,
          this.markerService.elementInMarker.sceneXR.gpar.id_element_type,
          this.markerService.elementInMarker.sceneXR.gpar.content,
          new ElementOptions(
            this.markerService.elementInMarker.sceneXR.gpar.options.travel_mode,
            this.markerService.elementInMarker.sceneXR.gpar.options.ar_custom,
          ),
          this.markerService.elementInMarker.sceneXR.gpar.id_version,
          this.markerService.elementInMarker.sceneXR.gpar.status,
          this.markerService.elementInMarker.sceneXR.gpar.id_element_parent,
          this.markerService.elementInMarker.sceneXR.gpar.id_content_parent
        );
        this.elementService.currentElement.instanceOfContent();
        this.editEditGPARInMarker([]);
      } else {
        this.showElementInMarkerForm = false;
        this.showEditSceneXRForm = false;
        this.showSceneGPAR = true;
      }
    } else {
      if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
        let httpOptions  = this.helperService.getHeaders();
        this.contentService.save(this.markerService.elementInMarker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
          (data) => {
            let elementInMarker = this.markerService.currentMarker.elementsInMarker.find(item => item.id_content == this.markerService.elementInMarker.id_content);
            let index = this.markerService.currentMarker.elementsInMarker.indexOf(elementInMarker);
            this.markerService.currentMarker.elementsInMarker.splice(index,1);
            this.contentService.content.childs =  this.markerService.elementInMarker.childs;
            this.contentService.content.sceneXR = this.markerService.elementInMarker.sceneXR;
            this.markerService.currentMarker.elementsInMarker.push(this.contentService.content);
            this.markerService.addCurrentMarker(this.markerService.currentMarker);
            this.saveElementInMarker.emit(this.markerService.currentMarker);
          },
          (error) => {}
        )
      }
    }
  }

  saveEditSceneVRInMarker(event) {
    if(this.errorService.validateElementInMarker(this.markerService.elementInMarker)) {
      let httpOptions  = this.helperService.getHeaders();
        this.contentService.save(this.markerService.elementInMarker.toFormData(), this.helperService.determinateHost(),httpOptions).then(
          (data) => {
            let elementInMarker = this.markerService.currentMarker.elementsInMarker.find(item => item.id_content == this.markerService.elementInMarker.id_content);
            let index = this.markerService.currentMarker.elementsInMarker.indexOf(elementInMarker);
            this.markerService.currentMarker.elementsInMarker.splice(index,1);
            this.contentService.content.childs =  this.markerService.elementInMarker.childs;
            this.contentService.content.sceneVR = this.markerService.elementInMarker.sceneVR;
            this.markerService.currentMarker.elementsInMarker.push(this.contentService.content);
            this.markerService.addCurrentMarker(this.markerService.currentMarker);
            this.saveElementInMarker.emit(this.markerService.currentMarker);
            console.log('event ::', event);
          },
          (error) => {}
        )
    }
  }

  editEditGPARInMarker(event) {
    this.showElementInMarkerForm = false;
    this.showEditSceneXRForm = false;
    this.showEditSceneGPAR = true;
  }

  cancel() {
    this.cancelElementInMarker.emit({});
  }
}
