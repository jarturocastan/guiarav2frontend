import { Component, OnInit } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute } from '@angular/router';
import { faImage, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Element } from 'src/modal/element';

@Component({
  selector: 'app-edit-travel',
  templateUrl: './edit-travel.component.html',
  styleUrls: ['./edit-travel.component.css']
})
export class EditTravelComponent implements OnInit {
  id_element_parent : number;
  languageName : String;
  faSpinner = faSpinner;
  faImage = faImage;

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.progressService.progressActiveByName('default');
    this.errorService.clean();
    this.route.params.subscribe(params => {
      let id_element : number = parseInt(params["id_element"]);
      this.id_element_parent = parseInt(params["id_element_parent"]);
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.getById(id_element,this.helperService.determinateHost(),httpOptions).then(() => {
        this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
              this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
                this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
                this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
                this.elementTypeService.determinateNextElementTypes(this.elementService.currentElement.id_element_type);
                this.progressService.progressInactiveByName('default');
              })
            })
          })
        })
      })
    })
  }

  cancel() {
    this.router.navigate(['/elementos', (this.id_element_parent != undefined)? this.id_element_parent : 0]);
  }

  save(element : Element) {
    if(this.errorService.validateElement(element)) {
      element.id_version = 2;
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
          this.elementService.currentElement.content = element.content;
          this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
            this.router.navigate(['/elementos',0]);
          })
        },
        (error) => {}
      )
    }
  }

}
