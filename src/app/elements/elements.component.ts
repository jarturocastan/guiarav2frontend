import { Component, OnInit } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { ElementService } from 'src/services/element.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { MarkerService } from 'src/services/marker.service';
import { ContentService } from 'src/services/content.service';

@Component({
  selector: 'app-elements',
  templateUrl: './elements.component.html',
  styleUrls: ['./elements.component.css']
})

export class ElementsComponent implements OnInit {
  id_element :number;
  options : any;

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    private router: Router,
    private route: ActivatedRoute,
    public markerService : MarkerService,
    public contentService : ContentService
  ) { }

  	ngOnInit() {
    	this.route.params.subscribe(params => {
			this.id_element =  params['id_element'];
			this.elementService.flush('instruction-gpar');
			this.progressService.clean();
			this.progressService.progressActiveByName('default');
			this.markerService.addCurrentMarker(null);
      this.markerService.markers = [];
      let httpOptions  = this.helperService.getHeaders();
			this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then((data) => {
				this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then((data) => {
					this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(
						(success) =>{
							if(this.id_element == 0) {
								this.elementTypeService.determinateMenu(0);
								this.languageService.determinateLanguagesCombobox(1);
								this.elementTypeService.currentElementType = this.elementTypeService.findElementTypeById(1);
								this.systemService.determinateSystemsCombobox(1);
								this.elementService.getAllByElementTypeId(1,this.helperService.determinateHost(),httpOptions).then((data) => {
									this.helperService.addBreadcrumbs(null);
									this.progressService.progressInactiveByName('default');
									this.elementService.elements = this.elementService.elements.sort((a,b) => {
										return a.options['position'] -b.options['position'];
									});
								},(error) => {
									this.progressService.progressInactiveByName('default');
									this.router.navigate(["/error",error[1],error[0].replace(new RegExp(' ', 'g'),'_%')]);
								})
							} else {
								this.elementService.getById(this.id_element,this.helperService.determinateHost(),httpOptions).then((data) => {
									this.elementService.getElementsByElementParentId(this.id_element,this.helperService.determinateHost(),httpOptions).then(() => {
                    if(this.elementService.elements.length > 0) {
                      this.elementTypeService.determinateMenu(0,this.elementService.elements[0].id_element_type);
                    } else {
                      this.elementTypeService.determinateMenu(this.elementService.currentElement.id_element_type);
                    }

                    this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
										this.elementTypeService.currentElementType = this.elementTypeService.findElementTypeById(this.elementService.currentElement.id_element_type);
										this.systemService.determinateSystemsCombobox(1);
										this.elementTypeService.currentElementType.id_element = this.elementService.currentElement.id_element;
										this.helperService.addBreadcrumbs( this.elementTypeService.currentElementType);
										this.progressService.progressInactiveByName('default');
										this.elementService.elements = this.elementService.elements.sort((a,b) => {
											return a.options['position'] -b.options['position'];
										});
									},(error) => {
										this.progressService.progressInactiveByName('default');
										this.router.navigate(["/error",error[1],error[0].replace(new RegExp(' ', 'g'),'_%')]);
									})
								},(error) => {
									this.progressService.progressInactiveByName('default');
									this.router.navigate(["/error",error[1],error[0].replace(new RegExp(' ', 'g'),'_%')]);
								})
							}
						},
						(error)=>{
							this.progressService.progressInactiveByName('default');
							this.router.navigate(["/error",error[1],error[0].replace(new RegExp(' ', 'g'),'_%')]);
						}
					)
				}, (error) => {
					this.progressService.progressInactiveByName('default');
					this.router.navigate(["/error",error[1],error[0].replace(new RegExp(' ', 'g'),'_%')]);
				})
			})
		})

		this.options = {
			onUpdate: (event: any) => {
				let data = {
					elements : []
        }
				for (let index = 0; index < this.elementService.elements.length; index++) {
					this.elementService.elements[index].options["position"] = index;
					data.elements.push({id_element : this.elementService.elements[index].id_element, position : index});
        }
        let httpOptions  = this.helperService.getHeaders();
				this.elementService.savePositionOfElements(data,this.helperService.determinateHost(),httpOptions).then(() => {})
			}
		};
  	}

}
