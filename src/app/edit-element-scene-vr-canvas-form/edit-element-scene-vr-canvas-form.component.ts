import { Component, OnInit, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { SystemService } from 'src/services/system.service';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { MarkerService } from 'src/services/marker.service';
import { ErrorService } from 'src/services/error.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Element } from 'src/modal/element';
import { ElementOptions } from 'src/modal/element-options';
import { Content } from 'src/modal/content';
import { faSpinner, faFileArchive, faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-edit-element-scene-vr-canvas-form',
  templateUrl: './edit-element-scene-vr-canvas-form.component.html',
  styleUrls: ['./edit-element-scene-vr-canvas-form.component.css']
})
export class EditElementSceneVrCanvasFormComponent implements OnInit {

  faFileArchive = faFileArchive;
  faSpinner = faSpinner;
  faCheck = faCheck;
  @Output()  cancelEditElementVR = new EventEmitter();
  @Output()  saveEditSceneVRInMarker = new EventEmitter();

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public markerService : MarkerService,
    public errorService : ErrorService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    let httpOptions  = this.helperService.getHeaders();
    this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
      this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
        this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.elementTypeService.determinateMenu(-1);
            console.log('sceneVR ::',this.markerService.elementInMarker.sceneVR);
            if(this.markerService.elementInMarker.sceneVR != null) {
              this.languageService.determinateLanguagesCombobox(this.markerService.elementInMarker.sceneVR.id_element_type);
              this.systemService.determinateSystemsCombobox(this.markerService.elementInMarker.sceneVR.id_element_type);
              this.contentTypeService.determinateContentTypeCombobox(this.markerService.elementInMarker.sceneVR.id_element_type);
              this.contentTypeService.contentType = this.contentTypeService.findContentTypeById(1);
              this.elementService.currentElement = this.elementService.getInstanceOfPlaneObject(this.markerService.elementInMarker.sceneVR).instanceOfContent();
              console.log('current element ::', this.elementService.currentElement);
            }
          });
        });
      });
    });
  }

  cancel() {
    this.cancelEditElementVR.emit({});
  }



  save(element : Element) {
    if(this.errorService.validateElement(element)) {
      let httpOptions  = this.helperService.getHeaders();
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
          console.log(element)
          this.elementService.currentElement.content = element.content;
            this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
            this.markerService.elementInMarker.sceneVR = this.elementService.currentElement;
            console.log('save marker');
            this.saveEditSceneVRInMarker.emit({});

          })
        },
        (error) => {}
      )
    }
  }

}
