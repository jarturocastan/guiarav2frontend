import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditElementSceneVrCanvasFormComponent } from './edit-element-scene-vr-canvas-form.component';

describe('EditElementSceneVrCanvasFormComponent', () => {
  let component: EditElementSceneVrCanvasFormComponent;
  let fixture: ComponentFixture<EditElementSceneVrCanvasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditElementSceneVrCanvasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditElementSceneVrCanvasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
