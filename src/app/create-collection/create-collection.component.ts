import { Component, OnInit } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementService } from 'src/services/element.service';
import { Element } from 'src/modal/element';
import { ElementOptions } from 'src/modal/element-options';
import { HelperService } from 'src/services/helper.service';
import { ProgressService } from 'src/services/progress.service';
import { LanguageService } from 'src/services/language.service';
import { SystemService } from 'src/services/system.service';
import { ContentTypeService } from 'src/services/content-type.service';
import { faImage, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorService } from 'src/services/error.service';
import { ContentService } from 'src/services/content.service';

@Component({
  selector: 'app-create-collection',
  templateUrl: './create-collection.component.html',
  styleUrls: ['./create-collection.component.css']
})
export class CreateCollectionComponent implements OnInit {
  languageName : String;
  faSpinner = faSpinner;
  faImage = faImage;

  constructor(
    public elementTypeService : ElementTypeService,
    public elementService : ElementService,
    public helperService : HelperService,
    public progressService : ProgressService,
    public languageService : LanguageService,
    public systemService : SystemService,
    public contentTypeService : ContentTypeService,
    public contentService : ContentService,
    public errorService : ErrorService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.progressService.progressActiveByName('default');
    this.errorService.clean();
    let element = new Element(null,1,[],new ElementOptions(false,false),1,1,null);
    let httpOptions  = this.helperService.getHeaders();
    this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(() => {
      this.languageService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
        this.systemService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
          this.contentTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
            this.elementTypeService.getAll(this.helperService.determinateHost(),httpOptions).then(() => {
              this.elementTypeService.determinateMenu(-1);
              this.languageService.determinateLanguagesCombobox(this.elementService.currentElement.id_element_type);
              this.systemService.determinateSystemsCombobox(this.elementService.currentElement.id_element_type);
              this.elementTypeService.determinateNextElementTypes(this.elementService.currentElement.id_element_type);
              this.progressService.progressInactiveByName('default');
              console.log('currentElement ::',this.elementService.currentElement);
            });
          })
        })
      })
    })
  }

  cancel() {
    this.router.navigate(['/elementos',0]);
  }

  save(element : Element) {
    if(this.errorService.validateElement(element)) {
      element.id_version = 2;
      element.options['position'] = null;
      let httpOptions  = this.helperService.getHeaders();
      console.log('httpOptions',httpOptions);
      this.elementService.save(element,this.helperService.determinateHost(),httpOptions).then(
        (data) => {
          this.elementService.currentElement.content = element.content;
          this.elementService.saveAllContent(this.helperService.determinateHost(),httpOptions).then(() => {
            this.contentService.createZip(this.elementService.currentElement.id_element,this.helperService.determinateHost(false),'ios');
            this.contentService.createZip(this.elementService.currentElement.id_element,this.helperService.determinateHost(false),'android');
            if(this.elementTypeService.nextElementType.id_element_type == 3) {
              this.router.navigate(['/crear/panel-instrucciones',this.elementService.currentElement.id_element]);
            } else {
              this.router.navigate(['/crear/recorrido',this.elementService.currentElement.id_element]);
            }
          })
        },
        (error) => {}
      )
    }
  }
}
