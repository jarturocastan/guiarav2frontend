import { Component, OnInit } from '@angular/core';
import { ElementTypeService } from 'src/services/element-type.service';
import { ElementType } from 'src/modal/element-type';
import { Router } from '@angular/router';
import { ElementService } from 'src/services/element.service';
import { HelperService } from 'src/services/helper.service';
import { UserService } from 'src/services/user.service';

declare var $:any;

@Component({
  selector: 'app-menu-top',
  templateUrl: './menu-top.component.html',
  styleUrls: ['./menu-top.component.css']
})
export class MenuTopComponent implements OnInit {

  constructor(
    public  elementTypeService : ElementTypeService,
    public  elementService : ElementService,
    public helperService : HelperService,
    public userService : UserService,
    private router: Router,
  ) { }

  ngOnInit() {
    $(document).foundation();
  }

  goToCreate(elementType :  ElementType) {
    switch (elementType.id_element_type) {
      case 1:
        this.router.navigate(['/crear/coleccion']);
        break;
      case 3:
      this.router.navigate(['/crear/panel-instrucciones',this.elementService.currentElement.id_element]); break
      case 2:
      this.router.navigate(['/crear/recorrido',this.elementService.currentElement.id_element]); break;
      case 4:
        this.router.navigate(['/crear/escena-xr',this.elementService.currentElement.id_element]); break;
      case 5:
        this.router.navigate(['/crear/escena-gpar',this.elementService.currentElement.id_element]); break;
      default:
        break;
    }
  }

  logout() {
    this.userService.logout(this.helperService.determinateHost(),this.helperService.getHeaders()).then(
      (success) => {
        this.router.navigate(["/login"]);
      },
      (error) => {
        console.log(error);
      }
    )
  }


}
