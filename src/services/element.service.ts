import { Injectable } from '@angular/core';
import { Element } from 'src/modal/element';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Language } from 'src/modal/language';
import { System } from 'src/modal/system';
import { ElementType } from 'src/modal/element-type';
import { ContentType } from 'src/modal/content-type';
import { Content } from 'src/modal/content';
import { ContentOptions } from 'src/modal/content-options';
import { ProgressService } from './progress.service';
import { ContentService } from './content.service';
import { ErrorService } from './error.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ElementOptions } from 'src/modal/element-options';


@Injectable({
  providedIn: 'root'
})
export class ElementService {
  currentElement : Element;
  elements : Array<Element> = [];
  status = [
    { id : 0, name : "Inactivo"},{ id : 1, name : "Activo"},{ id : 2, name : "Pendiente"}
  ]
  id_element_parent : number;

  constructor(
    public http : HttpClient,
    public progressService : ProgressService,
    public contentService : ContentService,
    public errorService : ErrorService,
  ) { }

  getById(id_element : number, host : String, httpOptions : any) {
    this.delay(2);
    let promise = new Promise((resolve, reject) => {
      this.http.get<Element>(host+'/element/getElement/'+id_element,{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization' : httpOptions['Authorization']
          }
        )
      })
        .toPromise().then(
          (data) => {
            this.currentElement = new Element(
                data.id_element,
                data.id_element_type,
                data.content,
                data.options,
                data.id_version,
                data.status,
                data.id_element_parent
            );
            this.currentElement.instanceOfContent();
            resolve();
          },
          (error) => {
            reject([error.error, error.status]);
          }
        )
    })
    return promise;
  }

  getAllByElementTypeId(id_element_type : number, host : String,httpOptions : any) {
    this.delay(2);
    let promise = new Promise((resolve, reject) => {
      this.http.get<Array<Element>>(host+'/element/getAllByElementTypeId/'+id_element_type,{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization' : httpOptions['Authorization']
          }
        )
      })
        .toPromise().then(
          (data) => {
            this.elements = data.map(item => new Element(
                item.id_element,
                item.id_element_type,
                item.content,
                item.options,
                item.id_version,
                item.status,
                item.id_element_parent
              ).instanceOfContent()


            );
            resolve();
          },
          (error) => {
            reject([error.error, error.status]);
          }
        )
    })
    return promise;
  }

  getElementsByElementParentId(id_element_type : number, host : String, httpOptions : any) {
    this.delay(2);
    let promise = new Promise((resolve, reject) => {
      this.http.get<Array<Element>>(host+'/element/getElementsByElementParentId/'+id_element_type,{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization' : httpOptions['Authorization']
          }
        )
      })
        .toPromise().then(
          (data) => {
            this.elements = data.map(item => new Element(
                item.id_element,
                item.id_element_type,
                item.content,
                item.options,
                item.id_version,
                item.status,
                item.id_element_parent
              ).instanceOfContent()
            );

            resolve();
          },
          (error) => {
            reject([error.error, error.status]);
          }
        )
    })
    return promise;
  }

  save(element : Element,host : String,httpOptions : any) {
    this.delay(2);
    let promise = new Promise((resolve, reject) => {
      this.http.post<Element>(host+'/element/save',element,{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization' : httpOptions['Authorization']
          }
        )
        }).toPromise().then(
          (data) => {
            this.currentElement = new Element(
                data.id_element,
                data.id_element_type,
                data.content,
                new ElementOptions(data.options.travel_mode,data.options.ar_custom),
                data.id_version,
                data.status,
                data.id_element_parent,
                data.id_content_parent
            );
            this.currentElement.instanceOfContent();
            resolve(this.currentElement);
          },
          (error) => {
            reject(error);
          }
        )
    })
    return promise;
  }

  savePositionOfElements(data : any,host : String,httpOptions : any) {
    this.delay(2);
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(host+'/element/savePositionOfElements',data,{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization' : httpOptions['Authorization']
          }
        )
        }).toPromise().then(
          (data) => {
            resolve();
          },
          (error) => {
            reject([error.error, error.status]);
          }
        )
    })
    return promise;
  }

  typing(value : String,language : Language, system : System, contentType : ContentType) {
    let content = this.currentElement.content.find(item => (item.id_language == language.id_language && item.id_system == system.id_system && item.id_content_type == contentType.id_content_type));
    if(content instanceof Content) {
      let index = this.currentElement.content.indexOf(content);
      this.currentElement.content.splice(index,1);
      content.resource = value;
      this.currentElement.content.push(content);
    } else {
      content = new Content(null,value,contentType.id_content_type,language.id_language,this.currentElement.id_element,system.id_system, new ContentOptions(0,0,null,null),1,1,null,[]);
      this.currentElement.content.push(content);
    }
  }

  fileInput(files,language : Language, system : System, contentType : ContentType, host : String,httpOptions : any) {
    let code : String = contentType.options.code;
    this.errorService.errors = [];
    if(contentType.options.findAccept(files.item(0).type) != undefined || contentType.options.accept.length == 0) {
      this.progressService.msg =" Subiendo "+contentType.name.toLocaleLowerCase()+"...";
      this.progressService.progressActiveByName(code);
      let content : Content = this.currentElement.content.find(item => (item.id_language == language.id_language && item.id_system == system.id_system && item.id_content_type == contentType.id_content_type));
      if(content instanceof Content) {
        let index = this.currentElement.content.indexOf(content);
        this.currentElement.content.splice(index,1);
      } else {
        content = new Content(null,"",contentType.id_content_type,language.id_language,this.currentElement.id_element,system.id_system, new ContentOptions(0,0,null,null),1,1,null,[]);
      }
      content.file.append('file',files.item(0));
      this.contentService.save(content.toFormData(),host,httpOptions)
        .then(() => {
          content = this.contentService.content;
          this.currentElement.content.push(content);
          if(this.currentElement.id_element_type == 4) {
            this.currentElement.content.forEach(item => {
              if(content.id_content_type != 29) {
                if(item.id_content_type != content.id_content_type && item.id_content_type != 29) {
                  let index = this.currentElement.content.indexOf(item);
                  let contentFound = this.currentElement.content[index];
                  if(contentFound != undefined) {
                    this.currentElement.content.splice(index,1);
                  }
                }
              }
            })
          }
          this.progressService.progressInactiveByName(code);
        });
    } else {
      this.progressService.progressInactiveByName(code);
      let msg : String = "Tipo de archivo invalido";
      this.errorService.errors.push(msg);
    }
  }

  findContentByLanguageAndSystemAndContentType(language : Language, system : System, contentType : ContentType) {
    return this.currentElement.content.find(item => (item.id_language && language.id_language && item.id_system == system.id_system && item.id_content_type == contentType.id_content_type ));
  }

  changeStatus(value : String) {
    this.currentElement.status = parseInt(value.toString());
  }

  saveAllContent(host : String,httpOptions : any) {
    this.delay(2);
    let promise = new Promise((resolve, reject) => {
      this.progressService.msg = "Guardando";
      this.progressService.progressActiveByName('default');
      let contents : Array<Content> = [];
      if(this.currentElement.content.length > 0) {
        for (let index = 0; index <  this.currentElement.content.length; index++) {
          let content : Content = this.currentElement.content[index];
          content.id_version = 2;
          let contentData = content.toFormData();
          if(contentData != undefined) {
            this.contentService.save(content.toFormData(),host,httpOptions).then(
              (data) =>{
                contents.push(this.contentService.content);
                if(index == (this.currentElement.content.length-1)) {
                  this.progressService.msg = "Redireccionando...";
                  this.progressService.progressInactiveByName('default');
                  resolve(data);
                }
              },
              (error) => {
                reject(error);
              }
            )
          }
        }
      } else {
        resolve();
      }

    });
    return promise;
  }

  saveLocal(key : String) {
    localStorage.setItem(""+key,JSON.stringify(this.currentElement));
  }

  getLocal(key : String) {
    let data : Element = JSON.parse(localStorage.getItem(""+key));
    if(data != null) {
      let elementInstance : Element = new Element(
        data.id_element,
        data.id_element_type,
        data.content,
        new ElementOptions(data.options.travel_mode,data.options.ar_custom),
        data.id_version,
        data.status,
        data.id_element_parent,
        data.id_content_parent
      );
      elementInstance.instanceOfContent();
      return elementInstance;
    } else {
      return null;
    }
  }

  flush(key : String) {
    localStorage.setItem(""+key,JSON.stringify(null));
  }

  delete(id_element : number, host : String,httpOptions : any) {
    this.delay(2);
    let promise = new Promise((resolve, reject) => {
      this.http.get<Element>(host+'/element/delete/'+id_element,{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization' : httpOptions['Authorization']
          }
        )
        })
        .toPromise().then(
          (data) => {
            resolve(data);
          },
          (error) => {
            reject([error.error, error.status]);
          }
        )
    })

    return promise;
  }

  getInstanceOfPlaneObject(element : Element) {
    let newElement : Element = new Element(
      element.id_element,
      element.id_element_type,
      element.content,
      new ElementOptions(element.options.travel_mode,element.options.ar_custom),
      element.id_version,
      element.status,
      element.id_element_parent,
      element.id_content_parent
    );
    newElement.instanceOfContent();
    newElement.gpar = element.gpar;
    return newElement;
  }

  addIdParent(id_element_parent : number) {
    localStorage.setItem('id_element_parent',""+id_element_parent);
  }

  getIdParent() {
    return parseInt(localStorage.getItem('id_element_parent'));
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }



}


