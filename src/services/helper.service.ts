import { Injectable } from '@angular/core';
import { ElementType } from 'src/modal/element-type';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  host : String;
  system : String;
  version : String;
  breadcrumbs: Array<ElementType> = [];

  constructor() { }

  determinateHost(absolutePath : Boolean= true) {
    switch (window.location.hostname) {
        case "localhost":
            this.host ="http://3.220.5.198";
        break;
        case "54.234.180.243" :
        this.host ="http://3.84.107.37:"+window.location.port;
        break;
        case "3.232.133.157" :
          this.host ="http://3.220.5.198:"+window.location.port;
        break;
        default:
          this.host ="http://3.220.5.198:81";
        break;
    }
    if(absolutePath) {
      const dataToken = JSON.parse(localStorage.getItem('APICredentials'));
      let api_rol_param = "";
      if(dataToken != undefined && dataToken != null) {
         api_rol_param = '/api/'+dataToken.user.rol.code;
      }
      this.host+=api_rol_param;
    }
    return this.host;
  }

  addBreadcrumbs(elementType : ElementType) {
    let breadcrumbs = JSON.parse(localStorage.getItem('breadcrumbs'));
    if(breadcrumbs != null && breadcrumbs != undefined) {
      this.breadcrumbs = breadcrumbs;
    }
    if(elementType == null) {
      elementType =  new ElementType(0,"HOME");
    }

    let el = this.breadcrumbs.find(item => (elementType.id_element_type == item.id_element_type));

    if(el != undefined) {
      let index = this.breadcrumbs.indexOf(el);
      this.breadcrumbs.splice((index + 1), (this.breadcrumbs.length - index));
    } else {
      this.breadcrumbs.push(elementType);
    }

    localStorage.setItem('breadcrumbs', JSON.stringify(this.breadcrumbs));
  }

  determinateSystem() {
    switch (window.location.port) {
      case "80":
        this.system = 'AURA';
        break;
      case "81":
        this.system = 'PLAYBOY';
        break;
      default:
        this.system = 'AURA';
        break;
    }
    return this.system;
  }

  determinateVersion() {
    switch (window.location.hostname) {
      case "54.234.180.243":
          this.version = "PRODUCTION";
        break;
      case "18.205.1.254":
        this.version = "DEVELOPER";
        break;
      default:
          this.version = "DEVELOPER";
        break;
    }
    return this.version;
  }

  getHeaders() {
    const dataToken = JSON.parse(localStorage.getItem('APICredentials'));
    const httpOptions = {
        'Accept':  'application/json',
        'Content-Type': 'application/json',
        'Authorization': (dataToken != null) ? dataToken.token_type+" "+dataToken.access_token : ''
    };
    return httpOptions;
  }


}
