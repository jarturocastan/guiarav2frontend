import { Injectable } from '@angular/core';
import { System } from 'src/modal/system';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SystemService {
	system : System;
	systems : Array<System> = [];
	systemsCombobox : Array<System> = [];

	constructor(
    public http : HttpClient,
    private router: Router,
    private route: ActivatedRoute,
	) { }

  /**@Relese*/
	getAll(host : String, httpOptions : any) {
    let promise = new Promise((resolve, reject) => {
      if(this.systems.length > 0) {
        resolve();
      } else {
        this.http.get<Array<System>>(host+'/system/getAll',{
          headers: new HttpHeaders({
              'Content-Type': 'application/json',
              'Authorization' : httpOptions['Authorization']
            })
          }).toPromise()
          .then(
            (data) => {
              this.systems = data.map(item => new System(item.id_system,item.name,item.options));
              resolve();
            },
            (error) => {
              this.router.navigate(["/error",error.status,error['message'].replace(new RegExp(' ', 'g'),'_%')]);
            }
          )
      }
    })
    return promise;
  }


  findSystemById(id_system : number) : System {
    return this.systems.find(item => (item.id_system == id_system));
  }

	determinateSystemsCombobox(id_element_type : number) {
        switch (id_element_type) {
            case 1:
                this.systemsCombobox = [];
                this.systemsCombobox = this.findMany([1]);
                this.changeSystem(this.systemsCombobox[0].name);
                break;
            case 3:
                this.systemsCombobox = [];
                this.systemsCombobox = this.findMany([1]);
                this.changeSystem(this.systemsCombobox[0].name);
                break;
            case 4:
                this.systemsCombobox = [];
                this.systemsCombobox = this.findMany([1]);
                this.changeSystem(this.systemsCombobox[0].name);
                break;
            case 5:
                this.systemsCombobox = [];
                this.systemsCombobox = this.findMany([1]);
                this.changeSystem(this.systemsCombobox[0].name);
                break;
            case 2:
                this.systemsCombobox = [];
                this.systemsCombobox = this.findMany([1]);
                this.changeSystem(this.systemsCombobox[0].name);
                break;
            default:
                this.systemsCombobox = [];
                break;
        }
	}

    findMany(ids_system : Array<number>) {
        let systems : Array<System> = [];
        ids_system.forEach(id => {
            let system = this.systems.find(item =>(item.id_system == id));
            if(system instanceof System) {
                systems.push(system);
            }
        });
        return systems;
	}

	changeSystem(value : String) {
        let system : System = this.systemsCombobox.find(item => (item.name == value));
        if(system instanceof System) {
            this.system = system;
        }
    }
}
