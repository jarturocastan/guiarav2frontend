import { Injectable } from '@angular/core';
import { ContentType } from 'src/modal/content-type';
import { ContentTypeOptions } from 'src/modal/content-type-options';
import { HttpClient , HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContentTypeService {
  contentTypes : Array<ContentType> = [];
  contentTypeCombobox : Array<ContentType> = [];
  contentType : ContentType;

  constructor(
    public http : HttpClient,
  ) { }

  getAll(host : String,httpOptions : any) {
    let promise = new Promise((resolve, reject) => {
        if(this.contentTypes.length > 0) {
          resolve();
        } else {
          this.http.get<Array<ContentType>>(host+'/content-type/getAll',{
            headers: new HttpHeaders(
              {
                'Content-Type': 'application/json',
                'Authorization' : httpOptions['Authorization']
              }
            )
            }).toPromise().then(
                  (data) => {
                      this.contentTypes = data.map(item => new ContentType(
                        item.id_content_type,
                        item.name,
                        new ContentTypeOptions(item.options.type,item.options.code,item.options.language,item.options.system,item.options.events,item.options.actions,item.options.accept)));
                      resolve();
                  },
                  (error) => {
                      reject();
                  }
              )
        }

    })
    return promise;
  }

  findContentTypeById(id_content_type : number) : ContentType {
    return this.contentTypes.find(item => (item.id_content_type == id_content_type));
  }

  findContentTypeNameById(id_content_type : number) : String {
    return this.contentTypes.find(item => (item.id_content_type == id_content_type)).name;
  }

  getAcceptByContentType(id_content_type : number) : String {
    let contentType : ContentType = this.findContentTypeById(id_content_type);
    if(contentType instanceof ContentType) {
      let accept : String = "";
      contentType.options.accept.forEach(acceptCode => {
        accept+=acceptCode+",";
      })
      return accept;
    }
    return "";
  }

  determinateContentTypeCombobox(id_element_type : number) {
    switch (id_element_type) {
        case 1:
            this.contentTypeCombobox = [];
            break;
        case 4:
            this.contentTypeCombobox = [];
            this.contentTypeCombobox = this.findMany([15,16,17,19]);
            this.contentType = this.contentTypeCombobox[0];
            break;
        case 3:
            this.contentTypeCombobox = [];
            this.contentTypeCombobox = this.findMany([12,1,10,30]);
            this.contentType = this.contentTypeCombobox[0];
            break;
        case 5:
            this.contentTypeCombobox = [];
            this.contentTypeCombobox = this.findMany([18]);
            this.contentType = this.contentTypeCombobox[0];
            break;
        default:
            this.contentTypeCombobox = [];
            break;
    }
  }

  findMany(ids_content_type : Array<number>) {
    let contentTypes : Array<ContentType> = [];
    ids_content_type.forEach(id => {
        let contentType = this.contentTypes.find(item =>(item.id_content_type == id));
        if(contentType instanceof ContentType) {
          contentTypes.push(contentType);
        }
    });
    return contentTypes;
  }


}
