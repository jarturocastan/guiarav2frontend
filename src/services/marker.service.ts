import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProgressService } from './progress.service';
import { ContentService } from './content.service';
import { ErrorService } from './error.service';
import { Language } from 'src/modal/language';
import { System } from 'src/modal/system';
import { ContentType } from 'src/modal/content-type';
import { ElementService } from './element.service';
import { ContentOptions } from 'src/modal/content-options';
import { Content } from 'src/modal/content';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MarkerService {
  currentMarker : Content;
  markers : Array<Content> = [];
  vuforia_target_ids_to_delete : Array<number> = null;
  elementInMarker : Content;

  constructor(
    public http : HttpClient,
    public progressService : ProgressService,
    public contentService : ContentService,
    public errorService : ErrorService,
    public elementService : ElementService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  fileMarker(files,language : Language, system : System, contentType : ContentType, host : String, httpOptions : any) {
    this.delay(2);
    let code : String = contentType.options.code;
    if(contentType.options.findAccept(files.item(0).type) != undefined) {
      this.progressService.msg =" Subiendo marcador...";
      this.progressService.progressActiveByName(code);
      let content : Content = new Content(null,"",contentType.id_content_type,language.id_language,this.elementService.currentElement.id_element,system.id_system, new ContentOptions(0,0,null,null),1,1,null,[]);
      content.file.append('file',files.item(0));
      this.contentService.save(content.toFormData(),host,httpOptions)
        .then(() => {
          if(this.contentService.content.options.rating >= 2) {
            this.progressService.msg =" Redirigiendo marcador...";
            this.contentService.content.elementsInMarker =[];
            content = this.contentService.content;
           // this.markers.push(content);
            this.addCurrentMarker(content);
            this.progressService.progressInactiveByName(code);
            this.progressService.clean();
            this.elementService.saveLocal('instruction-gpar');
            this.progressService.progressRun = false;
            this.router.navigate(['/canvas',0]);
          } else {
            this.progressService.progressInactiveByName(code);
            let msg : String = "El marcado que intentas subir tiene un ranking de "+this.contentService.content.options.rating+" y debe ser mayor 3";
            this.errorService.errors.push(msg);
          }
          //this.addVuforiaTargetIdToDelete(this.contentService.content.options.vuforia_target_id);
        });
    }
  }

  addVuforiaTargetIdToDelete(id : number) {
    this.vuforia_target_ids_to_delete = JSON.parse(localStorage.getItem('vuforia_ids_to_delete'));
    if(this.vuforia_target_ids_to_delete == undefined || this.vuforia_target_ids_to_delete == null) {
      this.vuforia_target_ids_to_delete = [];
    }
    this.vuforia_target_ids_to_delete.push(id);
    localStorage.setItem('vuforia_ids_to_delete',JSON.stringify(this.vuforia_target_ids_to_delete));
  }

  getVuforiaTargetIdToDelete() {
    this.vuforia_target_ids_to_delete = JSON.parse(localStorage.getItem('vuforia_ids_to_delete'));
    if(this.vuforia_target_ids_to_delete == undefined || this.vuforia_target_ids_to_delete == null) {
      this.vuforia_target_ids_to_delete = [];
    }
    return this.vuforia_target_ids_to_delete;
  }

  addCurrentMarker(marker : Content) {
    this.currentMarker = marker;
    localStorage.setItem('marker',JSON.stringify(marker));
  }

  getCurrentMarker() {
    let data = JSON.parse(localStorage.getItem('marker'));
    this.currentMarker = new Content(data["id_content"],data["resource"],data["id_content_type"],data["id_language"],data["id_element"],data["id_system"], new ContentOptions(data["options"].vuforia_target_id,data["options"].rating,null,null),data["id_version"],data["status"],data["id_content_parent"],data["elementsInMarker"]);
    return this.currentMarker;
  }

  typing(value : String,language : Language, system : System, contentType : ContentType) {
    let content = this.elementInMarker.childs.find(item => (item.id_language == language.id_language && item.id_system == system.id_system && item.id_content_type == contentType.id_content_type));
    if(content != undefined) {
      let index = this.elementInMarker.childs.indexOf(content);
      this.elementInMarker.childs.splice(index,1);
      content.resource = value;
      this.elementInMarker.childs.push(content);
    } else {
      content = new Content(null,value,contentType.id_content_type,language.id_language,this.currentMarker.id_element,system.id_system, new ContentOptions(0,0,null,null),1,1,this.elementInMarker.id_content,[]);
      this.elementInMarker.childs.push(content);
    }
  }

  fileInputChild(files,language : Language, system : System, contentType : ContentType, host : String,httpOptions : any) {
    this.delay(2);
    this.errorService.clean();
    let code : String = contentType.options.code;
    this.progressService.progressActiveByName(code);
    if(contentType.options.findAccept(files.item(0).type) != undefined) {
      this.progressService.msg =" Subiendo "+contentType.name.toLocaleLowerCase()+"...";
      this.progressService.progressActiveByName(code);
      let elementInMarker = this.elementInMarker.childs.find((data) => { return (data.id_system == system.id_system && data.id_language == language.id_language && contentType.id_content_type == data.id_content_type) })
      if(elementInMarker == null || elementInMarker == undefined) {
        elementInMarker = new Content(null,
         "",
         contentType.id_content_type,
         language.id_language,
         this.elementService.getLocal('instruction-gpar').id_element,
         system.id_system,
         new ContentOptions(
           0,0,null,
           null)
           ,1,1,this.elementInMarker.id_content,[]);
      }
      elementInMarker.id_language = language.id_language;
      elementInMarker.id_system = system.id_system;
      elementInMarker.id_content_type = contentType.id_content_type;
      elementInMarker.resource ="";
      elementInMarker.id_content_parent = this.elementInMarker.id_content;
      if(elementInMarker instanceof Content) {
        elementInMarker.file.append('file',files.item(0));
        this.contentService.save(elementInMarker.toFormData(),host,httpOptions)
          .then(() => {
            this.elementInMarker.childs.push(this.contentService.content);
            this.progressService.progressInactiveByName(code);
          });

      } else {
        this.progressService.progressInactiveByName(code);
        let msg : String = "Error elemento en marcado no instanciado";
        this.errorService.errors.push(msg);
      }
    } else {
      this.progressService.progressInactiveByName(code);
      let msg : String = "Tipo de archivo invalido";
      this.errorService.errors.push(msg);
    }
  }

  findChildByContentTypeAndLanguageAndSystem(language : Language, system : System, contentType : ContentType) {
    let child = this.elementInMarker.childs.find(item => (item.id_language == language.id_language && item.id_system == system.id_system && item.id_content_type == contentType.id_content_type));
    return child;
  }

  fileInput(files,language : Language, system : System, contentType : ContentType, host : String,httpOptions : any) {
    this.delay(2);
    this.errorService.clean();
    let code : String = contentType.options.code;
    this.progressService.progressActiveByName(code);
    if(contentType.options.findAccept(files.item(0).type) != undefined) {
      this.progressService.msg =" Subiendo "+contentType.name.toLocaleLowerCase()+"...";
      this.progressService.progressActiveByName(code);
      this.elementInMarker.id_language = language.id_language;
      this.elementInMarker.id_system = system.id_system;
      this.elementInMarker.id_content_type = contentType.id_content_type;
      this.elementInMarker.resource ="";
      this.elementInMarker.id_content_parent = this.getCurrentMarker().id_content;
      if(this.elementInMarker instanceof Content) {
        this.elementInMarker.file.append('file',files.item(0));
        this.contentService.save(this.elementInMarker.toFormData(),host,httpOptions)
          .then(() => {
            this.contentService.content.childs = this.elementInMarker.childs;
            this.contentService.content.sceneXR = this.elementInMarker.sceneXR;
            this.contentService.content.options = this.elementInMarker.options;
            this.elementInMarker = this.contentService.content;
            this.progressService.progressInactiveByName(code);
          });

      } else {
        this.progressService.progressInactiveByName(code);
        let msg : String = "Error elemento en marcado no instanciado";
        this.errorService.errors.push(msg);
      }
    } else {
      this.progressService.progressInactiveByName(code);
      let msg : String = "Tipo de archivo invalido";
      this.errorService.errors.push(msg);
    }
  }

  deleteMarker(marker : Content, host : String,httpOptions : any) {
    this.progressService.msg ="Eliminado marcador...";
    this.progressService.progressActiveByName('default');
    this.delay(2);
    let index : number = this.markers.indexOf(marker);
    let ids : Array<number> = [];
    this.contentService.delete(marker.id_content,host,httpOptions).then(
      (data) => {
        ids.push(marker.id_content);
        for (let index = 0; index < marker.elementsInMarker.length; index++) {
          ids.push(marker.elementsInMarker[index].id_content);
          this.contentService.delete(marker.elementsInMarker[index].id_content,host,httpOptions).then(
            (data) => {

            },
            (error) => {}
          )
        }
        this.markers.splice(index,1);
        ids.forEach(id => {
          let content = this.elementService.currentElement.content.find( item => (item.id_content == id));
          if(content != undefined) {
            let index = this.elementService.currentElement.content.indexOf(content);
            this.elementService.currentElement.content.splice(index,1);
          }
        })
        this.progressService.progressInactiveByName('default');
      },
      (error) => {}
    )
  }

  goToMarkerCanvas(marker : Content) {
    this.addCurrentMarker(marker);
    this.progressService.clean();
    this.elementService.saveLocal('instruction-gpar');
    this.router.navigate(['/canvas',1]);
  }

  clean() {
    this.markers = [];
    this.currentMarker = null;
  }

  findChildByContentTypeIdAndLanguageId(id_content_type : number, id_language : number) {
    let content : Content = this.elementInMarker.childs.find(item => (item.id_content_type == id_content_type && item.id_language == id_language));
    if(content != undefined) {
      return content.resource;
    } else {
      return "";
    }
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  savePositionOfMarker(data : any,host : String, httpOptions : any) {
    this.delay(2);
    let promise = new Promise((resolve, reject) => {
      this.http.post<any>(host+'/content/savePositionOfContents',data,{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization' : httpOptions['Authorization']
          }
        )
      }).toPromise().then(
          (data) => {
            resolve(data);
          },
          (error) => {
            reject([error.error, error.status]);
          }
        )
    })
    return promise;
  }
}
