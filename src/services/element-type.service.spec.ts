import { TestBed } from '@angular/core/testing';

import { ElementTypeService } from './element-type.service';

describe('ElementTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ElementTypeService = TestBed.get(ElementTypeService);
    expect(service).toBeTruthy();
  });
});
