import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProgressService {
  progress : Array<Boolean> = [];
  msg : String = "Cargando datos";
  type : number = 0;
  progressRun : Boolean = true;

  constructor() { }

  isProgressRun() {
    let  keys : Array<String> = Object.keys(Object.assign([], this.progress));
    return keys.some( key => {
        return (this.progress[""+key] == true);     
    })
  } 

  progressActiveByName(name : String) {
    this.progress[""+name] = true;
    this.progressRun = this.isProgressRun();
  }

  progressInactiveByName(name : String) {
    this.progress[""+name] = false;
    this.progressRun = this.isProgressRun();
  }

  clean() {
    this.type = 0;
    this.progressRun = true;
    this.msg = "Cargando datos";
  }
}
