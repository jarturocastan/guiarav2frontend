import { Injectable } from '@angular/core';
import { Language } from 'src/modal/language';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
    language : Language;
    languages : Array<Language> = [];
    languagesCombobox : Array<Language> = [];

    constructor(
        public http : HttpClient
    ) { }


    getAll(host : String,httpOptions : any) {
        let promise = new Promise((resolve, reject) => {
            if(this.languages.length > 0) {
                resolve();
            } else {
                this.http.get<Array<Language>>(host+'/language/getAll',{
                  headers: new HttpHeaders(
                    {
                      'Content-Type': 'application/json',
                      'Authorization' : httpOptions['Authorization']
                    }
                  )
                })
                    .toPromise().then(
                        (data) => {
                            this.languages = data.map(item => new Language(item.id_language,item.name,item.options));
                            resolve();
                        },
                        (error) => {
                            reject([error.error, error.status]);
                        }
                    )
            }

        })
        return promise;
    }

    determinateLanguagesCombobox(id_element_type : number) {
        switch (id_element_type) {
            case 1:
                this.languagesCombobox = [];
                this.languagesCombobox = this.findMany([2,3,4]);
                this.changeLanguage(this.languagesCombobox[0].name);
                break;
            case 2:
                this.languagesCombobox = [];
                this.languagesCombobox = this.findMany([2,3,4]);
                this.changeLanguage(this.languagesCombobox[0].name);
                break;
            case 3:
                this.languagesCombobox = [];
                this.languagesCombobox = this.findMany([2,3,4]);
                this.changeLanguage(this.languagesCombobox[0].name);
                break;
            case 5:
                this.languagesCombobox = [];
                this.languagesCombobox = this.findMany([2,3,4]);
                this.changeLanguage(this.languagesCombobox[0].name);
                break;
            default:
                this.languagesCombobox = [];
                break;
        }
    }

    findMany(ids_language : Array<number>) {
        let languages : Array<Language> = [];
        ids_language.forEach(id => {
            let language = this.languages.find(item =>(item.id_language == id));
            if(language instanceof Language) {
                languages.push(language);
            }
        });
        return languages;
    }

    changeLanguage(value : String) {
        let language : Language = this.languagesCombobox.find(item => (item.name == value));
        if(language instanceof Language) {
            this.language = language;
        }
    }

    findLanguageById(id_language : number) : Language {
        return this.languages.find(item => (item.id_language == id_language));
    }

    findLanguageByName(name : String) : Language {
        return this.languages.find(item => (item.name == name));
    }



}
