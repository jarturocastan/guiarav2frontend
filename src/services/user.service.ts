import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})

export class UserService {

	constructor(
		public http : HttpClient,
  ) { }

  getUser(host : String, httpOptions) {
    let promise = new Promise((resolve, reject) => {
      this.http.get<any>(host+'/getUser',{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/form-data',
            'Accept': 'application/form-data',
            'Authorization' : httpOptions['Authorization']
          }
        )
      }).toPromise().then(
        (success) => {
          resolve(success);
        },
        (error) => {
          reject(error);
        }
      )
    })
    return promise;
  }

	logIn(host : String, data : any) {
		let promise = new Promise((resolve, reject) => {
			this.http.post<any>(host+'/oauth/token',data).toPromise().then(
					(success) => {
            console.log('user ::',success);
						localStorage.setItem('APICredentials', JSON.stringify(success));
						resolve(success);
					},
					(error) => {
						reject(error);
					}
				)
		})
		return promise;
  }

  logout(host : String, httpOptions : any) {
    let promise = new Promise((resolve, reject) => {
			this.http.get<any>(host+'/user/logout',{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/form-data',
            'Accept': 'application/form-data',
            'Authorization' : httpOptions['Authorization']
          }
        )
      }).toPromise().then(
					(success) => {
						localStorage.clear();
						resolve(success);
					},
					(error) => {
						reject(error);
					}
				)
    })
    return promise;
  }

  getCredentials() {
    return JSON.parse(JSON.stringify('APICredentials'));
  }
}
