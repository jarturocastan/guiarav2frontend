import { Injectable } from '@angular/core';
import { ElementType } from 'src/modal/element-type';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ElementTypeService {
  menu : Array<ElementType> = [];
  elementTypes : Array<ElementType> = [];
  nextElementTypes : Array<ElementType> = [];
  currentElementType : ElementType;
  nextElementType : ElementType;

  constructor(
    public http : HttpClient,
  ) {

  }

  getAll(host : String,httpOptions : any) {
    let promise = new Promise((resolve, reject) => {
      if(this.elementTypes.length > 0) {
        resolve();
      } else {
        this.http.get<Array<ElementType>>(host+'/element-type/getAll',{
          headers: new HttpHeaders(
            {
              'Content-Type': 'application/json',
              'Authorization' : httpOptions['Authorization']
            }
          )
        })
          .toPromise().then(
            (data) => {
              this.elementTypes = data.map(item => new ElementType(item.id_element_type,item.name));
              resolve();
            },
            (error) => {
              reject([error.error, error.status]);
            }
          )
      }
    })
    return promise;
  }

  determinateMenu(id_element_type : number, forceTo : number  = 0) {
    let elementType : ElementType;
    if(forceTo == 0) {
      switch (id_element_type) {
        case 0:
          this.menu = [];
           elementType = this.findElementTypeById(1);

          if(elementType instanceof ElementType) {
            this.menu.push(elementType);
          }
          break;
        case 1:
          this.menu = [];
          elementType  = this.findElementTypeById(3);

          if(elementType instanceof ElementType) {
            this.menu.push(elementType);
          }
          elementType  = this.findElementTypeById(2);

          if(elementType instanceof ElementType) {
            this.menu.push(elementType);
          }
          break;
        case 4:
          this.menu = [];
          elementType  = this.findElementTypeById(5);

          if(elementType instanceof ElementType) {
            this.menu.push(elementType);
          }
          break;
        case 2:
          this.menu = [];
          elementType  = this.findElementTypeById(3);

          if(elementType instanceof ElementType) {
            this.menu.push(elementType);
          }

          elementType  = this.findElementTypeById(4);

          if(elementType instanceof ElementType) {
            this.menu.push(elementType);
          }

          break;
        default:
          this.menu = [];

          break;
      }
    } else {
      this.menu = [];
      elementType = this.findElementTypeById(forceTo);

     if(elementType instanceof ElementType) {
       this.menu.push(elementType);
     }
    }
  }

  findElementTypeById(id_element_type : number) : ElementType {
    return this.elementTypes.find(item => (item.id_element_type == id_element_type));
  }

  determinateNextElementTypes(id_element_type : number) {
    switch (id_element_type) {
      case 1:
        this.nextElementTypes = this.findMany([2,3]);
        this.changeNextElementType(this.nextElementTypes[0].name);
        break;
      case 2:
        this.nextElementTypes = []
        this.nextElementTypes = this.findMany([4,3]);
        this.changeNextElementType(this.nextElementTypes[0].name);
        break;

      default:
        this.nextElementTypes = [];
        break;
    }

  }

  findMany(ids_elementTypes : Array<number>) {
    let elementTypes : Array<ElementType> = [];
    ids_elementTypes.forEach(id => {
        let elementType : ElementType = this.elementTypes.find(item =>(item.id_element_type == id));

        if(elementType instanceof ElementType) {
          elementTypes.push(elementType);
        }
    });
    return elementTypes;
  }

  changeNextElementType(name : String) {
    this.nextElementType = this.nextElementTypes.find(item => (item.name == name));
  }

}
