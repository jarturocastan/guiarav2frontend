import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Content } from 'src/modal/content';
import { ContentOptions } from 'src/modal/content-options';

@Injectable({
  providedIn: 'root'
})

export class ContentService {
  content : Content;

  constructor(
    public http : HttpClient,
  ) { }

  save(contentFormData : FormData, host : String, httpOptions : any) {
    let promise = new Promise((resolve, reject) => {
      this.content = null;
      this.delay(2);

      this.http.post<Content>(host+'/content/save',contentFormData,{
        headers: new HttpHeaders(
          {
            'Content-Type': 'multipart/form-data',
            'Accept': 'application/json',
            'Authorization' : httpOptions['Authorization']

          }
        )
      })
        .toPromise().then(
          (data) => {
            this.content = new Content(
                data.id_content,
                data.resource,
                data.id_content_type,
                data.id_language,
                data.id_element,
                data.id_system,
                new ContentOptions(data.options.vuforia_target_id,data.options.rating,data.options.event,data.options.action,data.options.canvas),
                data.id_version,
                data.status,
                data.id_content_parent,
                data.elementsInMarker);
            resolve();
          },
          (error) => {
            reject(error);
          }
        )

    });
    return promise;
  }

  savePositionOfContents(data : any,host : String, httpOptions : any) {
    let promise = new Promise((resolve, reject) => {
      this.delay(2);
      this.http.post<any>(host+'/content/savePositionOfContents',data)
        .toPromise().then(
          (data) => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        )
    })
    return promise;
  }

  delete(id_number : number, host : String,httpOptions : any) {
    let promise = new Promise((resolve, reject) => {
      this.delay(2);
      this.http.get<Content>(host+'/content/delete/'+id_number,{
        headers: new HttpHeaders(
          {
            'Content-Type': 'application/json',
            'Authorization' : httpOptions['Authorization']
          }
        )
      }).toPromise().then(
          (data) => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        )
    })

    return promise;
  }

  createZip(id_number : number, host : String, system : String) {
    let promise = new Promise((resolve, reject) => {
      this.delay(2);
      this.http.get<Content>(host+'/api/app/createZip/'+id_number+'/'+system)
        .toPromise().then(
          (data) => {
            console.log('zip created ::'+id_number);
            resolve();
          },
          (error) => {
            reject(error);
          }
        )
    })

    return promise;
  }

  deleteNotIn(ids : Array<number>, id_element : number, host : String) {
    let promise = new Promise((resolve, reject) => {
      this.delay(2);
      this.http.post<Element>(host+'/content/deleteNotIn/'+id_element,{ ids : ids})
        .toPromise().then(
          (data) => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        )
    })

    return promise;
  }

  getInstanceOfPlaneObject(object : Content) {
    return new Content(
      object.id_content,
      object.resource,
      object.id_content_type,
      object.id_language,
      object.id_element,
      object.id_system,
      new ContentOptions(
        object.options.vuforia_target_id,
        object.options.rating,
        object.options.event,
        object.options.action,
        object.options.canvas,
        object.options.loop,
        object.options.transparency,
        object.options.subtitle,
        object.options.frame,
        object.options.frameStart,
        object.options.frameEnd,
        object.options.position
      ),
      object.id_version,
      object.status,
      object.id_content_parent,
      (object.elementsInMarker != undefined) ?object.elementsInMarker : []).instanceOfElementsInMarker();
  }

  async delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
}
