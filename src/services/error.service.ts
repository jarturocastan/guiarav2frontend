import { Injectable } from '@angular/core';
import { Element } from 'src/modal/element';
import { Content } from 'src/modal/content';
import { ProgressService } from './progress.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  errors : Array<String> = [];

  constructor(
    public progressService : ProgressService
  ) { }


  validateElement(element : Element) : Boolean{
    this.progressService.msg = "Validando a datos";
    this.progressService.progressActiveByName('default');
    let isValid : Boolean = false;
    let content : Content;
    switch (element.id_element_type) {
      case 1:
        this.errors = [];
         content = element.content.find(item => (item.id_content_type == 4));
        if(content instanceof Content) {
          isValid = true;
        } else {
          let msg : String = "Thumbnail es obligatorio";
          this.errors.push(msg);
        }
        break;
      case 2:
        this.errors = [];
         content = element.content.find(item => (item.id_content_type == 4));
        if(content instanceof Content) {
          isValid = true;
        } else {
          let msg : String = "Thumbnail es obligatorio";
          this.errors.push(msg);
        }
        break;
      case 4 :
        this.errors = [];
        if(element.content.length > 0) {
          isValid = true;
        } else {
          let msg : String = "Campo obligatorio";
          this.errors.push(msg);
        }
        break;
      case 6 :
          this.errors = [];
          if(element.content.length > 0) {
            isValid = true;
          } else {
            let msg : String = "Campo obligatorio";
            this.errors.push(msg);
          }
          break;
      case 5:
        this.errors = [];
        let marker = element.content.find(item => (item.id_content_type == 2));
        if(marker instanceof Content) {
          isValid = true;
        } else {
          let msg : String = "Marcador es obligatorio";
          this.errors.push(msg);
        }
        break;
        case 3:
        this.errors = [];
        if(element.options.ar_custom) {
          let footer : Content = element.content.find(item=>(item.id_content_type == 24));
          let content_aa : Content = element.content.find(item=>(item.id_content_type == 25));
          let content_ab : Content = element.content.find(item=>(item.id_content_type == 26));
          let content_ba : Content = element.content.find(item=>(item.id_content_type == 27));
          let content_bb : Content = element.content.find(item=>(item.id_content_type == 28));
          if(footer != undefined && content_aa != undefined && content_ab != undefined && content_ba != undefined && content_bb != undefined) {
            let clip : Content = element.content.find(item=>(item.id_content_type == 14));
            if(clip != undefined) {
              isValid = true;
            } else {
              let msg : String = "El clip es obligatorio";
              this.errors.push(msg);
            }
          } else {
            let msg : String = "Las imagenes son obligatorias";
            this.errors.push(msg);
          }
        } else {
          let clip : Content = element.content.find(item=>(item.id_content_type == 14));

          if(clip != undefined) {
            isValid = true;
          } else {
            let msg : String = "El clip es obligatorio";
            this.errors.push(msg);
          }
        }
        break;
      default:
        break;
    }
    this.progressService.progressInactiveByName('default');
    return isValid;
  }

  validateElementInMarker(content : Content) : Boolean {
    this.progressService.msg = "Validando a datos";
    this.progressService.progressActiveByName('default');
    let isValid : Boolean = false;
    let reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]';
    switch (content.id_content_type) {
      case 1:
        this.errors = [];
        if(content.resource != undefined) {
          if(content.options.action == "OpenURL") {
            if(content.childs != undefined) {
              let child = content.childs.find(item => (item.id_content_type == 23));
              if(child instanceof Content) {
                if(child.resource.match(reg)) {
                  isValid = true;
                } else {
                  let msg : String = "El formato del enlace es incorrecto";
                  this.errors.push(msg);
                }
              } else {
                let msg : String = "El enlace es obligatorio";
                this.errors.push(msg);
              }
            } else {
              let msg : String = "El enlace es obligatorio";
              this.errors.push(msg);
            }
          } else {
            isValid = true;
          }
        } else {
          let msg : String = "Asset bundle es obligatorio";
          this.errors.push(msg);
        }
        break;
      case 12:
        this.errors = [];
        if(content.resource != undefined) {
          if(content.options.loop) {
            if(content.options.frameStart > 0 && content.options.frameEnd > 0) {
              if(content.options.subtitle) {
                if(content.childs != undefined) {
                  let child = content.childs.find(item => (item.id_content == 11));
                  if(child instanceof Content) {

                    if(content.options.action == "OpenURL") {
                      if(content.childs != undefined) {
                        let child = content.childs.find(item => (item.id_content_type == 23));
                        if(child != undefined) {
                          if(child.resource.match(reg)) {
                            let child = content.childs.find(item => (item.id_content_type == 3));
                            if(child != undefined) {
                              if(child.resource.match(reg)) {
                                isValid = true;
                              } else {
                                let msg : String = "El formato de la url de google maps es incorrecto";
                                this.errors.push(msg);
                              }
                            } else {
                              isValid = true;
                            }
                          } else {
                            let msg : String = "El formato del enlace es incorrecto";
                            this.errors.push(msg);
                          }
                        } else {
                          let msg : String = "El enlace es obligatorio";
                          this.errors.push(msg);
                        }
                      } else {
                        let msg : String = "El enlace es obligatorio";
                        this.errors.push(msg);
                      }
                    } else {

                      let child = content.childs.find(item => (item.id_content_type == 3));
                      if(child != undefined) {
                        if(child.resource.match(reg)) {
                          isValid = true;
                        } else {
                          let msg : String = "El formato de la url de google maps es incorrecto";
                          this.errors.push(msg);
                        }
                      } else {
                        isValid = true;
                      }
                    }
                  } else {
                    let msg : String = "Debes ingresar por lo menos un subtitulo";
                    this.errors.push(msg);
                  }
                } else {
                  let msg : String = "Debes ingresar por lo menos un subtitulo";
                  this.errors.push(msg);
                }
              } else {
                if(content.options.action == "OpenURL") {
                  if(content.childs != undefined) {
                    let child = content.childs.find(item => (item.id_content_type == 23));
                    if(child !=  undefined) {
                          if(child.resource.match(reg)) {
                            isValid = true;
                          } else {
                            let msg : String = "El formato del enlace es incorrecto";
                            this.errors.push(msg);
                          }
                    } else {
                      let msg : String = "El enlace es obligatorio";
                      this.errors.push(msg);
                    }
                  } else {
                    let msg : String = "El enlace es obligatorio";
                    this.errors.push(msg);
                  }
                } else {
                  isValid = true;
                }
              }
            } else {
              if(content.options.frameStart >= content.options.frameEnd) {
                let msg : String = "Loop start frame y Loop finish frame es obligatorio";
                this.errors.push(msg);
              } else {
                let child = content.childs.find(item => (item.id_content_type == 3));
                if(child != undefined) {
                  if(child.resource.match(reg) != null) {
                    isValid = true;
                  } else {
                    let msg : String = "El formato de la url de google maps es incorrecto";
                    this.errors.push(msg);
                  }
                } else {
                  isValid = true;
                }
              }
            }
          } else {
            if(content.options.subtitle) {
              if(content.childs != undefined) {
                let child = content.childs.find(item => (item.id_content_type == 11));
                if(child  != undefined) {
                  if(content.options.action == "OpenURL") {
                    if(content.childs != undefined) {
                      let child = content.childs.find(item => (item.id_content_type == 23));
                      if(child != undefined) {
                        if(child.resource.match(reg)) {
                          isValid = true;
                        } else {
                          let msg : String = "El formato del enlace es incorrecto";
                          this.errors.push(msg);
                        }
                      } else {
                        let msg : String = "El enlace es obligatorio";
                        this.errors.push(msg);
                      }
                    } else {
                      let msg : String = "El enlace es obligatorio";
                      this.errors.push(msg);
                    }
                  } else {
                    let child = content.childs.find(item => (item.id_content_type == 3));
                    if(child != undefined) {
                      if(child.resource.match(reg) != null) {
                        isValid = true;
                      } else {
                        let msg : String = "El formato de la url de google maps es incorrecto";
                        this.errors.push(msg);
                      }
                    } else {
                      isValid = true;
                    }
                  }
                } else {
                  let msg : String = "Debes ingresar por lo menos un subtitulo";
                  this.errors.push(msg);
                }
              } else {
                let msg : String = "Debes ingresar por lo menos un subtitulo";
                this.errors.push(msg);
              }
            } else {

              if(content.options.action == "OpenURL") {
                if(content.childs != undefined) {
                  let child = content.childs.find(item => (item.id_content_type == 23));
                  if(child != undefined) {
                    if(child.resource.match(reg) != null) {
                      let child = content.childs.find(item => (item.id_content_type == 3));

                      if(child != undefined) {
                        if(child.resource.match(reg) != null) {
                          isValid = true;
                        } else {
                          let msg : String = "El formato de la url de google maps es incorrecto";
                          this.errors.push(msg);
                        }
                      } else {
                        isValid = true;
                      }
                    } else {
                      let msg : String = "El formato del enlace es incorrecto";
                      this.errors.push(msg);
                    }
                  } else {
                    let msg : String = "El enlace es obligatorio";
                    this.errors.push(msg);
                  }
                } else {
                  let msg : String = "El enlace es obligatorio";
                  this.errors.push(msg);
                }
              } else {
                let child = content.childs.find(item => (item.id_content_type == 3));
                if(child != undefined) {
                  if(child.resource.match(reg) != null) {
                    let child = content.childs.find(item => (item.id_content_type == 23));
                    if(child != undefined) {
                      if(child.resource.match(reg) != null) {
                        isValid = true;
                      } else {
                        let msg : String = "El formato de la url de google maps es incorrecto";
                        this.errors.push(msg);
                      }
                    }
                  } else {
                    let child = content.childs.find(item => (item.id_content_type == 23));
                    if(child != undefined) {
                      if(child.resource.match(reg) != null) {
                        isValid = true;
                      } else {
                        let msg : String = "El formato de la url de google maps es incorrecto";
                        this.errors.push(msg);
                      }
                    } else {
                      let msg : String = "El formato de la url de google maps es incorrecto";
                      this.errors.push(msg);
                    }
                  }
                } else {
                  let child = content.childs.find(item => (item.id_content_type == 23));
                  if(child != undefined) {
                    if(child.resource.match(reg) != null) {
                      isValid = true;
                    } else {
                      let msg : String = "El formato de la url de google maps es incorrecto";
                      this.errors.push(msg);
                    }
                  } else {
                    isValid = true;
                  }
                }
              }

            }
          }
        } else {
          let msg : String = "Video es obligatorio";
          this.errors.push(msg);
        }
        break;
      case 10:
      this.errors = [];
        if(content.resource != undefined) {
          if(content.options.action == "OpenURL") {
            if(content.childs != undefined) {
              let child = content.childs.find(item => (item.id_content_type == 23));
              if(child != null) {
                isValid = true;
              } else {
                let msg : String = "El enlace es obligatorio";
                this.errors.push(msg);
              }
            } else {
              let msg : String = "El enlace es obligatorio";
              this.errors.push(msg);
            }
          } else {
            isValid = true;
          }
        } else {
          let msg : String = "Image es obligatoria";
          this.errors.push(msg);
        }
      break;
      case 30: 
        isValid = true; break;
      
      default:
        break;
    }
    this.progressService.progressInactiveByName('default');
    return isValid;
  }

  clean() {
    this.errors = [];
  }
}
